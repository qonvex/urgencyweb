jQuery(document).ready(function($){

    skew_size_before();
    skew_size_after();
    up_and_down_hour();
    credit_up_and_down_hour();

    $(window).resize(function(){
        skew_size_before();
        skew_size_after();
    });

    $('.hours-slide.carousel').carousel({
        interval: false,
        autoplay: false,
    });

    $('.mouse').click(function(){
        var $scrolldown = $('.hm-section2').offset();
        $('html, body').animate({ 
            scrollTop: $scrolldown.top
        }, 'slow');
        return false;
    });

    // $(window).scroll(function(){
    //     var scroll = (parseInt($(window).scrollTop() / 430 - 6));
    //     $('.slant-before, .slant-after').css({ transform: 'rotate(' + scroll + 'deg)' });
    //     console.log(scroll);
    // });


    var testimonials = $('.owl-testimonials');
    testimonials.owlCarousel({
        margin: 100,
        nav: true,
        loop: true,
        autoplay: false,
        autoHeight:true,
        responsive: {
            0: {
                items: 1
            },
        }
    });

    var projectPreview = $('.owl-project-preview');
    projectPreview.owlCarousel({
        margin: 100,
        nav: true,
        loop: true,
        autoplay: false,
        autoHeight:true,
        responsive: {
            0: {
                items: 1
            },
        }
    });

    $(document).on('click','.hm-section2 .quotation-wrapper .attach-wrapper .attachments > div .myclose',function(){
        var close_attach = $(this).data('id');
        $('.'+close_attach).remove();
    });

    $(document).on('click','.new-project-wrapper .attach-wrapper .attachments > div .myclose',function(){
        var close_attach = $(this).data('id');
        $('.'+close_attach).remove();
    });

    $(document).on('mouseenter','.hm-section3 .expertise-wrapper .experise-inner-wrapper ul li .expertise-img',function(){
        $(this).siblings('.expertise-img-hover').css('opacity','1');
    });

    $(document).on('mouseleave','.hm-section3 .expertise-wrapper .experise-inner-wrapper ul li .expertise-img',function(){
        $(this).siblings('.expertise-img-hover').css('opacity','0');
    });

    $('.testimonials-inner-wrapper .prev_next_nav .prev').click(function(event) {
        $('.owl-prev').trigger('click');
    });

    $('.testimonials-inner-wrapper .prev_next_nav .next').click(function(event) {
        $('.owl-next').trigger('click');
    });

    $('.project-view-inner-preview .prev_next_nav .prev').click(function(event) {
        $('.owl-prev').trigger('click');
    });

    $('.project-view-inner-preview .prev_next_nav .next').click(function(event) {
        $('.owl-next').trigger('click');
    });

    $(window).load(function(){
        $('.account-wrapper .user-inner-wrapper .myaccount-projects-wrapper table tr label input').each(function(index, el) {
            if ($(this).prop("checked")) {
                $(this).closest('td').siblings('td').children('.progress-status').addClass('done');
                $(this).closest('td').siblings('td').children('.progress-status').removeClass('in-progress');
                $(this).closest('td').siblings('td').children('.progress-status').text('done');
            } else {
                $(this).closest('td').siblings('td').children('.progress-status').addClass('in-progress');
                $(this).closest('td').siblings('td').children('.progress-status').removeClass('done');
                $(this).closest('td').siblings('td').children('.progress-status').text('In Progress');
            }
        });
    });

    $('.account-wrapper .user-inner-wrapper .myaccount-projects-wrapper table tr label input').click(function(event) {
        if ($(this).prop("checked") == true) {
            $(this).closest('td').siblings('td').children('.progress-status').addClass('done');
            $(this).closest('td').siblings('td').children('.progress-status').removeClass('in-progress');
            $(this).closest('td').siblings('td').children('.progress-status').text('done');
        } else if($(this).prop("checked") == false){
            $(this).closest('td').siblings('td').children('.progress-status').addClass('in-progress');
            $(this).closest('td').siblings('td').children('.progress-status').removeClass('done');
            $(this).closest('td').siblings('td').children('.progress-status').text('In Progress');
        }
    });

});

$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;
            if (filesAmount > 2) {
            //     alert('You can only upload a maximum of 2 files');
            //     $('.imgquote').val('');
            } else {
                var n_img = $( ".attachments div" ).length;
                for (i = 0; i < filesAmount; i++) {
                    // var totalimage = $('.totalimage').val(numbersArray);
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        $($.parseHTML('<div class="'+n_img+'"><span class="myclose" id="'+n_img+'" data-id="'+n_img+'">x</span><span width="100px" style="margin-right:20px;">'+input.files[0].name+'</span></div>')).appendTo(placeToInsertImagePreview);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

    };

    $('.attach-wrapper input').on('change', function(e) {
        imagesPreview(this, 'div.attachments');
    });
});


function skew_size_before() {
    var tg_skew = 0.05729;
    var top_pad = 35;
    var skew_w = $(window).width() + 200;
    var skew_h = Math.floor(skew_w*tg_skew+210);
    
    var main_cont_pad =  Math.floor(skew_h/2);
    
    $(".slant-before").css({
        'width' : skew_w + 'px',
        'height' : skew_h + 'px'
    });
    $(".slant-before").css({'bottom' : -main_cont_pad + 25});
}

function skew_size_after() {
    var tg_skew = 0.05729;
    var top_pad = 35;
    var skew_w = $(window).width() + 200;
    var skew_h = Math.floor(skew_w*tg_skew+210);
    
    var main_cont_pad =  Math.floor(skew_h/2);
    
    $(".slant-after").css({
        'width' : skew_w + 'px',
        'height' : skew_h + 'px'
    });
    $(".slant-after").css({'top' : -main_cont_pad + 25});
}

function up_and_down_hour() {

    $('.btn-down').click(function() {
        var firstchild = $('.hm-section2 .number-hours-wrapper input:nth-child(1)');
        firstchild.remove();
        $('.hm-section2 .number-hours-wrapper').append(firstchild);
    });

    $('.btn-up').click(function() {
        var lastchild = $('.hm-section2 .number-hours-wrapper input:last-child');
        lastchild.remove();
        $('.hm-section2 .number-hours-wrapper').prepend(lastchild);
    });

}

function credit_up_and_down_hour() {

    $('.btn-down').click(function() {
        var firstchild = $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(1)');
        firstchild.remove();
        $('.credit-addhour-wrapper .number-hours-wrapper').append(firstchild);
    });

    $('.btn-up').click(function() {
        var lastchild = $('.credit-addhour-wrapper .number-hours-wrapper input:last-child');
        lastchild.remove();
        $('.credit-addhour-wrapper .number-hours-wrapper').prepend(lastchild);
    });

}