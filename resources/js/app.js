/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./js/backend.js');
require('./js/custom-script.js');
require('./js/html5.js');
require('./js/html5lightbox.js');
require('./js/owl.carousel.js');
global.$ = global.jQuery = require('jquery');
import $ from 'jquery';
window.$ = window.jQuery = $;
