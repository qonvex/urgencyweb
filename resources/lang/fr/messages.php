<?php
return [
    //HEADER
    'welcome' => 'Bienvenue dans notre application',
    'title_description'  => 'Tu le casses. Nous le réparons!',
    'account' => 'COMPTE',
    'order' => 'HISTORIQUE DES COMMANDES',
    'credit' => 'CRÉDIT',
    'logout' => 'Déconnexion',

    //FOOTER
    'address' => 'ADRESSE',
    'contact' => 'CONTACT',

    //LOGIN
    'login_login' => "S'identifier",
    'login_email' => 'Email',
    'login_password' => 'Mot de passe',
    'login_remember' => 'Rester connecté',
    'login_forgot_password' => 'Mot de passe oublié ?',
    'login_btn' => "S'IDENTIFIER",
    'login_create_account' => 'CRÉER UN COMPTE',

    //REGISTER
    'register_registration' => 'Création de compte',
    'register_name' => 'NOM',
    'register_email' => 'EMAIL',
    'register_password' => 'MOT DE PASSE',
    'register_confirm_password' => 'CONFIRMEZ LE MOT DE PASSE',
    'register_agree' => 'En cliquant sur créer un compte, vous acceptez nos conditions générales.',
    'register_create_account' => 'CRÉER UN COMPTE',

    //VERIFICATION
    'verify_verfication' => 'Vérification',
    'verify_description' => 'Entrez le code de vérification que nous venons de vous envoyer sur votre email.',
    'verify_info' => "Si vous n'avez pas reçu de code!",
    'verify_btn_resend' => 'Renvoyer',
    'verify_btn_verify' => 'Vérifier',
    'verify_btn_verify_now' => 'VÉRIFIEZ MAINTENANT',

    //FORGOT PASSWORD
    'forgot_forgot_password' => 'Mot de passe oublié',
    'forgot_email' => 'Email',
    'forgot_label' => 'Nous vous enverrons un lien pour réinitialiser votre mot de passe.',
    'forgot_btn' => 'Réinitialiser',

    //RESET PASSWORD
    'reset_password' => 'Changer le mot de passe',
    'reset_email' => 'Email',
    'reset_new_password' => 'nouveau mot de passe',
    'reset_cofirm_password' => 'Confirmez le mot de passe',
    'reset_btn' => 'soumettre',

    //INDEX PAGE
    'title_description'  => 'YOU BREAK IT. WE FIX IT!',
    'title_paragraph1'  => 'VOS CLIENTS NE PEUVENT PAS PASSER COMMANDE',
    'title_paragraph2'  => 'SUR VOTRE SITE ECOMMERCE',
    'title_paragraph3'  => 'Vous ne connaissez personne capable de vous dépanner rapidement ',

    'title_second_description1'  => 'Posez nous votre question',
    'title_second_description2'  => 'Nous vous envoyons un devis, chiffré',
    'title_second_description3'  => 'Validez le devis en créant votre compte',
    'title_second_description4'  => 'Réglez votre nombres d’heures',
    'title_second_description5'  => 'Nous intervenons sur votre site',
    'title_second_description6'  => 'Vous recevez la facture acquittée de notre intervention',

    'hour_description' => 'Commandez directement des heures si vous avez besoin que l’on intervienne au plus vite sur votre site',

    'login' => "S'identifier",
    'account' => 'Compte',
    'home' => 'Accueil',
    'profile' => 'Profil',
    'title' => 'urgence web',
    'ask_qoutation' => 'Faire une demande',
    'NAME' => 'NOM',
    'EMAIL' => 'EMAIL',
    'SUBJECT' => 'MATIÈRE',
    'MESSAGE' => 'MESSAGE',
    'name' => 'prénom | nom de famille',
    'email' => 'email',
    'subject' => 'matière',
    'message' => 'message',
    'attach_file' => 'PIÈCE JOINTE',
    'send' => 'ENVOYER',
    'price' => 'PRIX',
    'no_of_hours' => 'NON. Des heures',
    'HOUR' => 'HEURE',
    'HOURS' => 'HEURES',
    'order_now' => 'Commander',
    'order' => 'Commander',

    'our_expertise' => 'NOTRE EXPERTISE',
    'our_expertise_description' => 'Nos 5 développeurs sont très complémentaires et peuvent intervenir rapidement et avec efficacité sur tous type de développement et débogage',

    'testimonials' => 'TÉMOIGNAGES',
    'testimonials_description' => 'Ils ont apprécié notre réactivité',

    'customer' => 'Client',

    //ordernow modal
    'checkbox_label' => 'Vous avez déjà un compte ?',

    //ACCOUNT PAGE
    'projects' => 'PROJETS',
    'all' => 'TOUTE',
    'done' => 'TERMINÉ',
    'inprogress' => 'EN COURS',
    'waiting' => 'ATTENDRE',
    'new_project' => 'NOUVEAU PROJET',
    'in_progress' => 'En cours',
    'started_at' => 'Date de début :',
    'hours_used' => 'Heures utilisées :',
    'view' => 'VUE',
    'no_project' => 'Aucun projet',
    //account page modal
    'project_title' => 'TITRE DU PROJET',
    'project_start_date' => 'DATE DE DÉBUT',
    'project_status' => 'STATUT',
    'project_description' => 'DESCRIPTION DU PROJET',
    'project_files' => 'FICHIERS DE PROJET',
    'project_attach' => 'Pièce jointe',
    'project_message' => 'MESSAGE',
    'create_project' => 'CREER PROJET',

    // ORDER PAGE
    'order_history' => 'HISTORIQUE DES COMMANDES',
    'date' => 'DATE',
    'description' => 'LA DESCRIPTION',
    'hours' => 'HEURES',
    'price' => 'PRIX',
    'amount' => 'montante',

    //CREDIT PAGE
    'credit' => 'CRÉDIT',
    'hours' => 'Heures',
    'HRS' => 'HRS',
    'hrs' => 'hrs',
    'available_remaining_hours' => 'HEURES RESTANTES DISPONIBLES',
    'buy_hours' => "Heures d'achat",
    'total_hours' => "TOTAL D'HEURES UTILISE",
    'hour' => 'Heure',
    'day' => 'journée',
    'week' => 'La semaine',
    'month' => 'Mois',
    'year' => 'Année',
    'over_all' => 'Sur toutes les heures qui ont été utilisées depuis la première commande.',
    'project_status' => "L'ÉTAT DU PROJET",
    'consumed_hours' => 'HEURES CONSOMMEES:',
    'hours_consumed' => 'Heures consommées:',
    'in_progress' => 'EN COURS',
    'done' => 'TERMINÉ',
    //credit page buy hours
    'no_of_hours' => "Nombre d'heures",
    'payment_method' => 'Mode de paiement',

    //PROFILE
    'profile_image' => "Télécharger l'image de profil",
    'profile_name' => 'NOM',
    'profile_email' => 'EMAIL',
    'profile_contact' => 'CONTACT',
    'profile_address' => 'ADRESSE',
    'profile_change_password' => 'CHANGER LE MOT DE PASSE',
    'profile_current_password' => 'MOT DE PASSE ACTUEL',
    'profile_new_password' => 'NOUVEAU MOT DE PASSE',
    'profile_confirm_password' => 'CONFIRMEZ LE MOT DE PASSE',

    //PAY NOW
    'pay_now' => "PAYEZ MAINTENANT",
    'pay_now_description' => "Ou réglez directement un nombre d’heures pour que nous commencions l’intervention au plus vite",

    //admin login

    'admin_login' => 'CONNEXION ADMIN',

];
