<?php
return [
    //HEADER
    'account' => 'ACCOUNT',
    'order' => 'ORDER HISTORY',
    'credit' => 'CREDIT',
    'logout' => 'Logout',

    //FOOTER
    'address' => 'ADDRESS',
    'contact' => 'CONTACT',
    
    //LOGIN
    'login_login' => 'Log in',
    'login_email' => 'Email',
    'login_password' => 'Password',
    'login_remember' => 'Remember me',
    'login_forgot_password' => 'Forgot Password ?',
    'login_btn' => 'LOG IN',
    'login_create_account' => 'CREATE ACCOUNT',

    //REGISTER
    'register_registration' => 'REGISTRATION',
    'register_name' => 'NAME',
    'register_email' => 'EMAIL',
    'register_password' => 'PASSWORD',
    'register_confirm_password' => 'CONFIRM PASSWORD',
    'register_agree' => 'By clicking create account you agree to our terms and conditions.',
    'register_create_account' => 'CREATE ACCOUNT',
    
    //VERIFICATION
    'verify_verfication' => 'Verification',
    'verify_description' => 'Enter the verification code we just sent you on your email.',
    'verify_info' => "If you didn't receive a code!",
    'verify_btn_resend' => 'Resend',
    'verify_btn_verify' => 'Verify',
    'verify_btn_verify_now' => 'VERIFY NOW',

    //FORGOT PASSWORD
    'forgot_forgot_password' => 'Forgot password',
    'forgot_email' => 'Email',
    'forgot_label' => 'We will email you a link to reset your password',
    'forgot_btn' => 'submit',
    
    //RESET PASSWORD
    'reset_password' => 'Change password',
    'reset_email' => 'Email',
    'reset_new_password' => 'New Password',
    'reset_cofirm_password' => 'Confirm Password',
    'reset_btn' => 'submit',


    //INDEX PAGE
    'title_description'  => 'YOU BREAK IT. WE FIX IT!',
    'title_paragraph1'  => 'YOUR CUSTOMERS CANNOT PLACE ORDERS',
    'title_paragraph2'  => 'ON YOUR ECOMMERCE SITE',
    'title_paragraph3'  => "You don't know anyone who can help you quickly",

    'title_second_description1'  => 'Ask us your question',
    'title_second_description2'  => 'We send you a quotation, quantified',
    'title_second_description3'  => 'Validate the quote by creating your account',
    'title_second_description4'  => 'Set your number of hours',
    'title_second_description5'  => 'We work on your site',
    'title_second_description6'  => 'You receive the paid invoice for our intervention',

    'hour_description' => 'We are committed to solving your problems within 48 hours. Otherwise you are refunded.',

    'login' => 'Log in',
    'account' => 'Account',
    'home' => 'Home',
    'profile' => 'Profile',
    'title' => 'urgence web',
    'ask_qoutation' => 'ASK A QOUTATION',
    'NAME' => 'NAME',
    'EMAIL' => 'EMAIL',
    'SUBJECT' => 'SUBJECT',
    'MESSAGE' => 'MESSAGE',
    'name' => 'firstname | last name',
    'email' => 'email',
    'subject' => 'subject',
    'message' => 'message',
    'attach_file' => 'ATTACH FILE',
    'send' => 'SEND',
    'price' => 'PRICE',
    'no_of_hours' => 'NO. OF HOURS',
    'HOUR' => 'HOUR',
    'HOURS' => 'HOURS',
    'order_now' => 'ORDER NOW',
    'order' => 'ORDER',

    'our_expertise' => 'OUR EXPERTISE',
    'our_expertise_description' => 'Our 5 developers are very complementary and can intervene quickly and efficiently on any type of development and debugging',

    'testimonials' => 'TESTIMONIALS',
    'testimonials_description' => 'They appreciated our reactivity',

    'customer' => 'Customer',

    //ordernow modal
    'checkbox_label' => 'Already have an account?',

    //ACCOUNT PAGE
    'projects' => 'PROJECTS',
    'all' => 'ALL',
    'done' => 'DONE',
    'inprogress' => 'IN PROGRESS',
    'waiting' => 'WAITING',
    'new_project' => 'NEW PROJECT',
    'in_progress' => 'In Progress',
    'started_at' => 'Start date :',
    'hours_used' => 'Hours used :',
    'view' => 'VIEW',
    'no_project' => 'No project',
    //account page modal
    'project_title' => 'PROJECT TITLE',
    'project_start_date' => 'START DATE',
    'project_status' => 'STATUS',
    'project_description' => 'PROJECT DESCRIPTION',
    'project_files' => 'PROJECT FILES',
    'project_attach' => 'attach file',
    'project_message' => 'MESSAGE',
    'create_project' => 'CREATE PROJECT',

    // ORDER PAGE
    'order_history' => 'ORDER HISTORY',
    'date' => 'DATE',
    'description' => 'DESCRIPTION',
    'hours' => 'HOURS',
    'price' => 'PRICE',
    'amount' => 'AMOUNT',

    //CREDIT PAGE
    'credit' => 'CREDIT',
    'hours' => 'Hours',
    'HRS' => 'HRS',
    'hrs' => 'hrs',
    'available_remaining_hours' => 'AVALABLE REMAINING HOURS',
    'buy_hours' => 'Buy Hours',
    'total_hours' => 'TOTAL HOURS USED',
    'hour' => 'Hour',
    'day' => 'Day',
    'week' => 'Week',
    'month' => 'Month',
    'year' => 'Year',
    'over_all' => 'Over all hours that have been used since the first order.',
    'project_status' => 'PROJECT STATUS',
    'consumed_hours' => 'CONSUMED HOURS:',
    'hours_consumed' => 'Hours consumed:',
    'in_progress' => 'IN PROGRESS',
    'done' => 'DONE',
    //credit page buy hours
    'no_of_hours' => 'No. of hours',
    'payment_method' => 'Payment Method',


    //PROFILE
    'profile_image' => 'Upload profile image',
    'profile_name' => 'NAME',
    'profile_email' => 'EMAIL',
    'profile_contact' => 'CONTACT',
    'profile_address' => 'ADDRESS',
    'profile_change_password' => 'CHANGE PASSWORD',
    'profile_current_password' => 'CURRENT PASSWORD',
    'profile_new_password' => 'NEW PASSWORD',
    'profile_confirm_password' => 'CONFIRM PASSWORD',

    //PAY NOW
    'pay_now' => "PAY NOW",
    'pay_now_description' => "You can pay directly some hours to have us working on your project faster",

    //admin login
    'admin_login' => 'ADMIN LOGIN',
];
