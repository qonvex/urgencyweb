@extends('layouts.backofficeLayout')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header mt-3">
                    <h5>CUSTOMER DETAILS</h5>
                </div>
                <div class="card-body mt-3">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="logo ml-5">
                                @php
                                    $customer_photo = $customer->profile;
                                    $path = 'assets/profile/'.$customer_photo;
                                @endphp
                                <img style="width:70%; height: 70%; border-radius: 50%;"
                                    src="{{ asset($path) }}">
                            </div>
                        </div>
                        <div class="col-md-3 mt-2">
                            <p>Name : {{ $customer->name }}</p>
                            <p>Email : {{ $customer->email }}</p>
                            <p>Contact : {{ $customer->contact }}</p>
                            <p>Address : {{ $customer->address }}</p>
                        </div>
                        <div class="col-md-2 mt-3">
                            <p>Projects : {{ count($customer->projects) > 0 ? count($customer->projects) : '0' }}</p>
                            <p>Credit : {{ $customer->total_credits < 0 ? '0' : $customer->total_credits }} hrs</p>
                            <p>Total billed : {{ $customer->total_price_orders < 0 ? '0' : $customer->total_price_orders  }} €</p>
                        </div>
                        <div class="col-md-3 mt-3">
                            <p>Total hours ordered : {{ $totalOrderHours  }} hrs</p>
                            <p>Total hours consumed : {{ $totalConsumedHours  }} hrs</p>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-5">
                    <h5>Projects</h5>
                    <div class="col-md-1 col-sm-3 col-xs-4 form-group p-0">
                        <select class="form-control p-0" id="project-view-sorting" style="font-size: 12px;">
                            <option value="3" selected>ALL</option>
                            <option value="2">DONE</option>
                            <option value="1">IN PROGRESS</option>
                            <option value="0">WAITING</option>
                        </select>
                    </div>
                    <input id="customer_id" type="text" value="{{ $customer->id }}" hidden> 
                    <span id="error-bag"></span>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="text-primary">
                                <th>
                                    Title
                                </th>
                                <th>
                                    Start Date
                                </th>
                                <th>
                                    Hours used
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Approved
                                </th>
                                <th class="text-right">
                                    actions
                                </th>
                            </thead>
                            <tbody id="customer-project-table">
                                @if (count($customer->projects) > 0)
                                @foreach ($customer->projects as $project)
                                @if ($project->is_deleted == 0)
                                <tr>
                                    <td>
                                        {{ $project->title }} <br>
                                        <i><small>{{ \Illuminate\Support\Str::words($project->description, 5) }}</small></i>
                                    </td>
                                    <td>
                                        {{ \Carbon\Carbon::parse($project->start_date)->format('F d, Y') }}
                                    </td>
                                    <td>
                                        {{ $project->hours_used == 0 ? '0' : $project->hours_used}} hrs
                                    </td>
                                    <td>
                                        <span
                                            class="credit-status {{ $project->status == 0 ? ' ' : ($project->status == 1 ? 'in-progress' : 'done') }}">{{ $project->status == 0 ?  'waiting...' : ( $project->status == 1 ? __('messages.in_progress') : __('messages.done')) }}</span>
                                    </td>
                                    <td>
                                        @if ($project->is_approved == 1)
                                        <i class="fa fa-check"></i>
                                        @else
                                        <p class="mb-0">waitng...</p>
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <span>
                                            <a href="#" class="view-project" data-id="{{ $project->id }}"><i class="fa fa-eye"></i></a>
                                        </span>
                                        &nbsp; &nbsp;
                                        <span>
                                            <a href="#" class="delete-project" data-id="{{ $project->id }}"><i class="fa fa-trash text-danger"></i></a>
                                        </span>
                                        &nbsp; &nbsp;
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                                @else
                                <span class="bg-warning">Pas de project</span>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-body mt-5">
                    <h5>Orders</h5>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>
                                    ORDER ID
                                </th>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Hours
                                </th>
                                <th>
                                   Amount
                                </th>
                            </thead>
                            <tbody>
                                @if (count($orders) > 0)
                                @foreach ($orders as $order)
                                <tr>
                                    <td>
                                        {{ $order->payment->order_transaction_id }}
                                    </td>
                                    <td>
                                        {{ \Carbon\Carbon::parse($order->created_at)->format('F d, Y') }}
                                    </td>
                                    <td>
                                        {{ $order->hours }} hrs
                                    </td>
                                    <td>
                                        {{ $order->payment->amount }}€
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <span class="bg-warning">Pas de order</span>
                                @endif
                            </tbody>
                        </table>
                        {{ $orders->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('backoffice.projects.view')
@endsection
