@extends('layouts.backofficeLayout')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Customers</h4>
                    <div class="col-2 form-froup p-0">
                        <input id="search-project" name="search-project" type="text" class="form-control" placeholder="Search . . .">
                    </div>
                    <span id="error-bag"></span>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>
                                    
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Contact
                                </th>
                                <th>
                                    Address
                                </th>
                                <th>
                                    Projects
                                </th>
                                <th>
                                    Credit
                                </th>
                                <th class="text-right">
                                    actions
                                </th>
                            </thead>
                            <tbody id="customer-table">
                                @if (count($customers) > 0)
                                    @foreach ($customers as $customer)
                                    @php
                                        $customer_photo = $customer->profile;
                                        $path = 'assets/profile/'.$customer_photo;
                                    @endphp
                                    <tr>
                                        <td>
                                            <div class="logo ml-5">
                                                <img src="{{ asset($path) }}" class="customer-img">
                                            </div>
                                        </td>
                                        <td>
                                            <a href="{{ route('customer.view', $customer->id) }}" style="text-decoration:none; color:#000000;">
                                                {{ $customer->name }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('customer.view', $customer->id) }}" style="text-decoration:none; color:#000000;">
                                                {{ $customer->email }}
                                            </a>
                                        </td>
                                        <td>
                                        {{ $customer->contact == null ? 'no contact' : $customer->contact }}
                                        </td>
                                        <td>
                                        {{ $customer->address == null ? 'no address' : $customer->address }}
                                        </td>
                                        <td>
                                           {{  count($customer->projects->where('is_deleted', 0)) }}
                                        </td>
                                        <td>
                                            {{ $customer->total_credits < 0 ? '0' :  $customer->total_credits }} hrs
                                        </td>
                                        <td class="text-right">
                                            <span>
                                                <a href="{{ route('customer.view', $customer->id) }}"> <i class="fa fa-eye"></i></a>
                                            </span>
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                        </td>
                                    </tr>      
                                    @endforeach
                                @else 
                                <span class="bg-warning">Pas de customer</span>
                                @endif
                            </tbody>
                        </table>
                    </div> 
                    <div class="mt-3">
                        {{ $customers->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
