@extends('layouts.backofficeLayout')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Orders</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>Customer</th>
                                <th>
                                    ORDER ID
                                </th>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Hour
                                </th>
                                <th>
                                    Amount
                                </th>
                                <th class="text-right">
                                    actions
                                </th>
                            </thead>
                            <tbody>
                                @if (count($orders) > 0)
                                    @foreach ($orders as $order)
                                    <tr  class="view-order" data-id="{{ $order->id }}">
                                         <td>
                                            {{ $order->user->name}}
                                        </td>
                                        <td>
                                            {{ $order->payment->order_transaction_id }}
                                        </td>
                                        <td>
                                            {{ \Carbon\Carbon::parse($order->created_at)->format('F d, Y') }}
                                        </td>
                                        <td>
                                            {{ $order->hours }} hrs
                                        </td>
                                        <td>
                                            {{ $order->payment->amount }}€
                                        </td>
                                        <td class="text-right">
                                            <span>
                                                <a href="#" class="view-order" data-id="{{ $order->id }}"><i class="fa fa-eye"></i></a>
                                            </span>
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                        </td>
                                    </tr>      
                                    @endforeach
                                @else 
                                <span class="bg-warning">Pas de order</span>
                                @endif
                            </tbody>
                        </table>
                        {{ $orders->links() }}
                    </div> 
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Total</h4>
                </div>
                <div class="card-body">
                    <p>Count : <strong>{{ $total_orders > 0 ? $total_orders : '0' }}</strong>&nbsp;orders</p>
                    <p>Hours : <strong>{{ $total_hours > 0 ? $total_hours : '0' }}</strong>&nbsp;hrs </p>
                    <p>Billed : <strong>{{ $total_billed > 0 ? number_format($total_billed) : '0' }}</strong>&nbsp;€</p>
                </div>
            </div>
        </div>
    </div>
</div>
@include('backoffice.orders.view')
@endsection
