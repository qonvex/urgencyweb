@extends('layouts.backofficeLayout')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-6 col-xs-12 col-sm-12">
            <div class="card">
                <div class="card-header text-left">
                    <p class="ml-5">Projects Summary</p>
                </div>
                <div class="card-body text-left">
                    <span class="ml-5 mr-5">Waiting : <strong>{{ count($not_started) > 0 ? count($not_started) : '0' }}</strong></span>
                    <span class="ml-5 mr-5">In Progress : <strong>{{ count($in_progress) > 0 ? count($in_progress) : '0' }}</strong></span>
                    <span class="ml-5 mr-5">Done : <strong>{{ count($done) > 0 ? count($done) : '0' }}</strong></span>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-1 col-sm-3 col-xs-4 form-group p-0">
                        <select class="form-control p-0" id="project-sorting" style="font-size: 12px;">
                            <option value="3" selected>ALL</option>
                            <option value="2">DONE</option>
                            <option value="1">IN PROGRESS</option>
                            <option value="0">WAITING</option>
                        </select>
                    </div>
                    <span id="error-bag"></span>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>
                                    Title
                                </th>
                                <th>
                                    Start Date
                                </th>
                                <th>
                                    Hours used
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Approved
                                </th>
                                <th class="text-right">
                                    actions
                                </th>
                            </thead>
                            <tbody id="project-table">
                                @if (count($projects) > 0)
                                    @foreach ($projects as $project)
                                    <tr  class="view-project" data-id="{{ $project->id }}">
                                        <td>
                                            {{ $project->title }} <br>
                                            <i><small>{{ \Illuminate\Support\Str::words($project->description, 5) }}</small></i>
                                        </td>
                                        <td>
                                            {{ \Carbon\Carbon::parse($project->start_date)->format('F d, Y') }}
                                        </td>
                                        <td>
                                            {{ $project->hours_used == 0 ? '0' : $project->hours_used}} hrs
                                        </td>
                                        <td>
                                            <span class="credit-status {{ $project->status == 0 ? ' ' : ($project->status == 1 ? 'in-progress' : 'done') }}">{{ $project->status == 0 ?  'waiting...' : ( $project->status == 1 ? __('messages.in_progress') : __('messages.done')) }}</span>
                                        </td>
                                        <td>
                                            @if ($project->is_approved == 1)
                                                <i class="fa fa-check"></i>
                                            @else 
                                                <p class="mb-0">waiting...</p>
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            <span>
                                                <a href="#" class="view-project" data-id="{{ $project->id }}"><i class="fa fa-eye"></i></a>
                                            </span>
                                            &nbsp; &nbsp;
                                            <span>
                                                <a href="#" class="delete-project" data-id="{{ $project->id }}"><i class="fa fa-trash text-danger"></i></a>
                                            </span>
                                            &nbsp; &nbsp;
                                        </td>
                                    </tr>      
                                    @endforeach
                                @else 
                                <span class="bg-warning">Pas de project</span>
                                @endif
                            </tbody>
                        </table>
                    </div> 
                    <div class="mt-3">
                        {{ $projects->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('backoffice.projects.view')
@endsection