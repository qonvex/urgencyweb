<div class="modal fade" id="addAdmin" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="admin_mode"></h5>
            </div>
            <div class="modal-body">
                {{ Form::open(array('id' => 'adminForm', 'class' => 'text-dark')) }}
                    <div class="form-group">
                        <label for="name">Name :</label>
                        {{ Form::text('name', null, ['class' => 'form-control']) }}
                        <small class="text-danger err" id="admin-name-error">  
                        </small>
                    </div> 
                    <div class="form-group">
                        <label for="name">Email :</label>
                        {{ Form::text('email', null, ['class' => 'form-control']) }}
                        <small class="text-danger err" id="admin-email-error">  
                        </small>
                    </div>
                    <div class="form-group">
                        <div class="new-pass"></div>
                        <small class="text-danger err" id="admin-nomatch-error"></small>
                    </div>
                    <div class="form-group pass-div">
                        <label for="name">Password :</label>
                        <input name="password" type="password" class="form-control">
                        <small class="text-danger err" id="admin-password-error">  
                        </small>
                    </div>
                    <div class="form-group pass-div">
                        <label for="name">Confirm Password :</label>
                        <input name="confirm_password" type="password" class="form-control">
                    </div>
                    <span id="change-pass"></span>
                    <input type="hidden" name="id" id="id">
                {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="save-admin" type="button" class="btn btn-primary btn-admin">Save changes</button>
            </div>
        </div>
    </div>
</div>