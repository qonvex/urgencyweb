<div class="modal fade" id="editHour" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5><strong id="hours"></strong> price</h5>
            </div>
            <div class="modal-body">
                {{ Form::open(array('id' => 'hourForm', 'class' => 'text-dark')) }}
                
                {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="update-price" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>