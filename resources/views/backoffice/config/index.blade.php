@extends('layouts.backofficeLayout')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Prices configuration</h4>
                    <div class="col-md-12 text-right">
                        <a href="#" class="btn btn-default btn-add-price"><i class="fa fa-plus text-light">&nbsp;&nbsp;Add</i></a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive custom-table">
                        <table class="table">
                            <thead class=" text-primary">
                                <th style="width:33%">
                                    Hours
                                </th>
                                <th style="width:33%">
                                    Price
                                </th style="width:33%">
                                <th class="text-center">
                                    actions
                                </th>
                            </thead>
                            <tbody id="admin-table" class="pricing">
                                @if (count($config_hours) > 0)
                                   @foreach ($config_hours as $config_hour)
                                    <tr>
                                        <td>
                                            {{ $config_hour->hours == 1 ? $config_hour->hours.' hr' : $config_hour->hours.' hrs' }}
                                        </td>
                                        <td>
                                            {{ $config_hour->hour_price }} €
                                        </td>
                                        <td class="text-center">
                                                <a href="#" class="edit-price" data-id="{{ $config_hour->id }}" data-hour = "{{ $config_hour->hours }}"><i class="fa fa-edit text-secondary"></i></a>
                                                <span style="color:silver" class="mx-2">|</span>
                                                <a href="#" class="delete-price" data-id="{{ $config_hour->id }}"><i class="fa fa-trash text-danger"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach 
                                @endif
                            </tbody>
                        </table>
                    </div> 
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List of Administrators</h4>
                    <div class="col-md-12 text-right">
                        <a href="#" class="btn btn-default btn-add-admin"><i class="fa fa-plus text-light">&nbsp;&nbsp;Add</i></a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th style="min-width:60px">
                                    
                                </th>
                                <th style="width:30%">
                                    Name
                                </th>
                                <th style="width:40%">
                                    Email
                                </th>
                                <th style="width:20%" class="text-center">
                                    actions
                                </th>
                            </thead>
                            <tbody id="admin-table">
                                @if (count($admins) > 0)
                                   @foreach ($admins as $admin)
                                    <tr>
                                        <td class="text-center">
                                            <img style="width:40px; height: 40px; border-radius: 50%; margin: 0px;" src="{{ asset('assets/img/prof.jpg') }}">
                                        </td>
                                        <td>
                                            {{ $admin->name }}
                                        </td>
                                        <td>
                                            {{ $admin->email }}
                                        </td>
                                        <td class="text-center">
                                            @if (Auth::user()->is_super == 1)
                                                <a href="#" class="btn-edit-admin" data-id="{{ $admin->id }}"><i class="fa fa-edit text-secondary"></i></a>
                                                <span style="color:silver" class="mx-2">|</span>
                                                <a href="#" class="delete-admin" data-id="{{ $admin->id }}"><i class="fa fa-trash text-danger"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach 
                                @else 
                                    <span class="bg-warning">Pas de Admin</span>
                                @endif
                            </tbody>
                        </table>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
@include('backoffice.config.create')
{{-- @include('backoffice.config.editprice') --}}
@include('backoffice.config.addprice')
@endsection