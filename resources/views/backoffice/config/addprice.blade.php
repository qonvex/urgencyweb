<div class="modal fade" id="addHour" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5><strong id="hours"></strong> Price</h5>
            </div>
            <div class="modal-body">
                {{ Form::open(['id' => 'hourForm', 'class' => 'text-dark']) }}
                  <div class="form-row">
                      <div class="form-group col-6">
                          <label for="hour">Hour :</label>
                          {{ Form::number('hour', null, ['class' => 'form-control']) }}
                          <small class="text-danger" id="hour-error">  
                          </small>
                      </div> 
                      <div class="form-group col-6">
                          <label for="price">Price :</label>
                          {{ Form::number('price', null, ['class' => 'form-control']) }}
                          <small class="text-danger" id="price-error">  
                          </small>
                      </div> 
                      <input type="hidden" name="price_id" id="price_id">
                  </div>
                {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                <button id="add-price" type="button" class="btn btn-primary btn-price"><i class="fa fa-save"></i> Save</button>
            </div>
        </div>
    </div>
</div>