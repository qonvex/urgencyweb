@extends('layouts.backofficeLayout')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5 col-md-4">
                            <div class="icon-big text-center icon-warning">
                                <i class="nc-icon nc-single-copy-04 text-warning"></i>
                            </div>
                        </div>
                        <div class="col-7 col-md-8">
                            <a href="{{ route('new.project') }}" style="text-decoration:none;">
                                <div class="numbers">
                                    <p class="card-category">New Projects</p>
                                    <p class="card-title">
                                        {{ count($projects) > 0 ? count($projects) : '0' }}
                                        <p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">
                        <i class="fa fa-eye"></i> Waiting
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5 col-md-4">
                            <div class="icon-big text-center icon-warning">
                                <i class="nc-icon nc-icon nc-box-2 text-success"></i>
                            </div>
                        </div>
                        <div class="col-7 col-md-8">
                            <a href="{{ route('new.quotation') }}" style="text-decoration:none;">
                                <div class="numbers">
                                    <p class="card-category">Quotations</p>
                                    <p class="card-title">
                                        {{ count($new_quotations) > 0 ? count($new_quotations) : '0' }}
                                        <p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">
                        <i class="nc-icon nc-bookmark-2"></i> New
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5 col-md-4">
                            <div class="icon-big text-center icon-warning">
                                <i class="nc-icon nc-bullet-list-67 text-danger"></i>
                            </div>
                        </div>
                        <div class="col-7 col-md-8">
                            <div class="numbers">
                                <p class="card-category">Orders</p>
                                <p class="card-title">
                                    {{ count($orders) > 0 ? count($orders) : '0' }}
                                    <p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">
                        <i class="fa fa-calendar-o"></i>Total
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5 col-md-4">
                            <div class="icon-big text-center icon-warning">
                                <i class="nc-icon nc-money-coins text-primary"></i>
                            </div>
                        </div>
                        <div class="col-7 col-md-8">
                            <div class="numbers">
                                <p class="card-category">Total Billed</p>
                                <p class="card-title">
                                    {{ $total_billed > 0 ? number_format($total_billed).' €' : '0 €' }}
                                    <p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">
                        Total {{ $total_hours > 0 ? $total_hours : '0' }} hrs
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header ">
                    <h5 class="card-title">Latest Orders</h5>
                </div>
                <div class="card-body ">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>
                                    Customer
                                </th>
                                <th>
                                    Order ID
                                </th>
                                <th>
                                    Hour
                                </th>
                                <th>
                                    Amount
                                </th>
                                <th>
                                    Date
                                </th>
                            </thead>
                            <tbody>
                                @if (count($all_orders) > 0)
                                @foreach ($all_orders as $order)
                                <tr>
                                    <td>
                                        {{ $order->user->name }} <br>
                                    </td>
                                    <td>
                                        {{ $order->payment->order_transaction_id }} <br>
                                    </td>
                                    <td>
                                        {{ $order->hours }}
                                    </td>
                                    <td>
                                        {{ $order->payment->amount }} &nbsp; &euro;
                                    </td>
                                    <td>
                                        {{ \Carbon\Carbon::parse($order->created_at)->format('F d, Y') }}
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <span class="bg-warning">Pas de order</span>
                                @endif
                            </tbody>
                        </table>
                        {{-- {{ $all_orders->links() }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card ">
                <div class="card-header ">
                    <h5 class="card-title">Latest Quotations</h5>
                </div>
                <div class="card-body ">

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card ">
                <div class="card-header ">
                    <h5 class="card-title">Latest Projects</h5>
                </div>
                <div class="card-body ">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
