<div class="sidebar" data-color="white" data-active-color="danger">
    <!--
          Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
      -->
    <div class="logo">
        <a href="/admin" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="{{ asset('assets/img/logo-white.png') }}">
            </div>
        </a>
        <a href="/admin" class="simple-text logo-normal">
            Urgence Web
            <!-- <div class="logo-image-big">
              <img src="../assets/img/logo-big.png">
            </div> -->
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ Request::segment(1) === 'admin' ? 'active' : null }}">
                <a href="/admin">
                    <i class="nc-icon nc-bank"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li
                class="{{ Request::segment(1) === 'customers' || Request::segment(1) == 'customer' ? 'active' : null }}">
                <a href="{{ route('customers.index') }}">
                    <i class="nc-icon nc-single-02"></i>
                    <p>Customers</p>
                </a>
            </li>
            <li class="{{ Request::segment(1) === 'projects' ? 'active' : null }}">
                <a href="{{ route('projects.index') }}">
                    <i class="nc-icon nc-single-copy-04"></i>
                    <p>Projects</p>
                </a>
            </li>
            <li class="{{ Request::segment(1) === 'orders' ? 'active' : null }}">
                <a href="{{ route('orders.index') }}">
                    <i class="nc-icon nc-bullet-list-67"></i>
                    <p>Orders</p>
                </a>
            </li>
            <li class="{{ Request::segment(1) === 'quotations' ? 'active' : null }}">
                <a href="{{ route('quotations.index') }}">
                    <i class="nc-icon nc-box-2"></i>
                    <p>Quotations</p>
                </a>
            </li>
            <li class="{{ Request::segment(1) === 'config' ? 'active' : null }}">
                <a href="{{ route('config.index') }}">
                    <i class="nc-icon nc-settings"></i>
                    <p>Config</p>
                </a>
            </li>
        </ul>
    </div>
</div>
