@extends('layouts.backofficeLayout')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-10 offset-1">
            <div class="card">
                <div class="card-header">
                   <h5>Quotations</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th style="width:25%">
                                    from
                                </th>
                                <th style="width:25%">
                                    email
                                </th>
                                <th style="width:20%">
                                    subject
                                </th>
                                <th style="width:20%">
                                    date created
                                </th>
                                <th style="width:10%" class="text-center">
                                    actions
                                </th>
                            </thead>
                            <tbody>
                                @if (count($quotations) > 0)
                                    @foreach ($quotations as $quotation)
                                    <tr id="quotation" data-id="{{$quotation->id}}" style="cursor: pointer">
                                        <td>
                                            {{ $quotation->name }}
                                        </td>
                                        <td>
                                            {{ $quotation->email }}
                                        </td>
                                        <td>
                                              {{ $quotation->subject }}
                                        </td>
                                        <td>
                                            {{ $quotation->created_at->format('d/m/Y') }}
                                        </td>
                                        <td class="text-center">
                                            <a href="#" class="edit-quotation" data-id="{{ $quotation->id }}"><i class="fa fa-edit text-secondary"></i></a>
                                            <span class="mx-2 text-silver">|</span>
                                            <a href="#" class="delete-quotation" data-id="{{ $quotation->id }}"><i class="fa fa-trash text-danger"></i></a>
                                        </td>
                                    </tr>      
                                    @endforeach
                                @else 
                                <span class="bg-warning">Pas de quotation</span>
                                @endif
                            </tbody>
                        </table>
                        {{ $quotations->links() }}
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
@include('backoffice.quotations.view')
@endsection