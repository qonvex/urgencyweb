
@component('mail::message')
{{ $content['title'] }}
<br><br>
<p>Hi, {{ $content['user']['name'] }}</p>
<br>
{{ $content['body'] }}
<br>
@component('mail::panel')
<h1>{{ $content['pin'] }} </h1>
@endcomponent

@component('mail::button', ['url' => $content['link']])
{{ $content['button'] }}
@endcomponent

<span>Thanks, </span>
{{ config('app.name') }}
@endcomponent