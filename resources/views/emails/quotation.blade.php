@component('mail::message')
{{ $content['title'] }}
<br><br>
<p>Hi, {{ $content['name'] }}</p>
<br>
{{ $content['body'] }}
<br><br>
@component('mail::panel')
 <p>NAME : {{ $content['name'] }}</p>
 <p>EMAIL : {{ $content['email'] }}</p>
 <p>SUBJECT : {{ $content['subject'] }}</p>
 <p>MESSAGE : {{ $content['message'] }}</p>
@endcomponent

<span>Thanks, </span>
{{ config('app.name') }}
@endcomponent