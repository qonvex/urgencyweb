<div class="modal fade urgence-modal" id="profileForm" tabindex="-1" role="dialog" aria-labelledby="Profile"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <div class="credit-addhour-wrapper">
                    <div class="logo-wrapper" style="margin-bottom: 7px;">
                        <div class="logo">
                            <img src="assets/img/logo.png" alt="logo" class="img-responsive" width="150px">
                        </div>
                        <div class="slogan">
                            <h3 style="font-size:10px;">{{ __('messages.title_description') }}</h3>
                        </div>
                    </div>
                    @php
                        $user_photo = Auth::user()->profile;
                        $path = 'assets/profile/'.$user_photo;
                    @endphp
                    {{ Form::open(array('action' => ['Customer\AccountController@updateProfile', Auth::user()->id],'method' => 'PUT', 'class' => 'profileForm', 'enctype' => 'multipart/form-data')) }}
                    <div class="row">
                        <div class="col-12 text-center mb-2">
                            <img id="profile-pic" src="{{ asset($path) }}" style="border-radius: 50%; height: 90px; width: 90px;">
                            <img id="profile-view" src="#" width="100px" hidden style="border-radius: 50%; height: 90px; width: 90px;"/><br>
                        </div>
                        <div class="order-details-wrapper">
                            <input id="user-id" type="text" value="{{ Auth::user()->id }}" hidden>
                            <div class="order-name">
                                <h4>{{ __('messages.profile_image') }}</h4>
                                {{ Form::file('profile', ['id' => 'photo-profile']) }}
                            </div>
                            <div class="order-name">
                                <h4>{{ __('messages.profile_name') }}</h4>
                                {{ Form::text('name', Auth::user()->name, ['class' => 'form-control']) }}
                            </div>
                            <div class="order-email">
                                <h4>{{ __('messages.profile_email') }}</h4>
                                {{ Form::email('email', Auth::user()->email, ['class' => 'form-control']) }}
                            </div>
                            <div class="order-contact">
                                <h4>{{ __('messages.profile_contact') }}</h4>
                                {{ Form::number('contact', Auth::user()->contact, ['class' => 'form-control']) }}
                            </div>
                            <div class="order-address">
                                <label>{{ __('messages.profile_address') }}</label>
                                {{ Form::text('address', Auth::user()->address, ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                <input id="showPassword" type="checkbox" class="pt-1">
                                <span class="inline-block">{{ __('messages.profile_change_password') }}</span>
                            </div>
                            <div id="password-inputs" hidden="true">
                                <div class="order-password">
                                    <h4>{{ __('messages.profile_current_password') }}</h4>
                                    {{ Form::password('current_password', ['class' => 'form-control']) }}
                                </div>
                                <div class="order-password">
                                    <h4>{{ __('messages.profile_new_password') }}</h4>
                                    {{ Form::password('new_password', ['class' => 'form-control']) }}
                                </div>
                                <div class="order-password">
                                    <h4>{{ __('messages.profile_confirm_password') }}</h4>
                                    {{ Form::password('confirm_password', ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-right">
                            {{ Form::submit('Save changes', ['class' => 'btn btn-primary']) }}
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
