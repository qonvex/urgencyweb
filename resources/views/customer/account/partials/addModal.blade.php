<!-- CREATE NEW PROJECT MODAL -->
<div class="modal fade urgence-modal" id="NewProject" tabindex="-1" role="dialog" aria-labelledby="NewProject" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
              </button>
              <div class="new-project-wrapper">
                  <div class="logo-wrapper">
                      <div class="logo">
                          <img src="assets/img/logo.png" alt="logo" class="img-responsive" width="280px">
                      </div>
                      <div class="slogan">
                          <h3>{{ __('messages.title_description') }}</h3>
                      </div>
                  </div>
                  {{ Form::open(array('id' => 'projectForm', 'enctype' => 'multipart/form-data')) }}
                      <div class="project-title">
                          <h4>Project title</h4>
                          {{ Form::text('title', null, ['class' => 'form-control']) }}
                          <small class="alert-danger" id="project-title-error"></small>
                      </div>
                      <div class="start-date">
                          <span class="start-date-label">{{ __('messages.project_start_date') }} :</span>
                          {{ Form::text('start_date', null, array('id' => 'datepicker', 'value' => 'jj/mm/aaaa'))}}
                          <small class="alert-danger" id="project-start-date-error"></small>
                      </div>
                      <div class="project-description">
                          <h4>{{ __('messages.project_description') }}</h4>
                          {{ Form::textarea('description', null, ['class' => 'form-control', 'rows' => '6']) }}
                          <small class="alert-danger" id="project-description-error"></small>
                      </div>
                      <div class="project-message">
                          <h4>{{ __('messages.project_message') }}</h4>
                          {{ Form::textarea('message', null, ['class' => 'form-control', 'rows' => '6']) }}
                          <small class="alert-danger" id="project-message-error"></small>
                      </div>
                      <div class="form-group attach-wrapper">
                            <div class="attachments mb-3">
                            </div>
                            <label class="btn btn-default btn-file ">
                              <span class="fa fa-paperclip"></span>{{ __('messages.project_attach') }}
                              <input id="project_files" name="file[]" type="file" style="display: none;" multiple="true">
                          </label>
                      </div>
                      {{ Form::submit(__('messages.create_project'), ['class' => 'btn-create-project form-control', 'id' => 'btn-project']) }}
                  {{ Form::close() }}
              </div>
            </div>
      </div>
    </div>
</div>
