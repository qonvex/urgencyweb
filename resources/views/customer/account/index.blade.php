@extends('layouts.customerLayout')

@section('content')
<div class="myaccount-process-wrapper">
    <div class="process-title">
        <h4>{{ __('messages.account') }}</h4>
    </div>
    <div class="myaccount-projects-wrapper">
        <div class="myacount-actions-wrapper">
            <ul>
                <li>
                    <span>{{ __('messages.projects') }}</span>
                    <select id="project-sort" name="project-select">
                        <option value="3" selected>{{ __('messages.all') }}</option>
                        <option value="2">{{ __('messages.done') }}</option>
                        <option value="1">{{ __('messages.inprogress') }}</option>
                        <option value="0">{{ __('messages.waiting') }}</option>
                    </select>
                </li>
                <li>
                    <a href="#" data-toggle="modal" data-target="#NewProject">
                        <img src="assets/img/ic_add_box_48px.png">
                        <span>{{ __('messages.new_project') }}</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="table-scroll">
            <table id="project-table" cellspacing="0" cellpadding="0" width="100%">
                @if (count($projects) > 0)
                    @foreach ($projects as $project)
                    <tr>
                        <td width="10%" valign="center">
                            <label>
                                <span class="{{ $project->status == 0 ? 'status-white' : ($project->status == 1 ? 'status-orange' : 'status-green') }}"></span>
                            </label>
                        </td>
                        <td width="25%" valign="center" align="left">
                            <small class="progress-status {{ $project->status == 0 ? 'text-waiting': ($project->status == 1 ? 'text-inprogress' : 'text-done') }}">{{ $project->status == 0 ? 'waiting...': ($project->status == 1 ? 'In progress' : 'Done') }}</small>
                            <h4>{{ $project->title }}</h4>
                            <p>{{ \Illuminate\Support\Str::words($project->description, 5) }}</p>
                        </td>
                        <td width="25%" valign="center" align="center">
                            <strong><span>{{ __('messages.started_at') }}</span></strong> <span>{{ \Carbon\Carbon::parse($project->start_date)->format('F d, Y') }}</span>
                        </td>
                        <td width="25%" valign="center" align="center">
                            <strong><span>{{ __('messages.hours_used') }}</span></strong>
                            <span>
                                {{ $project->hours_used != null ? $project->hours_used. ' hrs' : '0 hrs' }}
                            </span>
                        </td>
                        <td width="15%" valign="center" align="center">
                            <a href="/account/project/{{ $project->id }}">{{ __('messages.view') }}</a>
                        </td>
                    </tr>
                    @endforeach
                @else 
                    <p class="bg-danger">{{ __('messages.no_project') }}</p>
                @endif
            </table>
        </div>
    </div>
</div>
@endsection
