@extends('layouts.customerLayout')

@section('content')
<div class="project-view-wrapper">
    <div class="project-view-inner">
        <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td width="20%" align="left" valign="top"><h4 class="project-view-legend">{{ __('messages.project_title') }}:</h4></td>
                <td width="80%" align="left" valign="top"><h4>{{ $project->title }}</h4></td>
            </tr>
            <tr>
                <td  width="20%" align="left" valign="top"><h4 class="project-view-legend">{{ __('messages.project_start_date') }}:</h4></td>
                <td width="80%" align="left" valign="top"><h4>{{ \Carbon\Carbon::parse($project->start_date)->format('F d, Y') }}</h4></td>
            </tr>
            <tr>
                <td width="20%" align="left" valign="top"><h4 class="project-view-legend">{{ __('messages.project_status') }}:</h4></td>
                <td width="80%" align="left" valign="top"><h4 class="poject-view-status">{{ $project->status == 0 ? 'waiting...' : ( $project->status == 1 ? 'In Progress' : 'Done') }}</h4></td>
            </tr>
            <tr>
                <td width="20%" align="left" valign="top"><h4 class="project-view-legend">{{ __('messages.project_description') }}:</h4></td>
                <td width="80%" align="left" valign="top"><h4>{{ $project->description }}</h4></td>
            </tr>
            <tr>
                <td width="20%" align="left" valign="top"><h4 class="project-view-legend">{{ __('messages.project_files') }}:</h4></td>
                <td width="80%" align="left" class="mt-5">
                    @foreach ($project->files as $file)
                        <span>{{ $file->file_name }}</span>, &nbsp;
                    @endforeach
                </td>
            </tr>
        </table>
    </div>
</div>
@endsection
