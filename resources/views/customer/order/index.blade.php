@extends('layouts.customerLayout')

@section('content')
<div class="myaccount-process-wrapper order-history-wrapper">
    <div class="process-title">
        <h4>{{ __('messages.order_history') }}</h4>
    </div>
    <div class="myaccount-projects-wrapper">
        <div class="myacount-actions-wrapper">
            <ul>
                <li>
                    <?php
                            $montharr = array('All','January','February','March','April','May','June','July','August','September','October','November','December');
                            $monthlength = count($montharr);
                        ?>
                    <select id="select-month">
                        <?php
                                for ($i=0; $i < $monthlength; $i++) {
                                    ?>
                        <option value="<?=$i?>"><?=$montharr[$i]?></option>
                        <?php
                                }
                            ?>
                    </select>
                </li>
                <li></li>
            </ul>
        </div>
        <table cellspacing="0" cellpadding="0" width="100%">
            <tr class="order-history-table-heading">
                <td width="30%" align="left">
                    <h4>{{ __('messages.date') }}</h4>
                </td>
                <td width="20%" align="left">
                    <h4>{{ __('messages.hours') }}</h4>
                </td>
                <td width="20%" align="left">
                    <h4>{{ __('messages.price') }}</h4>
                </td>
                <td width="20%" align="left">
                    <h4>{{ __('messages.amount') }}</h4>
                </td>
                <td width="10%" align="center">
                    <h4></h4>
                </td>
            </tr>

            @if (count($orders) > 0)
            <tbody id="order-table">
            @foreach ($orders as $order)
                <tr>
                    <td valign="center" align="left">
                        <h5>{{ \Carbon\Carbon::parse($order->created_at)->format('F d, Y') }}</h5>
                    </td>
                    <td valign="center" align="left">
                        <h5>{{ $order->hours }} hrs</h5>
                    </td>
                    <td valign="center" align="left">
                        <h5>{{ $order->price }} €</h5>
                    </td>
                    <td valign="center" align="left">
                        <h5>{{ $order->payment->amount }} €</h5>
                    </td>
                    <td valign="center" align="center">
                        <ul>
                            <li>
                                <a href="{{ route('order.invoice', $order->id) }}" ><img src="{{ asset('assets/img/download.png') }}"></a>
                            </li>
                            <li>
                                <a href="#" class="delete-order" data-id="{{ $order->id }}"><img src="{{ asset('assets/img/delete.png') }}"></a>
                            </li>
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
            @else 
                <p class="bg-danger">No Order</p>
            @endif
        </table>
    </div>
</div>
@endsection
