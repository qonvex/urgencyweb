<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>UrgenceWeb Invoice</title>
   <style>
       @font-face {
        font-family: SourceSansPro;
        src: url(SourceSansPro-Regular.ttf);
        }

        .clearfix:after {
        content: "";
        display: table;
        clear: both;
        }

        a {
        color: #0087C3;
        text-decoration: none;
        }

        body {
        position: relative;
        width: 100%;  
        height: 100vh; 
        color: #555555;
        background: #FFFFFF; 
        font-family: Arial, sans-serif; 
        font-size: 14px; 
        font-family: SourceSansPro;
        }

        header {
        padding: 10px 0;
        margin-bottom: 20px;
        border-bottom: 1px solid #AAAAAA;
        }

        #logo {
        float: left;
        margin-top: 8px;
        }

        #logo img {
        height: 70px;
        }

        #company {
        margin-right: 0px;
        text-align: right;
        }


        #details {
            width: 100%;
        }

        #client {
            padding-left: 6px;
            border-left: 6px solid #0087C3;
            margin-right: 0px;
            display: inline-block;
            width: 49%;
            box-sizing: border-box;
            margin-left: 0px;
        }

        #client .to {
        color: #777777;
        }

        h2.name {
        font-size: 1.4em;
        font-weight: normal;
        margin: 0;
        }

        #invoice {
            margin-right: 0px;
            text-align: right;
            display: inline-block;
            width: 50%;
        }

        #invoice h1 {
        color: #0087C3;
        font-size: 2.4em;
        line-height: 1em;
        font-weight: normal;
        margin: 0  0 10px 0;
        }

        #invoice .date {
        font-size: 1.1em;
        color: #777777;
        padding-right: 8px;
        }

        table {
        text-align: center;
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px;
        }

        table th,
        table td {
        text-align: center;
        padding: 20px;
        background: #EEEEEE;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;
        }

        table th {
        white-space: nowrap;        
        font-weight: normal;
        }

        table td {
        text-align: center;
        }

        table td h3{
        color: #57B223;
        font-size: 1.2em;
        font-weight: normal;
        margin: 0 0 0.2em 0;
        }

        table .no {
        color: #FFFFFF;
        font-size: 1.6em;
        background: #57B223;
        }

        table .desc {
        text-align: left;
        }

        table .unit {
        background: #DDDDDD;
        }

        table .qty {
        }

        table .total {
        background: #57B223;
        color: #FFFFFF;
        }

        table td.unit,
        table td.qty,
        table td.total {
        font-size: 1.2em;
        }

        table tbody tr:last-child td {
        border: none;
        }

        table tfoot td {
        padding: 10px 20px;
        background: #FFFFFF;
        border-bottom: none;
        font-size: 1.2em;
        white-space: nowrap; 
        border-top: 1px solid #AAAAAA; 
        }

        table tfoot tr:first-child td {
        border-top: none; 
        }

        table tfoot tr:last-child td {
        color: #57B223;
        font-size: 1.4em;
        border-top: 1px solid #57B223; 

        }

        table tfoot tr td:first-child {
        border: none;
        }

        #thanks{
        font-size: 2em;
        margin-bottom: 50px;
        }

        #notices{
        padding-left: 6px;
        border-left: 6px solid #0087C3;  
        }

        #notices .notice {
        font-size: 1.2em;
        }

        footer {
        color: #777777;
        width: 100%;
        height: 30px;
        position: absolute;
        bottom: 0;
        border-top: 1px solid #AAAAAA;
        padding: 8px 0;
        text-align: center;
        }
   </style>
</head>

<body>
    <header class="clearfix">
        <div id="logo">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAH4CAYAAAA8UVUTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAPGNJREFUeNrsnU9wHNd957sHpETqAqEKVUxlkyWWStb2btZSAnqdPQkH0H9iS2LsgE72QpzActVWCTfhJvhGnEKfvMTF0B42MbG2JcdrOyGrDJ422QgxtZvEdhLB4P6rYhXLEGyRoICZ7n2v5w00HHbP9HT3635/Pp+qqZFAoGf69Xvv9/39fu/9XhjHcQAAfvHfXr7wrHj7T+L1c/Fa/nffufkerQLgFy2aAMA7478q3nbF6yXxuiz/W/0MADwiJAIA4I3hvyjeronX2YxfuauiAW/SWgAIAACw3/DPiLcN8Xox55/cFq9FIQR2aT0ABAAA2Gf4ZZ5/VbxeLXiJr8q/Z30AgJuwBgDATeO/HHTz/K+WuIz82111LQAgAgAABhv+uaAb7j9b8aXl+gCZFtiilQEQAABgjuGfCboL/F7R/FFvBd2Fgru0OgACAACaM/wyzy9D9K/X/NFfkYKD9QEACAAAqN/4Lyqvf7Khr7CvogEbPA0ABAAA6Df8c8rwP2/IV3pHCYEtng4AAgAAqjf8M0F3W99lQ7/iG0F32+AuTwsAAQAAJfnLlz/Vy/Mvx0E8afjXlWkBGZ1gfQAAAgAAShj/ReX1J9v6hACw5atTVhgAAQAABQz/C8qTfqJ8r0UiQHJbCYE7PFUABAAAZPBX3XC/NPyXh41My0SA5I0wCJd/9zt/QVoAwBAoBQxgjvFfDbrle5NFfuEw5T70Xw30NIIwOXb4L7v3CABEAABAGP65IKN8rytRgAHBkpQV/t3v/MUWTx8AAQDgn+F/5VMzQe+Y3niYobdfBGRELG4rIbBLbwBAAAD4YPjTj+l1VAQMS1eof0mOHf4k6wMAaoU1AAD1Gv8qjul1jeTY4b96+VMcOwxABADALf77K0me/1o8qnyvY1GAUYsVU/41KSv8SdYHACAAACw3/DPBwDG9I0ecQyIgR/g/6x+TY4c/+RbrAwAQAAAW8devfPpZYZBTj+nNNeJi+wVAAe8/7R+TY4eFEGB9AAACAMB4478YJOH+7Lr9PkQBCnv/T/7CvooGbNC7ABAAACYa/rmgu7r/xTwG2WURUJH3P4jcNrgqhMAWvQ0AAQBgguGfCYYc06tDBNgsAMb0/tP+OTl2+N+yPgAAAQDQiOG/+OnuMb1xclTvZBGD7GIUQJP3P/grybHDQgSs0hMBEAAAdRr/i0F3df/ZPJbcJxGg2fsfJDl2WAgBjh0GQAAAaDX8mcf0miYCrBMA+b3/NG7L0wY/8dafc+wwAAIAoDre7ob7ZSGfy0WtuA9RgJq9/7TPlusDpBBg2yDACCgFDDDa+PfK914u48HqMo5OHBsclv3z4wskxw7/9SufpqwwABEAgMKGfy4ocEyvr6kAnYv/wmKfnRw7/Im3/nyL3gyAAAAYyfbFZFufNPwvxkUMtaepgIZy/3miHLfFryx+4s0/36V3AyAAANIM/xPH9MZF7bxnIqCmrX/FBMCH/5wcOyyEAOsDABAAAMfGfzHoru6fHM+oIgJKef85fqH0GoeUssJCBGzQ6wEBgAAAj/mbi5+ZC7p1+58vYm8RAIZ7/9kXeEcJgS1GAfgKuwDAd+P/Q/F6vrjxK+PZur8rIGz474dc4Pmwm+4BQAAA+EwZL9ZnEVDqWmGDnw0ACAAA7SLA2/Y01vsHAAQAQPOW0MsCQQ17/2gDAAQAQD1RAANFgM62wvsHQAAAIALq8GpDqxoY7x8AAQDglbJoxGiFWm4lNLGZsPAACACAHHYibCAK4GAqoMr7x/sHQAAAIAIqFgE2eP/Nhw8AEAAA4JrYseAzdO5cQBsAIAAAvI0ClDfQnixUBEAAACACEAHNe/9lL4D2AEAAABQSAUDZXwAEAADGjyiARd4/ACAAAEpFAdwTATVZTbb+ASAAAGwXAb4aG8r+AiAAAPwWByWsjcu1AfD+ARAAAM5HAXwrEORq2V/EASAAAKBWEeBUG+H9AyAAAPLyP7/4e8+K16K3wsKiKICrZX9Nuas7X/jsC+K1IV7PMjMAAgCc5m+/+Hur4m1XvKwQAKQC9F2Ksr8J0vBflmNCiIBVZgiokxM0AdRk+C+Kt2vidVZOzrFNHrv4wnGcbajiIXcz7F5NbwdXy/4a+tUmxev1d77wWSmMl5//1vffZNYABADYbvhnxNuGeL1oqYfWnJWKhxvnLOExUlhUoDwo+6uNs+L1bSEEbov3RSEEdhkMgAAAq/i7L35OhjZXxetVJ+xxE1GABkWAq2V/LRIHUjD/TAiBr8pxJITAe8wqUDWsAQAdxn856Ob5X3Wphnsj6wEaNJY2ev9W96/0H0sBvfs/vvDZZWYWIAIAJhv+uaAb7j87jocMxT31Mn+qZQ2CwVv/LNYNcn3AHysRsPjxb31/i4EBCAAwgr//g8/NxHGywO8V5+2xa6mAMY0w3n+t3v8gUlj/UAiBt8T78sdZHwAIAGjQ8Ms8v/RKXh9mGF2LAtgmAkyzYk14/46JAym0XxFC4Cvi/drHWR8ABWENABQ1/otBN8//er9h1DnBg37DSNlfq3STHHu7PhfVAiIAUK/hnwu6q/tfLOrlmRIJCIViieO44N+6nwoYy1Dh/TclDuT6gK8rEbD6b775vS1mKUAAQNWGf0YZ/stFDaOREywioBbvv0k32VHv/7j/KqQg/6EQAm8oIbDLrAUIAChr+Ht5/mXlbZQyjKZFAcqKAF+g7K81bS4F+kUhBK6Jv7/2W9/8HusDIBPWAEAmP174/EVhHO8E3Vzj5Dh/68t6AFfOCqDsr419L/PuJtWYvaNKcAMQAYDchv+FoFu3/0Vf7plUQDPef9kL4P0P/fukrLAQAbKs8PJvffN7d5jdAAEAWYb/WWX4L1dhHFkPkH+ybrKZKPvrlPefhhTyP/rb7vqAZdIC0IMUACT8ZOHzq0F3W9/lCiacxwyj75Qxco0dG2yB99+o8DHf+08jOXb47774uVVGJRABAGn454KU8r3eGmuPUgGlQw+U/bVRjCbrA4QIWBTvi//6m/91i1ZBAIB/hn8mGDimd+Q2MVIBjYmAJrxgyv6a2z8raLKkrLAQAreVENilZREA4Dg/7eb5V4OMY3oRAaZ6boaVCba88I+r4f8C95wcOyyEQHLssBACrA/wCNYA+GX8+47phbCgl5VH7BQ1PE1sDTTSkDla+Een91+SV8Wld//+Dz7HscNEAMAxwz8XdFf3P1/a23QsCjA0927beoC6DDjev5GiqYJiS8mxw+qcj+V/9V9YH4AAAGv5h0svzQgDlnlMbxlDQyrAUAviyYmMeP9akY7CD4UQeEsJgV0GFwIA7DH8fcf0DjfUZTxKV0roOhUFqEAENO6Jhjq/m3/ef96+mkJy7LAQAsmxw0IIsD7AMVgD4J7xXxRvvfK9RnpTJtYGcGo9gE4jSNlf67z/CkRPUlZYpQaACACYxj9eemkuzjimt0wUQGcqwKZIgDMhDcM9Ubx/Y7z/QeS2wa8rEbDK+gAEAJhh+GcCdUxvmVB2EyLANg/Mt1RA1ZaGsr9Wev+DJMcOCyHwhvheqx/b/O4us7C9kAKw2/hLwy/D/ZdpDf2GxL2tgfZ4/z72N4O8/zTknHPnx90S4kAEAOriny69dDHubus7O5bHSBQg96TKRgWzvP+y6yFCD9VFDfeclBUWImBRvC9/bPO7bzJQEACgz/AfH9Nb1FgjAsp6TG6lAvD+ze9vBnr/g98rOXZYCIHbSghw7DACACo0/KnH9II+T9+nrYE2e/+GeMI+ev9pJMcOCyHwhvj05Y9ufpdtg4bDGgDzjX+vfO/lcSbGMrnsMuHW0LLzf3W1oW9CqqkLsPivce8/jeTY4Z90S48DEQAYl3e/9NKceNsQY+1sHNfvxZIK0DmxmxUFoOyvW6Kr6XtWn56UFVYiYPGjm9/dYuQjAGC04Z8JBo7pzTPgMLflJixSAf5FHkwO4pgaYSrwvZJjh3/SXR8ghcAuMw4CAJ4w/C8/K2bn1SDlmN4y9fOJAtgpAqwxomU9UQ8L/7gsfIZcOjl2+KcLn0+OHf4I6wOMgDUAZhj/xSDJ84evFh20rAdwbZJvpjZApZYG7792719rsaWwknSOnON2f9rdOghEALw2/HJb30bw2DG92f7mKI+RVIBbUQDTUwGU/UX4FPxucn3A13+q1gd8hG2DRAA85eLjxr+ZwUkUoPy96qoS2JiT7nDZX1cDUxZ4/4M8r+ZAQABAnimOVID5XpYpk7re1f0ONbJJz9vQwj+AAIBaDU39IoA5u2SEwLKzAjKv63DZX7z/BsYF0wYCAOz2PsqKA1IBzYmAeoUrStI175/FvAgA8CQKQCrAPTsV1njTJhf+wftnnAACABGgUQTY5E3oinhYd2ywJdaCsr/2fS/EAQIA8DatnXhdSgWEFTw8nSVo2fqnp80N2fpnlfBBAIAVUwCpAHNFAJhkaPD+8f4BAWDtJIcIsO55OpYKoOwvwgfvHwEABg94MCsKYI0IoOyvfSLRvsI/gAAAHcPKxiiALyIAYaq3gfD+/RI+gADweLJFBLgWXWg8CmB54R9bjaCpoidPf2XMIwDAQBEAZkYBjCwQFFbRF5v1/v0c//4KH0AAQImBTxTAThFgqqWh7K9b3n8tzwx1ggCAcsaaVAB9pZIoAN6/d/MKxhkQAI6LAOblkp6MT6kAA71/GzzhZjxsu8v+IjAQAGC6F+BRFMAlEWBV42o3NMwBdRtnmhwBAAZFAUgFeNaPKo4CUPbXPu+fwj+AAEAEVHRdXwynO1GA0IH9I3j/eP+AAAAPDSMiQL+RCyv6naIXwPu3T/jg/SMAwMMoACLARkMSWnt9vP8G2hzvHwEAiAAGuDtRgKYODNLdYfD+7RM+eP8IAMAwWj8Rsx6Asr8+ev9anxlTKAIA7I4C+CQCXJ/QKPyD9w8IAGhY2buWCnAmvx7qKb7UVBTAFO9f93jB+29KnIRoRQQA6BjgJg0gG/N8Qw1KiXu1LRVggvePMajf+2fxHyAAHB3cpALMFQE+eqJs/Ssy1ij7CwgAr6MArAfwbNK3IBVgmvePnbHP++eRIQCgogmawWRfFMA0EUDZX/vmAwr/AALAC7WuZygTBTBXBPhj6PD+8f4BAQCkAqCRKABlf31zCvD+AQGACACjowAmrAdwueyvqYbK5bK/zEIIALDMm2jCgCEC6rpfc2d0vP/6vf+mBQYgABjgHqUCfPPA6pxQy0YBXC7766P33/gzY2pHAIC5IoABXHKCtC4VwIzuk/ePdw4IANAzuXgUBbBNBDThiZpc9hfvX9Mzo+wvAgDsjgL4lAoYfa9uTFtamp6yv155/zwzBAAgApgMLI0CjPOnLhf+wfuv3/sHBABgGJ2LArgsAkz1/rEz9nn/FMNCAIBHUQBEgJt9zXbv324jjvcPCABEgAMiAM/LgSiAhd4/hX/M8v4BAQAYRqejAM6JAMr+4v1X1Q95tAgA8DMKQCrAvImQsr94/3j/CABABNQiAvzx4PR4SbVHASj765T3b2q/BgQAMFk6FQUwvUAQZX99nEfw/hEA4JkBIwpgowgw2lqAcd6/yaKHroQAgKBs3phUAH2lmsmesr/0pyrblRZHAEDDIgAV7l4UoLFFlY6W/Q0t7n86RU/oaZsjAMAbA1Z0mJIKMFcEuOb9l39+mJu6vX9AAIAhUQBSAb6JvtCozmly2d+QPqBJ0DH2EQDgiAgAl1MBlP21s8/h/QMCABqVJaQCzBUBtgvaKoygj4vUKPwDCACiABVHARABvhmCPA8J7x/vH3GAAADPRQATs5lRgCZDtRT+wfsHBABgGJ2aGLwSAY6W/XV165/JAgNxgAAAh6MALDQ0w7iZ0D/x/j18ZjQvAgCqnexcSwWEHjyzUpOwSVEAR2d0Cv/Q5oAAcGbgcx/NeIs+rweg7K9/wkfnjgoeGQIAHIkC2JgKaEIEuNrpKPtrn/dP2V8EABg8ASAC/BJ2JkcBKPuL92+K8AEEACIArI0CGCkCKPvrRb+tTtAxZyEAwHAPgCiAjSLANKNG4R/7vGDK/gICgCgAIsAz767uAkGU/cX7N0n4AAIAEYAHZn0UoNZUgIdlf10eEZT9BQQAVGTA9EQBEAFVCDvbDU2znjDeP94/AgC8jwIgAsw0KrWkAjws+4sNwvsHBEDD3lMxw9lUKoDB3UwUQF8qoFkjivdvn/DB+0cAgKcGrOjUQCrAXBFA2V9/vHPaHBAARAFIBeRsQ989TFfL/rL4r1i74v0jAAARgNJ3LQrgmBHF+6+/XZgTEACgySC74oP4FAWwXQS4WvYX71+fwMD7RwBAzYOHVIC5IgBDU38fwPvXJejw/hEA0FgUwCURgHAzOwpA4R+8f0AAgGEiwBfD6FIUwLX1AJT9xfuv9pkjPBAAUHogkQowVwTYJPrw/vH+aVcEABgaBSAV4JZwKzPB1h0FoPAP3n/dzxwQAIgABpX1UQATUwF19tkmnxvDxVTvnyeDAIBaDRhRADtFgO1GFO/fwmeG948AAPuiAIgAzyb60O+WZ/GfjW3OfIAAAK0GGfR5T66lAij765/woewvIACYHLyOArgkAnz0sPH+628XvH8EAFgQBUAEeNZPCkYBKPuL91/tM2MsIgDAehHgT9u6EwWoej2Ay2V/Tc2xu132lxkJAQBOiw4bowA+pQJCS7x/zIxfwgcQAGBYFAARYGE/qWiCd7nsL95/E88c4YEAAKdEgDdG1bIoQB1bA/H+/fL+Md8IAIBKvTybJljnRIDB28D0LlLD+zfxmQMCACyMApRLBbjlZfmSX6Xsr41inbK/gABww9CQCrDumZkYBfDN+3e17+H9AwIAEdCgd8FkW9aAmSQCXBVgrob/zRa9qAMEABhjkEkF5Jt0mxABdhhhyv66JHya9P6RBggAjHUDkzEiwD7hVkcUwOWyvyz+w/sHBIBXIgDsiwLUKAL24iC4b0pfw/u3T/jg/dvNCZrAfY8yjrMHYFxCdMQZfz3sM8t+ri7DEw/5wsO+76h7GXXtQEP75+DRB1EUd6J4Sj2vg4kwMROnmjIGeP8NtQvePxEAcDsKQCrATuGmox8dRdH+QbtzShj/072fCX1yuh3Fp6I42G/K+8fMeCZ8GOIIADDHIGNw3U4FdOL4/sN2WwiAeDIrbiB+Z1IIgWDctEAV/cvHsr+NiwOtIpUJBwEAiA5EQGMioOvdx/uP2p3gg3ZnWlr2MBj+SoRAFE93ummLfbx/P73/kGfmBawBMHTg152bb2o9AGjj4LDTCYVHP1nkj+Uza8fxZCsMH7XC5OGebsL71y3q8P51eP9ABABKTU545UQBikYBjqLo/oN2O8nrl22PKI5Pieuc7gTx/Uaeh8FGkMjD+FegzREA4ODAJxXQvAgQxvreQbsTHHai6aq3WURRPH0URzLSc88W79/VsRiWbFe8f0AAEAXwXgS4Mo8Jo7x/0Gk/FMb/TKQz3yLTAlF8ph1HD4Ng+I4BE56PTiOIE0DLugBrAAwRAVl7xW1bD+DDMxnVRrpqA6Q8s4PDqHN4GEWTdc694is8I6MBE2G4PxGEJ8WPnqnb+2fxn33eP9KACABomAzNmrzciQKYnAoQXvi9B+2j08fGvwHktsGjKHpGph5sMoJ+e+h4/4AA8GbQkwrQ3b61G917B5128KjTOWNClCZOxEh8JqkfEAf3TH9eppspCv8AAoAogNMiwJVnUnbSG+fawrjK/fwHD9vtMx0D91XK5YFHcXRGiIGDIMf6AAr/mCV88i0sxMS7BmsADDQ4seMb502sDTA0p9/weoAPOp39oyiatKFXRHF8+ki8WnJ9QBhO6nhOeP94/0AEwD9x4FAUwKf1AEUnblm+90FSvreb5w8teSkhMKnSAvfx/u0XPnj/CACoMQrggwhAuKUjjeaBMPziNW1zNEjuQGnH0XS7ew/3625Hn8wUZX+hCKQADB7QOrYGwugJq8FUwKMPOp1YePzTLrWpbLOjOJ5uheHBiVZLNsUpelp+Q0nZXyACABVNNEQBSk3GmrYGHnU69x8ctU8J43/a1b6XrA/oRKeiKL2ssMnV73wUHmWvUH5hISAAPB58pALcRxrD94+O5La+6diDuE4vLSCEgIwM3KcHUPYXmoMUgAGDv+gqcV1VAml7/akAdUzvZCeOp30VSO0ong7DODjRau2Luy+1YwDv3yzvH3GBAABbJ4YGjg32SAQcfNDphMIDnqSndfuZbIuJMHw00QpTjx2m7G8x4VPFPNCcuIA6IAVgwSRAKsAVjze698ujoyQPTq9/nE4cnzqMotNFygqz9W/8v8f7BwQAIsCpY3zrnijzLgiURu3B0VEgT+tj+8awcED3tMGjSNYV7JYV1msE7e7gFP6BspACgMKDnFTA8FSA+On+Qbt9Uho1eswYOqCbFjjTCoOHJ1qto2HrA/D+9Xj/Ogv/4P0jAKCAUXXp2GATva8K6ys8PIyio0edTq3H9LpGFATPiHYMTnTLCo997DCFf/D+YTikACwbQKQCzHsu/c9EePv33j86eubY+ENp2nE8eSiPHQ4eP22wXPW70No5QPvfa2wa5hIEAHgkWJwy9MO91XsP2+3gYad9JqJbVI6MzBxF0WPrA/D+mxivbLd0CVIAhhohX1IBNrb/Y4ZJ7ufvdJ4SHip5/hqQ4kq29UQQHpycCAuVFcb7x0MHBAAiABFQ6rnIY3rFa5KF/fXTCeLTUacTCA2wf6LVmsQTNd/7R1wgAKDKAZ3jLHmoXoDJY3oPjtrTURBPYnAaFgJxPCmFgBAB91thOD36udrt/VP2F6qENQCOKnbbCgQ1I6DGa8Mo7tbtf3B0NB2xod8Y1PoAzhdoyPtHXBABgAY80VFRAFIBlXHwqNMJP+h0pumR5iJF2WEUT8uywidbrSfKClP4R4+BxrYTAYAG1XuIvNY28QnP8v4vDg9PC+NP+V5LkGWFxfM63bEoGkDZXyACALVHEIgCpN+LNB4P2+3p6Pi0PrCNdhRNd8STOznRut8KQqujN64W/uGYcCIAoDkKwHqA/Mjyve+328H7Ms/PAkvrkQL2sNOZPow68r/3ffP+y3voeP9EAMAKb17XegAv2jYMDh61o8NHnTYV/BxEijm5ZfNEq7V/Imw9FaQcO+yi96/T/uL9IwDAC+HhdirgMIrvHRy1z4h7PM3Tdpt2FE3KWMCJVnivFYbP4v1rFdVgOKQALIwCFPUWSAV8yPbFT8/84vDoP4hX8PDo6ExMfMQb5LOWZYUPO9Hzoh9syL6A9z/eFfD+EQCACLASMeGvirc7URx/kTy/30JAcFn2BdUnnJoH8NBhFKQAoCJPxfxUwN9c/MxF8XZNCJmzPDHoQ677eF30j0Xxvvw7b/7gTdedBN1XGB4VRHkgAKD08KRAUG7D/4I0/OL1Ij0HhiCF4bdFf7mthMCdZkW1mWV/Md8IAEAEGN8+YiKXi7xWxetVeguMgRSKPxL956uy/wgh8B7efz3iAuqFNQBQsddihucgJu9l8baL8YcSyL6zq/qSMd6/zjGqW1wQ/icCAI5HAZpMBYjJek68bQTdcC5AWeT6gD9WImDxd978wZbJHj7ePyAAEAFGiwAd/Oj3PzMjDb/4XPL8oAMpKH8o+plcH7D429/+wa6N3n9z0gDvHwEAUHEUQEzIMs8vvbPXaVmoASkwfyb63VfE+zUhBN6re+zo89Dx/n2DNQAOGdWiXoWtBYLEJLwYdPP8GH+oG9nndlUfNML7p+wvEAHAsx46ubiQChCT7lzQ3db3PE8dGkSuD/i66I8yArX829/Wuz6Asr+AAIDSHkZsafU7leeXhv8VniQYhBSicn3AW0oI7Nrn/VP2FwEAzkcB9P2tvihAX55/WXldACYihemc6K9SpFa6PoCyv6AD1gA4KgKKehpNrAcYYfwXxZusyPY6xh8sYFL11TtVrw9owvsvKy7w/okAgIGRANNTAWLypHwv2IzcNvh1JQJkWuBOE6K8rPeP+UYAAOJhrChAmVSACvdLw3+ZJwEOkJQVFv36DSUE3qt6rJrq/YP5kAJw3JA34XUUTQWISXI16G7rw/iDa8g+vav6eO5xWHoOoOwvEAHAmx8mAnRsDRyHO1/4bPeY3pBjesFpkvUBor8vymjAC9/6/pt5xi/ePyAAwDLhMToVICbCmaBbt588P/hEcuyw6P+3n2q1/uMJAy0p3j8CAIgCaFkPID/qQbv96+I/f8bTAY958TCKXozi+P7JVmt6MBVA2V9AAIBTIuBRJ3r0sN2WPzjHUwEIgnYcT7c7nYOnWq3wRKt1qg4PHe8fEABQ3yQXxcH7R0f7wvJPMj0APMHpoygS4yTaPzkxMVk2LUDZX8gDuwA8iwIEI6IAVXsFnVga/vb9Xx4dyTgAhXwAhiDHyGGnE3zQ6dyPtXn/lP0FBAAiQKMIkGH/g05nf//wMBCezTQtDzCWcJ4+aLfl2NkfN/2Ghw55IQUAlXMYdQ4eHLVDPH6AcggBMClej56amIhPhOFpyv4CAgBKTwE6FgS24ygJ90fCe6GVASrjlEwLHAXB/acnJqYnwurP3CgvDQABAF6KAGHwZbj/npikzoj/nWYiAdDC9AdCCEyErXtPTbTOhIVGvR7vHxAA4CGPOp39g3b7pPjPM7QGgH46cXTmoB09PNFqHZ1stY531VD2FxAAUEsUoB1HD98/ah+JfyfPD1A/z7SjSC4W3Bci4OSJMHwG7x8QAKBVBMhtfQ/a7Xti8sHjB2gYKcDl+oB2GN57emLiTCssNgfg/fsJ2wAh30QjXu8Lwy+39WH8AcwiiuMzctvgB53o3pNBO7x/IAIABaMASZ6/03lKeBsYfgCDkeJcvA5OTkwcPtUKJ/OM/Sa9f7QFAgAMFQGyNGmvfG/XE2C4AljAaSECTneiYP+piYnJiTKLAxnyTkMKAJ5AbuvbPzykfC+AxcixK7cNPmp37qct5sX7BwQAPMbDdvv+e4eHSSlSWgPAAUEfxNMHQggcRtH94ud6gouQAoCER51OckyvmB4w/AAO0o6i6XYU5D52WGfZX7x/BAAYgMzzP2h/WL6XgQngNOrY4fj+0xOt6RZJfq8hBeApMs//y6OjJM9P7X4Av4iDePqRXB+QHDscj+X9lwXJgQCABpHH9L7HMb0AOALJscOdsY4dpvCPO5AC8IjDKFLH9FK+FwA+pP/Y4ZOt8DQtggAAR5DH9D5od+51qOAHANkkxw63I1lWuHUmbX0Ai/8QAGAJHx7Tmxj+M4TuAGAUYto486gdBROt8N7JVqvQ+QKAAIAGkXn+R+0Ox/QCQCE6UXymE3UenpyQxw6Hk3j/CAAwnKMoevigLY/ppYIfAJTmmaOO3DYY7D890To5EbaeoUkQAGCaWlfH9ErVTmsAQJVIh0KmBcIwuncqOXZ4PH8e799M2AboANLw/+LwKMD4A4BmIXBGbht81OnciykrTAQAmuODqPNIDEY5Cs9Q0AsA6iKKYykEcpUVZmpCAECFdLpV/PYDTuoDgOY4fRhFsr7I/qmJickiZYVxXJqFFIBdqvu4fC/GHwAMYVKWFT5IKSuMfUcAQAUcdNr3fyEMP8f0AoCJxKqscPfYYbx/GyAFYDiyfO/DdlsOFQw/ABhP99jh6NHTE634RNiirDARABgXmecXHv99YfzlADpFiwCARZz6oBOdftjp3I9qPm0QEADWEnX383NMLwBYTy8t0D12GBAAkIkYJPvC63/IMb0A4BJy7dLDdnusY4dBP6wBMACZ5z/odA6FWmZlPwC4PNdNHsXx/tOt1lMnOHYYAeAz7Tj6lQN5TG+cVPBjMACA80hHR24bbHWPHf4VWqQ5QvEwaIUG+fHC51fF23LAvn4A8AdZyOzaxza/u0pTIAB8FwEz4k0OhMu0BgA4zhtyvhPGf5emQADAh0JgTgmBF2kNAHCM28rwb9EUCADIFgKLSgicpTUAwHJkuH9ZGP4NmgIBAPlEwLNBd23A67QGAFjKV4Jurv89mgIBAGPyk+76gGvi9QqtAQCW8JZ0YD5Knh8BAJUIgTklBJ6nNQDAUN5Rhn+LpkAAQPVCQKYFVgO2DQKAOcg8/6ow/NdoCnugFLBlqAE2I15fpTUAwADkXDSD8ScCAPVGA6QQ2AjYNggA9SO39S2S50cAQIP8tLs+QAoBtg0CgG7uSsP/EfL8CAAwSgisBpQVBgA9JOV7P0L5XgQAGCsCZP0AmYujrDAAVIUs37v8EfbzIwDACiHwghICrA8AgKLcVob/Dk2BAAD7hMBFJQRYHwAAebmrDP+bNAUCACzmHy691CsrzPoAABjGvnIYrv3LG39GuB8BAA4JgZmAY4cBIJ3kmF5h+HdpCgQAuCsE5gLKCgNAl6R8rzD8WzQFAgD8EQKLSgiQFgDwj31l+DdoCj+hFLDHqIE/E3SP7AQAf5BjfgbjTwSAVoDgH7vrAzh2GMBtkmN6f5M8PyAAIEUIzAWUFQZwjaR872+S5wcEAOQQAhw7DGA/yTG9wvBzUh88AWsAIBU1YcwEHDsMYCvJMb0YfyACAGWiAVIIbASUFQawgeSYXvL8gACAyvinSy9RVhjAXJLyvb9x488o3wsIANAmBFYDygoDmEJSvlcY/lWaAhAAUIcI4NhhgOZ5Q3n91O0HBADULgTmgu5uAdYHANSHzPOv/gbb+gABAAYIgUUlBFgfAKCPu8rwb9AUgAAAY3j3Sxw7DKCJ42N6n/sG4X5AAIC5QmAmoKwwQFUk5XuF4d+lKQABALYIgbmAY4cBivKOMvxbNAUgAMBWIbAYcOwwQF72leHfoClAJ5QCBu2oiWwm4NhhgFEkx/Ri/IEIADgYDXhZCgE5ubFtEOBDkvK9z33jO7s0BSAAwHUhMBdw7DDAXWX4t2gKQACAb0KAY4fBR5JjeoXh56Q+QACA1yKAssLgE19Vxp/9/IAAAFBC4AUlBFgfAC4i8/zLwvDfoSkAAQCQws6XXubYYXCJ5Jjec9/4Dsf0AgIAIKcQWA0oKwz2kpTvFYZ/laYABADA+CJgJuguEmR9ANiEPKZ39Rzb+gABAFBaCMwFHDsM5nNbGf4tmgIQAADVCoHFgGOHwTzuKsO/QVMAAgBAEz/7w5d7xw6/TmuAAcjyvdf+xZ+yrQ8QAAB1CYGZgGOHoTmSY3qF4d+lKQABANCMEJgLOHYY6uMdZfi3aApAAACYIQQWA44dBn3sK8O/QVOAC3AcMDiDmphngm6pVYAqkX1qBuMPRAAADGf3D1+RQkBO1mwbhDIkx/TO/OlbuzQFIAAA7BICcwHHDsP43FWGf4umAAQAgN1CYDWgrDCMJinfKwz/Kk0BCAAAd0QAxw7DMGT53mVh/NnPDwgAAEeFAMcOQz+3leHnmF5AAAB4IgQ4dthv7irDzzG9gAAA8FAE9MoKsz7AH/aV8LtGuB8QAAAIgZmAY4d9IDmml219AAgAgEEhMBdw7LCL3FaGf4umAEAAAAwTAosBZYVdICnfKwz/Bk0BgAAAyMXdD9cHcOywnSTH9J4lzw+AAAAoKARmAo4dtonkmN6z5PkBEAAAFQmBuYCywibzjjL8WzQFAAIAQIcQkGmB1YD1AaYg8/yrwvBfoykA8sNxwABjogzNTMCxwyaQHNOL8QcgAgBQK//rjzh2uCGSY3r/+Z+Q5wdAAAA0KwQoK1wPSfleYfgp3wuAAAAwSgisBpQV1kFSvlcY/lWaAgABAGCqCODY4Wp5Q3n97OcHQAAAmM///qOLHDtcjuSY3l//kzc5phcAAQBgpRBYDLrbBlkfkA+Z518Vhn+DpgBAAADYLgI4dng0x8f0CuNPuB8AAQDglBCYCSgrnMYbyuvfpSkAEAAALguBOSUEnve8KZLyvcLwb9ErABAAAD4JgcXAz2OH95Xh36AXADQDpYABGkQZwJmge3StL8h7ncH4AxABAIDgeH2ANIqubhtMyveS5wdAAABAuhCYC9w6dviuMvxbPF0ABAAAjBYCth87nBzTKww/J/UBIAAAYBz+T7d+gBQBr1r21eUxvau/xn5+AAQAAJQSAraUFU7K9/4a5XsBEAAAUKkQMPXY4bvK8HNMLwACAAA0CoHVwIyywkn5XmH4V3kqAAgAAKhHBDR97PAbyusnzw+AAACAuvm///7354LuQsG61gfIPP/qP/vP396i9QEQAADQvBBYDPQeO3xXGf4NWhsAAQAAZomA3rHDr1d42eNjeoXxJ9wPgAAAAIOFwExQzbHDb0lBIQz/Lq0KgAAAAHuEwFxQ7Njhd5Th36IVARAAAGCvEFgM8h07vK8M/watBuA2HAcM4AHSoIdBMCNeXw2l8k9/yX+bwfgDEAEAAAf5f931AdLI97YNJsf0/ip5fgAEAAB4IQTm5PuvkucHQAAAAACAH7AGAAAAAAEAAAAACAAAAABAAAAAAAACAAAAABAAAAAAgAAAAAAABAAAAAAgAAAAAAABAAAAAAgAAAAAQAAAAAAAAgAAAAAQAAAAAIAAAAAAAAQAAAAAIAAAAAAAAQAAAIAAAAAAAAQAAAAAIAAAAAAAAQAAAAAIAAAAAEAAAAAAAAIAAAAAEAAAAACAAAAAAAAEAAAAACAAAAAAAAEAAAAACAAAAABAAAAAAAACAAAAABAAAAAAgAAAAABAAAAAAAACAAAAABAAAAAAgAAAAAAABAAAAAAgAAAAAAABAAAAAAgAAAAAQAAAAAAAAgAAAAAQAAAAAIAAAAAAAAQAAAAAIAAAAAAAAQAAAAAIAAAAAEAAAAAAIAAAAAAAAQAAAAAIAAAAAEAAAAAAAAIAAAAAEAAAAABgPidoAgAAAPv48pe/vCDezonXknqXrInX3te+9rW1UX8fxnFMKwIAANhl+K/2Gf0s1oQQWEEAAAAA2G/8peF/Tf3vunjdEkZ+s+/fpShYUL8zJV7b4nVJ/M4OAgAAAMBu459p1Ad+/3rQTQ/I3zsvfn8PAQAAAPC4sZSe87sDP5be9QVDvt+8eLspjb/4TufH+DspGKRwWBd/d6X/31gECAAAYD7Sm5ce/AVl2JfUz9JYUQY/WQwofndWRgLE+1p/1IBtgAAAAAajvH8ZoVgbDONnID3+t8XfTfUJAslS/y8hAAAAAMxmXr2vp/2jEAVh76UiBHvBh9sDA+X1b/ddBwEAAABgATKEv53H+xe/c0sZ+0Hkz2f7ogIIAAAAAMOR3vxOnl9U+f7ZlH/qiYdjAcAiQAAAAIsRRj9tO58UDOspAiAgAgAAAGAHO8Hoqn/9yHD/4L7/c4NCAAEAAABgNjKn/1j+vp++BYCX1I/SUgByAeBOvyhAAAAAAJgvACQLw35JlQSWLykUrvZ+rooczap/CxAAAAAAFqAMu0wDvJYVBehD7vmXXv6Sqh8Q9ImBdQQAAACAXcjjfc/1GfNtZexXBsSCFApX1M/PqVLAMnKwPnh2AGcBAACA95h+FoD6jjeUMd8U3+tSjt/vnQOQehgQEQAAAAALUEZfrvBfEMb958rApxl+Gf5/u8/4X0grIkQEAAAAiABYEAFI8exHIXP+K1kVBE8U+JAr4mLrmm6qd9zhYzcweIRhRQ926HXVd5GrJo/rKSsuqFKLVd3zVN/1ZXunLfCQuZ5NpQDXqr7XCu9Fttd8Spv1I7//nhpY2w0M6FHPfZznsadrLJS45yX1neWzWBgyKSS1wavsyzb2kQruR7bxjYEfr4wapyU+7+3g8S1eYx0NW6AvDZ4299yoM+h96wuqD5xT9zKf8it76j56889e2c9UJ/ytq3ZbGOgTvc8b2WZUAiyurKr4nMEHl8VxaUfxd1eV8Vmve/IecS9ZxnKQ1/r+bkfdx5ohg3gpYwAPex7XlUFdq2piLCicF4ZMooMs9f1tYrCqmphc7iMZE/Gm+o79RVoW+ib8qsXT4Hwh94bPajKOCyne8A594TFHIY+t6N+Sd1X87ba6j/WSfa9n6Au3B2sA+gyAzKnoNv4qN9P7nNkSA/OmuM5N5eU25m323ctUgUucUwPi58rbaOIe5AR6U3lx8wUvI7/7u1n5uBq++80xjH8a8vn9XMf3d6GP5GA9zSjXYJBH/bzMczuXMh426QvHAqaMrZB947q4zrvK8WgMIgA1ef1KMZYxMmnMK8NzSe0TrVP9VnkvU2pAyOtd0emJpnj9Nyq8pJyc5uvIGWrqs1dVm1wo+wxc6SM5uZVhlKv2ypeGCIAVzWJjr6gAcGi+qPo+pKC5Ia6ba0U/AqD+iXQzZRDvFPiMWdVxRnnr60OuPyynKzvRSh2hsZz3sjlk8psd4cmcU4Jmp8HnvhNknLutGBa+nJdeuS4RMMYkNKwvDUs9yZ+/LT7nQtFn4EofyYsMv6uw7qwuo6yEWVafO6eEZ5UpwUGxsVnE0Do0X8jvf3PIfWwPEUjD1jgk96HWdtTep70WAGpQXU2b/KsypmoA3BwyeDfV4Noc4zun5aqvqnzuZg2DYCrLaOZtN2WAl1IGVNJe4t/P61L2Gc89CPLnwtfUc30tY3KSIuCquM5Kxd97SrX/7BBPdD1HX1obkcPseSZjRwJc6SMF2Bx4LlUb5VGCbyEjElGkn82ntPmmx/PF1BARk3fOuDJinVHvPi7UKQJ8jwAMTn6Vrpbv6zhZK8lXxp0gerWe+4zYuYH72dE4CLIG89jRB/X7axmeeG/i0LG6WV77eorhvDTOBKIWXV0akkaQJTtvVeyVXc8w/jvqGWyO8f33VPuvq+supExI8ueXPOwjRVhP+Y6VGGXVrqNyxQsqCliFERw0UDvj9mPH+kLauBvbVvTN3fPqmufShHedfdrnRYCDanJFw1a56xmqUXr858sYB9WZzqdMMNc1iqW0e7lQJlqi/laGywcnrllNi+rmByYlOZAL57zVc8gK91f2/ft2jaR5/eeLrgGR963yj2nRinNjLjJ1pY8UascUL3khR932PKSF/wfbM49IGGduHBQ3vs4XaSmIS2VshZr3z2eIw1m10wsBUCNrVefP1SrVtAG5XtWCDzV5XxjoSFNVN45SrEsZg/lWBfdxK8OIXtW8y6GSiI/6/mkGdL6K1eCqDdImhe0y4iVlYl0ZEMTn84YjHe4j47CpySgvpIi+tRQjuFBBX0sTG5sezxdTKcZ/s4J7SJu7jx0HTbtIEAAp7FSdqx3i/W1rKsgjBYXOvFHavaxUGd5WIfUVnV50ynO/UuH3X8t4BlUYgLSozs6QyEOZe1gr6KW52EeKRIMGjXKpFeNZ2/EyIg7zFRjAKvb+u9oXVjTstsqau2uJAiAAuqcmVe0xpy1W6T1sHRPPnq5rKzU/nzIprGm4j7UURbykycPTIfrSQqVlDcBsxjVWdCx6kmK4QL7X1T5SRR8omwYYth1vs0rBmbHWYJO+cOy86biHvQwbNF9HFMB3AbCjqZpeVuhfm5euFLGOsrRpoTyd2w3XNHnRg89dx26JtGvOamj/W3XWffC0j1TZB5YqbNvj7Xhq7trR+FlF9v672hdWdN2Aeo63mujTvguAyidRpaLnax4EwzzQspReEVxgMGxrHgjrmr77TsqEHJRU8gsN9SXf+0gZIV7Jd8u5HW+wL59Tf1dFXyuy99/FvlDHmRlrFQtHBEAOdDzU+YwOpH1vZ8bkU0bMDK6Y1yKacjyX2YpWU+t87sfPOuVnUwXbfzblb/dMOQPC8T5SpWNRtDTwQg5jWknaKeOcgU36Qj33oJ7roNia0p0G8F0A6DhAY7Zmg6Pzs2ZrarM8nzFf4WDTeQ9pHlPRnOR8w33J2z5SktJGOSMfv57Sl9PC9EsV9LUinrurfeGWLf0GAZCfHU2Vo9Im+zrLO+5ovpemBvQ5A9snrwCwrf197yNlBWYVRnmc7XibKZ7juGHwKvb+u9gX9mo8hjht7tAa1fK5EqCuEqJTjguAd1XJ4bqZMrB9EABu9pEq2Bzw4MctDZx7O546knhv4P4Xgpyh64y1BrfoC7XP3bWLWp8jAK4KAFeZogm09lv6SLVRgLSaALm88oJH8ZbZfjj4vbZr9HpN7wt7DY9trX2aOgAOPESMrlcGbY8+Yg1FjfITe/+FQR4Vki+0/bCKvf/0BXvFPQKASRJvEzFJH9HDrZRnl2dR19j5+BLbD9PWGqzTF/ywEydo81omxDoXJ+muHvWcKeewM0kYC32ka5RvCQ97Z2D8D83NlzyKd/BI4mT74YhwfhV7/13uC1MNf5bWtmsZ1iDnHBj3Ta9OrvL57DQgMMCu9qePDGfcNECZfPx6MMa6g4yiZZv0BaOcN61RFdNSAC7kkNIGQZ37kxc034sLIs3mvnSO72gV4+bmC+fjC2w/fKL0b8kS0y72ham6TuYzMQJQdw7SBc8h64xn7feWET6s+l7mA2iyLy1Y8B3pIx8a5Z2UNlrIGL9V5OPHqQmwUPKzfOkLdd1DmljTuhujiACYdaChdQ74vaD6A0Ga8P57C4ueON7UoJPXXDceae0/VaLWe13fkT4y3CjPZrRP6Xx8xgFB8ylio3TpX4/6gnbRnVFGWXsRolECYDvDm53S0AALgTvbSFIFgM4ogOpAOkRGmlfwWgB1YUP700dGzwdDc/MVb8fLs+5A195/F/vCbA2i+7WcbVmfAFDhqyc8EE2e+pIro13l0dJyNzc0iSd5zes1Tu5LNebFEABPMm9SFIA+MnI+yJObrzIfv5kyZy+M+LxN+sJQrmp23uZzOpK1RgAyvVkN3r9recO086NlKOyGhs+6EWhabKNEYNqgvm7Q6WsuG4+djDF4XZOYXBr3uvSRQkZ58NjeQQO9VrLPZK47qGitgW99QUYBXtMw3rKct1t1VGPMIwBuZXggCxU1wDmd6qrhKMBmRtvdrGIwyGvIa9UgntImo1kdYkb2B/F6mxxy/WJSjWk5Gb1bYHzTR4bPB2m5+QV1P5Xn41P+vj8Xr3vvv6t94WpVdi+H83aljn7ZytFxs8LZ18uGddRD0+a9GsCVIHtb4NtlwriqI76dYvwrzxspVb+iU8z0TYQ31WRxE++xvvZXfbE3QcvryXTV9bwTK30kF+sZXnnug3/GFABPrDuoeK2BD31hsA1vVCECRjhvK3UVT8pbByDtYU6pRp8vMeHcHFC+m4FDKFV9IcjeH3tTDYiFMdptSXWcNOEkn9O2pntZy4poFPQYB+/rNSVozvW3D+Y/V/u/Xab9VduntfXsOBMRfWRsr7y3Ra/yfPyQdQdp5wxsMl8MfWbbKSLgeonvPp/hvEm2VdvVQhjHcd4vfSPI3g4hle1anslCqbbX0jqieJ2XnWPw2uK6V0p2lnMp15Uq+0Idjaw+/2aOSMd6kF34YTYYvh1FqsY1KRCCJ3NKpduw716GpRy21WetjzmQXwuyi2BcKpoLa+K5q/sZTGldGadNhnkNKaL5sXtT7b85xnddyuiXsu0vFPFEbOojdZPSNoPH+Mr/f66KkLyaa99Oaa/+5y3n7ZUa79e2+SKxbUPm7xV1D3s5rt8Te8Pa40LF6ZihjHMWwBXVAGmTj7ypJVX3etjDvDokzJJMNg2dHa3be5P3dV4Z5mFGvOjiypW6VKM0nkPEoOwb15U6vhUMP1N8aYQgKmyAHI4C7Im2vaCiP/MZ3tW8GkPDxOR8MHzdyJ6aSHfoI1o8yv62HzRkleXjpSEU7bw9MGcXPWfA2/lCzd+XlAiYSrFpV1U7Z7XlVDB6K2Ttxn8sAZBj8ul1rnEX9PWMv+3nT49sPzmpKhV4Nahm3cMtZfy3c7RxlfdyKcPTHcfIjLqvS3UPBov60YUc7V9UTG6XMf70kVwC4PqIf6/687IiRtt1zLsu9AUlps4r+zebIWaKromTou9SE52xNe7ko8KnKxV2zudcN/4DbSgf9nMqorJdot2kaMornPY03IeMODxX8YQlv+cVdV8Y/9Htf36E11QkknS+Ki+KPpIp4LLaY0ftFqiS9RrFhtN9QY4LOT6C6hZa96IWl5rqjyeKPkyhhjaVl1Fkb6Ts5GsaOrtNE4HsROsqr7vU57Vl5WPX+wbSMKZqvIcdFdXorU8ouk92W6ngtQDG8kpUNKBXBbLowqrceUz6SGWOz8KYxrqw4FBzdS2f50NfkOupxD2slbB/Y699MEoA9D1MOXGsqIVnUyPCOL2owTre3RMeQa8j6+rQe5rvYVt16hWV4jinXktDlO86/aGy9j/On/YVKxm2aLS3PmC7LhFOH3msLTZFG+yliHVdHnmaANhsqk1d6Atj2r/+Od6ovpx7FwDYQcaCm/M+pVkAAGA0LZrAOeYzFDQAAAACwFHvX4Z9B8OK24TYAQAAAeA2aTm0TZoFAAAQAO56/1mLaG7ROgAAMMgJmkC7Ye4V/bmiORSfdtLWLRb/AQAAEYD6jb9ckNc79+BtDUdJ9j5HVhZLq0LFvnoAAEiFbYD6jL9cjNd/WtWxVx6McWDLiM+Q15bGP23lf2UHAAEAgHuQAtBHVlW/3oEtveIQYxdjUav9l4LhhTNWeAQAAEAEoJkogAz538j568NOk+oxrFpWv/HnFD0AAEAANCwCeickLtTwcZyiBwAACADDhIAM27+mUQhcMeFwCQAAQABAthiQQqDM+dc9OEUPAAAQAJaKgd5pWJKshYM9NpXRDzD6AACAAAAAAICxoBAQAAAAAgAAAAAQAAAAAOAk/1+AAQDNrpClwcFTyQAAAABJRU5ErkJggg==" alt="" />
        </div>
        <div id="company">
            <h2 class="name">UrgenceWeb</h2>
            <div>848 MUTTON TOWN ROAD BATTLE GROUND, WA</div>
            <div>360-687-0425</div>
            <div><a href="mailto:company@example.com">webemergency.info@gmail.com</a></div>
        </div>
        </div>
    </header>
    <main>
        <div id="details" class="clearfix">
            <div id="client">
                <div class="to">INVOICE TO:</div>
                <h2 class="name">{{ $customer_name }}</h2>
                <div class="address">{{ $customer_address }}</div>
                <div class="email"><a href="{{ $customer_email }}">{{ $customer_email }}</a></div>
            </div>
            <div id="invoice">
                <h1>INVOICE</h1>
                <div class="date">Date of payment: {{ \Carbon\Carbon::parse($invoice_date)->format('F d, Y') }}</div>
            </div>
        </div>
        <table border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th class="no">#</th>
                    <th class="unit">AMOUNT</th>
                    <th class="qty">HOUR</th>
                    <th colspan="2" class="total">TOTAL</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="no">{{ $payment_id }}</td>
                    <td class="unit">€{{ number_format($amount, 2) }}</td>
                    <td class="qty">{{ $hours }}</td>
                    <td colspan="2" class="total">€{{ number_format($amount, 2) }}</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2">TAX</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2">GRAND TOTAL</td>
                    <td>€{{ number_format($amount, 2) }}</td>
                </tr>
            </tfoot>
        </table>
        <div id="thanks">Thank you!</div>
        <div id="notices">
            <div>NOTICE:</div>
            <div class="notice">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quas, aliquid.</div>
        </div>
    </main>
    <footer>
        Invoice was created on a computer and is valid without the signature and seal.
    </footer>
</body>

</html>
