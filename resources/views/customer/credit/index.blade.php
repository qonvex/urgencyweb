@extends('layouts.customerLayout')

@section('content')
<div class="myaccount-process-wrapper credit-wrapper">
    <div class="process-title">
        <h4>{{ __('messages.credit') }}</h4>
    </div>
    <div class="myaccount-projects-wrapper">
        <div class="hours-wrapper">
            <div class="row">
                <div class="col-md-4">
                    <div class="hours-remaining-wrapper">
                        <h4>{{ __('messages.available_remaining_hours') }}</h4>
                        <h2>{{ $customer->total_credits < 0 ? '0' : $customer->total_credits }} {{ __('messages.hours') }}</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent </p>
                        <button type="button" href="#" data-toggle="modal" data-target="#CreditAddhour">{{ __('messages.buy_hours') }}</button>
                    </div>
                </div>
                <div class="col-md-4">

                </div>
                <div class="col-md-4">
                    <div class="total-hours-used-wrapper">
                        <span>{{ __('messages.total_hours') }}</span>
                        {{-- <select id="calc-hour">
                            <option value="" selected>{{ __('messages.hour') }}</option>
                            <option value="day">{{ __('messages.day') }}</option>
                            <option value="">{{ __('messages.week') }}</option>
                            <option value="">{{ __('messages.month') }}</option>
                        </select> --}}
                        <h2 id="calc-result" data-id="{{ $customer->total_hour_orders < 0 ? 0 : $customer->total_hour_orders }}">{{ $customer->total_hour_orders < 0 ? '0' : $customer->total_hour_orders }} {{ __('messages.hours') }}</h2>
                        <p>{{ __('messages.over_all') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="myacount-actions-wrapper">
            <ul>
                <li>
                    <span>{{ __('messages.project_status') }}</span>
                </li>
                <li>
                    <span>{{ __('messages.consumed_hours') }}</span>
                    <span>{{  $customer->total_consumed_hours < 0 ? '0' : $customer->total_consumed_hours }} {{ __('messages.HRS') }}</span>
                </li>
            </ul>
        </div>
        <div class="table-scroll">
            <table cellspacing="0" cellpadding="0" width="100%">
                @if (count($projects) > 0)
                @foreach ($projects as $project)
                @if ($project->is_deleted == 0)
                    <tr>
                        <td align="left">
                            <h4>{{ $project->title }}</h4>
                            <h5>{{ \Illuminate\Support\Str::words($project->description, 5) }}</h5>
                        </td>
                        <td align="center">
                            <strong><span>{{ __('messages.hours_consumed') }}</span></strong> <span>{{ $project->hours_used == null ? 0 : $project->hours_used }}
                                {{ __('messages.hrs') }}</span>
                        </td>
                        <td align="right">
                            <span class="credit-status {{ $project->status == 1 ? 'in-progress' : 'done' }}">{{ $project->status == 1 ?  __('messages.in_progress') : __('messages.done') }}</span>
                        </td>
                    </tr>
                @endif
                @endforeach
                @else 
                <p class="bg-danger">{{ __('messages.no_project') }}</p>
                @endif
            </table>
        </div>
    </div>
</div>
@endsection
