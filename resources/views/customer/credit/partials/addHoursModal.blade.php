<div class="modal fade urgence-modal" id="CreditAddhour" tabindex="-1" role="dialog" aria-labelledby="CreditAddhour"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <div class="credit-addhour-wrapper">
                    <div class="logo-wrapper">
                        <div class="logo">
                            <img src="assets/img/logo.png" alt="logo" class="img-responsive" width="280px">
                        </div>
                        <div class="slogan">
                            <h3>{{ __('messages.title_description') }}</h3>
                        </div>
                    </div>
                    {{ Form::open() }}
                    @php
                        $hours = App\Config::all();
                    @endphp
                        <div class="row">
                            <div class="col-md-5">
                                <div class="hours-wrapper">
                                    <h3>{{ __('messages.no_of_hours') }}</h3>
                                    <div class="main-hours-wrapper">
                                        <div class="number-hours-wrapper" id="hourInputs">
                                            @foreach ($hours as $hour)
                                                <input type="text" name="" value="{{ $hour->hours }} hours"  readonly="readonly" data-id="{{ $hour->hour_price }}" data-hour_id="{{ $hour->id }}">
                                            @endforeach
                                        </div>
                                        <div class="hour-nav">
                                            <a href="#myCarousel"><span class="fa fa-angle-up btn-up"></span></a>
                                            <a href="#myCarousel"><span class="fa fa-angle-down btn-down"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-4">
                                <div class="price-wrapper">
                                    <h2 class="price-title">{{ __('messages.price') }}</h2>
                                    <h3><span class="price" id='hour-price' data-id=""></span><span class="euro">&euro;</span></h3>
                                </div>
                            </div>
                        </div>
                        <div class="payment-method-wrapper">
                                <h4>{{ __('messages.payment_method') }}</h4>
                                <div id="paypal-button-container"></div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
