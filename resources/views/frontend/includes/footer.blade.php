<footer>
    <div class="slant-after">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="logo">
                    <img src="assets/img/logo-w.png" alt="logo" class="img-responsive" width="250px">
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{ __('messages.address') }}</h4>
                        <p>848 mutton town road</p>
                        <p>battle ground, wa</p>
                    </div>
                    <div class="col-md-6">
                        <h4>{{ __('messages.contact') }}</h4>
                        <p class="email">webemergency.info@gmail.com</p>
                        <p>360-687-0425</p>
                    </div>
                </div>
                <div class="slogan">
                    <h3>{{ __('messages.title_description') }}</h3>
                </div>
                <div class="order">
                    <ul>
                        <li><a href="#"><img src="assets/img/PayPal.png" alt="logo" height="35px"></a></li>
                        <li><a href="#"><img src="assets/img/visa.png" alt="logo" height="35px"></a></li>
                        <li><a href="#"><img src="assets/img/mc.png" alt="logo" height="35px"></a></li>
                        <li><a href="#"><img src="assets/img/discover.png" alt="logo" height="35px"></a></li>
                        <li><a href="#"><img src="assets/img/ae.png" alt="logo" height="35px"></a></li>
                    </ul>
                </div>
                <div class="copyright">
                    &copy; <?php echo date("Y"); ?> Urgence web | ohm 
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- LOGIN MODAL -->
@include('auth.login')

<!-- REGISTER MODAL -->
@include('auth.register')


<!-- FORGOT PASSWORD MODAL -->
@include('auth.passwords.email')


<!-- ORDER NOW MODAL -->
@include('layouts.partials.orderNow')


<!-- QUATATION REQUEST SENT POP-UP -->
@include('layouts.partials.quotation')

</body>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/mousewheel.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom-script.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/toastr.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/backend.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/paymentFront.js') }}"></script>
</html>
