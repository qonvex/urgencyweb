<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Urgence-web</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.carousel.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom-style.css') }}">
</head>
<style>
.scroll-me .btn-order a {
    background: #60bd64;
    padding: 10px 50px;
    border-radius: 50px;
    color: #fff;
    font-size: 18px;
    text-transform: uppercase;
    margin-left: 50px;
    margin-top: 0px;
    text-decoration: none;
}
</style>
<body>

    <div class="hm-section1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="home-banner">
                        <div class="logo">
                            <img src="{{ asset('assets/img/logo-w.png') }}" alt="logo" class="img-responsive"
                                width="280px">
                        </div>
                        <div class="slogan">
                            <h3>{{ __('messages.title_description') }}</h3>
                        </div>
                        <div class="description">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit.</p>
                        </div>
                    </div>
                    <div class="scroll-me text-center">
                            <div class="btn-order">
                                <a href="#" data-toggle="modal" data-target="#ForgotPasswordVerification">{{ __('messages.verify_btn_verify_now') }}</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- VERIFY MODAL -->
    @include('auth.verificationCodeModal')

</body>
<script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom-script.js') }}"></script>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#ForgotPasswordVerification').modal('show');
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#resend').click(function(e) {
        e.preventDefault();
        let str = $('#token').val();
        $.ajax({
            url: `/resend/code/${str}`,
            type: 'POST',
            data: {
                str: str,
            },
            processData: false,
            contentType: false,
            success(data) {
                console.log(data)
            },
            error(response) {
                
            }
        });
    });
</script>
</html>
