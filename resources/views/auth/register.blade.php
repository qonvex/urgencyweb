<!-- CREATE ACCOUNT POP-UP -->
<div class="modal fade urgence-modal" id="CreateAccount" tabindex="-1" role="dialog" aria-labelledby="CreateAccount" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                  </button>
                  <div class="login-wrapper create-account-wrapper">
                      <div class="logo-wrapper">
                          <div class="logo">
                              <img src="assets/img/logo.png" alt="logo" class="img-responsive" width="280px">
                          </div>
                          <div class="slogan">
                              <h3>{{ __('messages.register_registration') }}</h3>
                          </div>
                      </div>
                      <form method="POST" action="{{ route('register') }}" id="register-form">
                        @csrf
                          <div class="form-group login-inner-wrapper">
                              <div class="login-email">
                                  <h4>{{ __('messages.register_name') }}</h4>
                                  <label>
                                      <span><img src="assets/img/login.png" width="28px" height="28px"></span> 
                                      <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="NOM" required autocomplete="name" autofocus>
                                    </label>
                                    <small class="invalid-feedback text-danger" role="alert" id="register-name-error">  
                                    </small>
                              </div>
                              <div class="login-email">
                                  <h4>{{ __('messages.register_email') }}</h4>
                                  <label>
                                      <span><img src="assets/img/login.png" width="28px" height="28px"></span>
                                      <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="EMAIL" required autocomplete="email">
                                    </label>
                                    <small class="invalid-feedback text-danger" role="alert" id="register-email-error">  
                                    </small>
                              </div>
                              <div class="login-password">
                                  <h4>{{ __('messages.register_password') }}</h4>
                                  <label>
                                      <span><img src="assets/img/password.png" width="28px" height="28px"></span>
                                      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="**********" required autocomplete="new-password">
                                    </label>
                                    <small class="invalid-feedback text-danger" role="alert" id="register-password-error">
                                    </small>
                              </div>
                              <div class="login-password">
                                  <h4>{{ __('messages.register_confirm_password') }}</h4>
                                  <label>
                                      <span><img src="assets/img/password.png" width="28px" height="28px"></span>
                                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="**********" required autocomplete="new-password">
                                  </label>
                              </div>
                          </div>
                          <div class="create-account-submit-wrapper">
                              <div class="terms-wrapper">
                                  <img src="assets/img/exclamation.png" width="14px"><a href="">{{ __('messages.register_agree') }}</a><br>
                              </div>
                              <button id="register-btn" type="submit" class="btn-create-account form-control"><i class="fa fa-circle-o-notch fa-spin loading" style="display:none;"></i>&nbsp{{ __('messages.register_create_account') }}</button>
                          </div>
                      </form>
                  </div>
                </div>
          </div>
        </div>
  </div>