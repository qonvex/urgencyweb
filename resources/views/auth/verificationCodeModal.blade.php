@php
    $last = collect(request()->segments())->last()
@endphp
<div class="modal fade urgence-modal" id="ForgotPasswordVerification" tabindex="-1" role="dialog" aria-labelledby="ForgotPasswordVerification" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
              </button>
              <div class="login-wrapper forgot-password-verification-wrapper">
                  <div class="logo-wrapper">
                      <div class="logo">
                          <img src="{{ asset('assets/img/logo.png') }}" alt="logo" class="img-responsive" width="280px">
                      </div>
                      <div class="slogan">
                          <h3>{{ __('messages.title_description') }}</h3>
                      </div>
                      <div class="title">
                          <h3>{{ __('messages.verify_verfication') }}</h3>
                      </div>
                      <div class="subtitle">
                          <h4>{{ __('messages.verify_description') }}</h4>
                      </div>
                  </div>
                  <form method="POST" action="/verification/{{ $last }}">
                    @csrf
                      <div class="verification-code-wrapper">
                          <input type="text" maxlength="1" name="first" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" />
                          <input type="text" maxlength="1" name="two" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" />
                          <input type="text" maxlength="1" name="three" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" />
                          <input type="text" maxlength="1" name="four" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" />
                          <input type="text" maxlength="1" name="five" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" />
                      </div>
                      <div class="verification-code-resend-wrapper">
                          <h4>{{ __('messages.verify_info') }}<a id="resend" href="#">{{ __('messages.verify_btn_resend') }}</a></h4>
                          <input id="token" type="text" hidden value="{{ $last }}">
                      </div>
                      <div class="forgot-password-verification-submit-wrapper">
                          <button type="submit" class="btn-forgot-password-verification form-control">{{ __('messages.verify_btn_verify') }}</button>
                      </div>
                  </form>
              </div>
            </div>
      </div>
    </div>
</div>