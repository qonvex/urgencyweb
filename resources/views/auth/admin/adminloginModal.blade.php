<!-- LOGIN POP-UP FOR ADMIN -->
<div class="modal fade urgence-modal" id="adminLogin" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
              </button>
              <div class="login-wrapper">
                  <div class="logo-wrapper">
                      <div class="logo">
                          <img src="{{ asset('assets/img/logo.png') }}" alt="logo" class="img-responsive" width="280px">
                      </div>
                      <div class="slogan">
                          <h3>{{ __('messages.login_login') }}</h3>
                      </div>
                  </div>
                  <form method="POST" action="{{ route('login.admin') }}">
                    @csrf
                      <div class="form-group login-inner-wrapper">
                          <div class="login-email">
                              <h4>Email</h4>
                              <label>
                                  <span><img src="{{ asset('assets/img/login.png') }}" width="28px" height="28px"></span>
                                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="EMAIL" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    {{-- @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror --}}
                              </label>
                          </div>
                          <div class="login-password">
                              <h4>{{ __('messages.login_password') }}</h4>
                              <label>
                                  <span><img src="{{ asset('assets/img/password.png') }}" width="28px" height="28px"></span>
                                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="**********" required autocomplete="current-password">

                                  {{-- @error('password')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                                  @enderror --}}
                              </label>
                          </div>
                      </div>
                      <div class="login-other-wrapper">
                          <div class="remember-me">
                              {{-- <input type="checkbox" id="remember_me" name="remember_me"> --}}
                              <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                              <label for="remember_me">{{ __('messages.login_remember') }}</label>
                            </div>
                          {{-- <div class="forgot-password">
                              <a href="#" data-toggle="modal" data-target="#ForgotPassword" class="btn-forgot-password">Forgot Password ?</a>
                          </div> --}}
                      </div>
                      <button type="submit" class="btn-login form-control">{{ __('messages.login') }}</button>
                  </form>
              </div>
            </div>
      </div>
    </div>
</div>