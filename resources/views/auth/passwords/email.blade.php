<div class="modal fade urgence-modal" id="ForgotPassword" tabindex="-1" role="dialog" aria-labelledby="ForgotPassword"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <div class="login-wrapper create-account-wrapper">
                    <div class="logo-wrapper">
                        <div class="logo">
                            <img src="assets/img/logo.png" alt="logo" class="img-responsive" width="280px">
                        </div>
                        <div class="slogan">
                            <h3>{{ __('messages.title_description') }}</h3>
                        </div>
                        <div class="title">
                            <h3>{{ __('messages.forgot_forgot_password') }}</h3>
                        </div>
                    </div>
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group login-inner-wrapper">
                            <div class="login-email">
                                <h4>{{ __('messages.forgot_email') }}</h4>
                                <label>
                                    <span><img src="assets/img/login.png" width="28px" height="28px"></span>
                                    <input id="email" type="email"
                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                        placeholder="{{ __('messages.forgot_email') }}" value="{{ old('email') }}" required autocomplete="email"
                                        autofocus>
                                </label>
                            </div>
                            <div class="forgot-info">
                                <img src="assets/img/information.png" width="14px"><span>{{ __('messages.forgot_label') }}</span><br>
                            </div>
                        </div>
                        <div class="create-account-submit-wrapper">
                            <button type="submit" class="btn-create-account form-control">{{ __('messages.forgot_btn') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
