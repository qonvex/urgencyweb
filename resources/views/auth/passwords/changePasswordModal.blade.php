
<div class="modal fade urgence-modal" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="changePassword"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <div class="login-wrapper create-account-wrapper">
                    <div class="logo-wrapper">
                        <div class="logo">
                            <img src="{{ asset('assets/img/logo.png') }}" alt="logo" class="img-responsive" width="280px">
                        </div>
                        <div class="slogan">
                            <h3>{{ __('messages.title_description') }}</h3>
                        </div>
                        <div class="title">
                            <h3>{{ __('messages.reset_password') }}</h3>
                        </div>
                    </div>
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group login-inner-wrapper">
                            <div class="login-email">
                                <h4>{{ __('messages.reset_email') }}</h4>
                                <label>
                                    <span><img src="{{ asset('assets/img/login.png') }}" width="28px" height="28px"></span>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"  value="{{ $email ?? old('email') }}" placeholder="EMAIL" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                </label>
                            </div>
                            <div class="login-password">
                                <h4>{{ __('messages.reset_new_password') }}</h4>
                                <label>
                                    <span><img src="{{ asset('assets/img/password.png') }}" width="28px" height="28px"></span>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="**********" required autocomplete="current-password">
                                </label>
                            </div>
                            <div class="login-password">
                                <h4>{{ __('messages.reset_cofirm_password') }}</h4>
                                <label>
                                    <span><img src="{{ asset('assets/img/password.png') }}" width="28px" height="28px"></span>
                                    <input id="password-confirm"  type="password" class="form-control" name="password_confirmation" placeholder="**********" required autocomplete="current-password">
                                </label>
                            </div>
                        </div>
                        <div class="create-account-submit-wrapper">
                            <button type="submit" class="btn-create-account form-control">{{ __('messages.reset_btn') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
