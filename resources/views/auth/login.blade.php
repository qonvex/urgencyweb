<!-- LOGIN POP-UP -->
<div class="modal fade urgence-modal" id="Login" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                  </button>
                  <div class="login-wrapper">
                      <div class="logo-wrapper">
                          <div class="logo">
                              <img src="{{ asset('assets/img/logo.png') }}" alt="logo" class="img-responsive" width="280px">
                          </div>
                          <div class="slogan">
                              <h3>{{ __('messages.login_login') }}</h3> 
                          </div>
                      </div>
                      <form method="POST" action="{{ route('login') }}" id="login-form"> 
                        @csrf
                          <div class="form-group login-inner-wrapper">
                              <div class="login-email">
                                  <h4>{{ __('messages.login_email') }}</h4>
                                  <label>
                                      <span><img src="{{ asset('assets/img/login.png') }}" width="28px" height="28px"></span>
                                      <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="EMAIL" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    </label>
                                    <small class="invalid-feedback text-danger" role="alert" id="email-error">
                                           
                                    </small>
                                    @error('email')
                                        <small class="invalid-feedback text-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </small>
                                    @enderror
                              </div>
                              <div class="login-password">
                                  <h4>{{ __('messages.login_password') }}</h4>
                                  <label>
                                      <span><img src="{{ asset('assets/img/password.png') }}" width="28px" height="28px"></span>
                                      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="**********" required autocomplete="current-password">
                                    </label>

                                    <small class="invalid-feedback text-danger" role="alert" id="password-error">
                                           
                                    </small>

                                    @error('password')
                                    <span class="invalid-feedback text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                              </div>
                          </div>
                          <div class="login-other-wrapper">
                              <div class="remember-me">
                                  {{-- <input type="checkbox" id="remember_me" name="remember_me"> --}}
                                  <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                  <label for="remember_me">{{ __('messages.login_remember') }}</label>
                                </div>
                              <div class="forgot-password">
                                  <a href="#" data-toggle="modal" data-target="#ForgotPassword" class="btn-forgot-password">{{ __('messages.login_forgot_password') }}</a>
                              </div>
                          </div>
                          <button id="login-btn" type="submit" class="btn-login form-control"><i class="fa fa-circle-o-notch fa-spin loading" style="display:none;"></i>&nbsp;{{ __('messages.login_btn') }}</button>
                          <a href="#" class="form-control btn-create-account" data-toggle="modal" data-target="#CreateAccount" style="text-decoration:none;">{{ __('messages.login_create_account') }}</a>
                      </form>
                  </div>
                </div>
          </div>
        </div>
  </div>