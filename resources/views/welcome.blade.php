@include('frontend.includes.header')
<div class="hm-section1">
    <div class="slant-before">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="home-banner">
                    <div class="logo">
                        <img src="{{ asset('assets/img/logo-w.png') }}" alt="logo" class="img-responsive" width="280px">
                    </div>
                    <div class="slogan">
                        <h3>{{ __('messages.title_description') }}</h3>
                    </div>
                    <div class="description">
                        <p>
                            {{ __('messages.title_paragraph1') }} <br>
                            {{ __('messages.title_paragraph2') }} <br>
                            <!-- {{ __('messages.title_paragraph3') }} <br> -->
                        </p>
                    </div>
                </div>
                <div class="scroll-me">
                    <span class="scroll-btn">
                        <a href="#">
                            <span class="mouse">
                                <span>
                                </span>
                            </span>
                        </a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hm-section2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="contact-wrapper">
                    <div class="title">
                        <h2>urgence web<span class="reg">&reg; </span></h2>
                    </div>
                    <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <section class="main-timeline-section">
                                    <div class="conference-center-line"></div>
                                    <div class="conference-timeline-content">
                                        <div class="timeline-article timeline-article-bottom">
                                            <div class="content-date">
                                                  <span>{{ __('messages.title_second_description6') }}</span>
                                            </div>
                                            <div class="meta-date">
                                                <span>6</span>
                                            </div>
                                        </div>
                                        <div class="timeline-article timeline-article-top">
                                            <div class="content-date">
                                                  <span> {{ __('messages.title_second_description5') }}</span>
                                            </div>
                                            <div class="meta-date">
                                                <span>5</span>
                                            </div>
                                        </div>
                                           <div class="timeline-article timeline-article-bottom">
                                            <div class="content-date">
                                                  <span>{{ __('messages.title_second_description4') }}</span>
                                            </div>
                                            <div class="meta-date">
                                                <span>4</span>
                                            </div>
                                        </div>
                    
                                        <div class="timeline-article timeline-article-top">
                                            <div class="content-date">
                                                  <span> {{ __('messages.title_second_description3') }}</span>
                                            </div>
                                            <div class="meta-date">
                                                <span>3</span>
                                            </div>
                                        </div> 
                                        <div class="timeline-article timeline-article-bottom">
                                            <div class="content-date">
                                                  <span>{{ __('messages.title_second_description2') }}</span>
                                            </div>
                                            <div class="meta-date">
                                                <span>2</span>
                                            </div>
                                        </div> 
                                        <div class="timeline-article timeline-article-top">
                                            <div class="content-date">
                                                  <span> {{ __('messages.title_second_description1') }}</span>
                                            </div>
                                            <div class="meta-date">
                                                <span>1</span>
                                            </div>
                                        </div> 
                                       </div>
                                  </section>
                            </div>
                        </div>
                </div>
                <div class="col-md-12">
                    <div class="pay-now">
                        <div class="title-cont">
                            <h3>{{ __('messages.pay_now') }}</h3>
                            <p>{{ __('messages.pay_now_description') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-6">
                    <div class="quotation-wrapper">
                        <div class="title">
                            <h2>{{ __('messages.ask_qoutation') }}</h2>
                        </div>
                        {{ Form::open(array('id' => 'qoutationForm', 'enctype' => 'multipart/form-data')) }}
                            <div class="form-group">
                                <label>{{ __('messages.NAME') }}</label>
                                {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'firstname | last name']) }}
                                <small class="invalid-feedback text-danger" role="alert" id="quotation-name-error"> 
                                </small>
                            </div>
                            <div class="form-group  email-wrapper">
                                <label>{{ __('messages.EMAIL') }}</label>
                                {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'email']) }}
                                <small class="invalid-feedback text-danger" role="alert" id="quotation-email-error"> 
                                </small>
                            </div>
                            <div class="form-group subject-wrapper">
                                <label>{{ __('messages.SUBJECT') }}</label>
                                {{ Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'subject']) }}
                                <small class="invalid-feedback text-danger" role="alert" id="quotation-subject-error"> 
                                </small>
                            </div>
                            <div class="form-group">
                                <label>{{ __('messages.MESSAGE') }}</label>
                                {{ Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'message', 'rows' => '6']) }}
                                <small class="invalid-feedback text-danger" role="alert" id="quotation-message-error"> 
                                </small>
                            </div>
                            <div class="form-group attach-wrapper">
                                    <div class="attachments">
                                    </div>
                                    <label class="btn btn-default btn-file ">
                                      <span class="fa fa-paperclip"></span>{{ __('messages.attach_file') }}
                                      <input id="files" name="file[]" type="file" style="display: none;" multiple="true" value="aksbdfabk">
                                  </label>
                              </div>
                              <button id="btn-quotation" type="submit" class="btn-send"><i class="fa fa-circle-o-notch fa-spin loading" style="display:none;"></i>&nbsp;{{ __('messages.send') }}</button>
                        {{ Form::close() }}
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="order-wrapper">
                        <div class="price-wrapper">
                            <h2 class="price-title">{{ __('messages.price') }}</h2>
                            <h3><span class="price" id='hour-price' data-id=""></span><span class="euro">&euro;</span></h3>
                        </div>
                        @php
                            $hours = App\Config::all();
                        @endphp
                        <div class="hours-wrapper">
                            <h3>{{ __('messages.no_of_hours') }}</h3>
                            <div class="main-hours-wrapper">
                                <div class="number-hours-wrapper" id="hourInputs">
                                    @foreach ($hours as $hour)
                                        <input type="text" name="" value="{{ $hour->hours }} hours"  readonly="readonly" data-id="{{ $hour->hour_price }}" data-hour_id="{{ $hour->id }}">
                                    @endforeach
                                </div>
                                <div class="hour-nav">
                                    <a href="#myCarousel"><span class="fa fa-angle-up btn-up"></span></a>
                                    <a href="#myCarousel"><span class="fa fa-angle-down btn-down"></span></a>
                                </div>
                            </div>
                            <p>{{ __('messages.hour_description') }}</p>
                        </div>
                    </div>
                    <div class="btn-order">
                        <a href="#" id="orderNowModal">{{ __('messages.order_now') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hm-section3">
    <div class="slant-after">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title">{{ __('messages.our_expertise') }}</h2>
                <p class="subtitle">{{ __('messages.our_expertise_description') }}</p>
            </div>
        </div>
    </div>
    <div class="expertise-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="experise-inner-wrapper expertise-inner-desktop">
                        <ul>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/HTML 5.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/HTML 5.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/CSS 3.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/CSS 3.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/GIT.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/GIT.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/PHP.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/PHP.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/ANDROID.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/ANDROID.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/PRESTASHOP.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/PRESTASHOP.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/WINDOWS.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/WINDOWS.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/SYMFONY.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/SYMFONY.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/IOS.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/IOS.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/WORDPRESS.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/WORDPRESS.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/MYSQL.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/MYSQL.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/DRUPAL.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/DRUPAL.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/JAVA.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/JAVA.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/LARAVEL.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/LARAVEL.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/POSTGRE SQL.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/POSTGRE SQL.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/SHOPIFY.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/SHOPIFY.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/MAGENTO.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/MAGENTO.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/JOOMLA.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/JOOMLA.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/APACHE.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/APACHE.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/ANGULAR JS.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/ANGULAR JS.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/BOOTSTRAP.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/BOOTSTRAP.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/IONIC.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/IONIC.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/JQUERY.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/JQUERY.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/NODE JS.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/NODE JS.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/MONGO DB.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/MONGO DB.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/WOO COMMERCE.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/WOO COMMERCE.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/PHOTOSHOP.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/PHOTOSHOP.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/XD.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/XD.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/AFTER EFFECTS.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/AFTER EFFECTS.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                            <li>
                                <div class="expertise-img">
                                    <img src="assets/img/expertise-bw/INDESIGN.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                                <div class="expertise-img-hover">
                                    <img src="assets/img/expertise-colored/INDESIGN.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="experise-inner-wrapper expertise-inner-mobile">
                        <div class="owl-expertise-mobile owl-carousel owl-theme">
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/HTML 5.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/HTML 5.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/CSS 3.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/CSS 3.png" alt="expertise" class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/GIT.png" alt="expertise" class="img-responsive"
                                            width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/GIT.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/PHP.png" alt="expertise" class="img-responsive"
                                            width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/PHP.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/ANDROID.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/ANDROID.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/PRESTASHOP.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/PRESTASHOP.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/WINDOWS.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/WINDOWS.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/SYMFONY.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/SYMFONY.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/IOS.png" alt="expertise" class="img-responsive"
                                            width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/IOS.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/WORDPRESS.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/WORDPRESS.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/MYSQL.png" alt="expertise" class="img-responsive"
                                            width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/MYSQL.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/DRUPAL.png" alt="expertise" class="img-responsive"
                                            width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/DRUPAL.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/JAVA.png" alt="expertise" class="img-responsive"
                                            width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/JAVA.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/LARAVEL.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/LARAVEL.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/POSTGRE SQL.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/POSTGRE SQL.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/SHOPIFY.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/SHOPIFY.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/MAGENTO.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/MAGENTO.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/JOOMLA.png" alt="expertise" class="img-responsive"
                                            width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/JOOMLA.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/APACHE.png" alt="expertise" class="img-responsive"
                                            width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/APACHE.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/ANGULAR JS.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/ANGULAR JS.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/BOOTSTRAP.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/BOOTSTRAP.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/IONIC.png" alt="expertise" class="img-responsive"
                                            width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/IONIC.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/JQUERY.png" alt="expertise" class="img-responsive"
                                            width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/JQUERY.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/NODE JS.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/NODE JS.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/MONGO DB.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/MONGO DB.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/WOO COMMERCE.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/WOO COMMERCE.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/PHOTOSHOP.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/PHOTOSHOP.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/XD.png" alt="expertise" class="img-responsive"
                                            width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/XD.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/AFTER EFFECTS.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/AFTER EFFECTS.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="expertise-img-wrapper">
                                    <div class="expertise-img">
                                        <img src="assets/img/expertise-bw/INDESIGN.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                    <div class="expertise-img-hover">
                                        <img src="assets/img/expertise-colored/INDESIGN.png" alt="expertise"
                                            class="img-responsive" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slant-before">
    </div>
</div>
<div class="hm-section4">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title">{{ __('messages.testimonials') }}</h2>
                <p class="subtitle">{{ __('messages.testimonials_description') }}</p>
                <div class="owl-testimonials owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonials-inner-wrapper">
                            <div class="testimonials-content-wrapper">
                                <div class="testimonials-text">
                                    <p>
                                        Le site que je souhaitais dans un budget contenu
                                    </p>
                                </div>
                                <div class="testimonials-details">
                                    <div class="testimonials-img">
                                        <img src="{{ asset('assets/img/testimonials/Jacky-jacky-hamard.fr.jpg') }}" alt="testimonials"
                                            class="img-responsive img-circle">
                                    </div>
                                    <div class="testimonials-name">
                                        <h4>JACKY – JACKY-HAMARD.FR</h4>
                                    </div>
                                    <div class="testimonials-position">
                                        <p>{{ __('messages.customer') }}</p>
                                    </div>
                                    <div class="prev_next_nav">
                                        <div class="prev">
                                            <img src="{{ asset('assets/img/arrow-left.png') }}">
                                        </div>
                                        <div class="next">
                                            <img src="{{ asset('assets/img/arrow-right.png') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonials-inner-wrapper">
                            <div class="testimonials-content-wrapper">
                                <div class="testimonials-text">
                                    <p>
                                        Merci pour la réalisation de mon site, super résultat et c’est l’outil degestion idéal pour mon activité !
                                    </p>
                                </div>
                                <div class="testimonials-details">
                                    <div class="testimonials-img">
                                        <img src="{{ asset('assets/img/testimonials/Danela-havanabeautyclub.com.jpg') }}" alt="testimonials"
                                            class="img-responsive img-circle">
                                    </div>
                                    <div class="testimonials-name">
                                        <h4>DANELA – HAVANABEAUTYCLUB.COM</h4>
                                    </div>
                                    <div class="testimonials-position">
                                        <p>{{ __('messages.customer') }}</p>
                                    </div>
                                    <div class="prev_next_nav">
                                        <div class="prev">
                                            <img src="{{ asset('assets/img/arrow-left.png') }}">
                                        </div>
                                        <div class="next">
                                            <img src="{{ asset('assets/img/arrow-right.png') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonials-inner-wrapper">
                            <div class="testimonials-content-wrapper">
                                <div class="testimonials-text">
                                    <p>
                                        Je fais confiance à Studio Seizh pour mes projets depuis plus de 2 ans et je suis pleinement satisfaite de mon choix. Merci à toute l’équipe !
                                    </p>
                                </div>
                                <div class="testimonials-details">
                                    <div class="testimonials-img">
                                        <img src="{{ asset('assets/img/testimonials/AnneSophie.jpg') }}" alt="testimonials"
                                            class="img-responsive img-circle">
                                    </div>
                                    <div class="testimonials-name">
                                        <h4>ANNE SOPHIE</h4>
                                    </div>
                                    <div class="testimonials-position">
                                        <p>{{ __('messages.customer') }}</p>
                                    </div>
                                    <div class="prev_next_nav">
                                        <div class="prev">
                                            <img src="{{ asset('assets/img/arrow-left.png') }}">
                                        </div>
                                        <div class="next">
                                            <img src="{{ asset('assets/img/arrow-right.png') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('frontend.includes.footer')

{{-- backend test commit --}}