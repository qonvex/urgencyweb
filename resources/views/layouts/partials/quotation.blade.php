<div class="modal fade urgence-modal" id="QuatationSent" tabindex="-1" role="dialog" aria-labelledby="QuatationSent" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
              </button>
              <div class="login-wrapper quotation-sent-wrapper">
                  <div class="logo-wrapper">
                      <div class="logo">
                          <img src="assets/img/logo.png" alt="logo" class="img-responsive" width="280px">
                      </div>
                      <div class="slogan">
                          <h3>You break it. We fix it!</h3>
                      </div>
                      <div class="heading">
                          <h2>Quotation Request Sent!</h2>
                      </div>
                  </div>
                  <div class="qoutation-text">
                      <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent auctor tellus sit amet justo ultrices mattis. Nunc mi enim, pellentesque ac quam</h4>
                  </div>
                  <button type="button" class="btn-return form-control" data-dismiss="modal" aria-label="Close">Return</button>
              </div>
            </div>
      </div>
    </div>
</div>