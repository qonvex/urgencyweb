<!-- ORDER NOW -->
<div class="modal fade urgence-modal" id="OrderNow" tabindex="-1" role="dialog" aria-labelledby="OrderNow"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <div class="logo-wrapper">
                    <div class="logo">
                        <img src="assets/img/logo.png" alt="logo" class="img-responsive" width="280px">
                    </div>
                    <div class="slogan">
                        <h3>You break it. We fix it!</h3>
                    </div>
                </div>
                <div class="col-12 mb-5">
                    <div class="col-md-6 order-hour">
                        <div>
                            <h3><span id="hour-modal" style="font-size: 44px;"></span></h3>
                            <label>{{ __('messages.no_of_hours') }}</label>
                        </div>
                    </div>
                    <div class="col-md-6 order-price">
                        <div>
                            <h1><span id="hour-price-modal"></span>€</h1>
                            <label>{{ __('messages.price') }}</label>
                        </div>
                    </div>
                </div>

                @if (!Auth::check())
                <div class="login-wrapper" id="payment-form-wrapper">
                    <div class="form-group">
                        <input id="check-account" type="checkbox" class="pt-1">
                        <span class="inline-block">{{ __('messages.checkbox_label') }}</span>
                    </div>
                    <form id="new-customer">
                        @csrf
                        <div class="form-group login-inner-wrapper">
                            <div class="login-email">
                                <h4>{{ __('messages.register_name') }}</h4>
                                <label>
                                    <span><img src="assets/img/login.png" width="28px" height="28px"></span>
                                    <input id="new-customer-name" type="text"
                                        class="form-control @error('name') is-invalid @enderror" name="name"
                                        value="{{ old('name') }}" placeholder="{{ __('messages.register_name') }}" required autocomplete="name"
                                        autofocus>
                                </label>
                                <small class="invalid-feedback text-danger" role="alert" id="new-customer-name-error">
                                </small>
                            </div>
                            <div class="login-email">
                                <h4>{{ __('messages.register_email') }}</h4>
                                <label>
                                    <span><img src="assets/img/login.png" width="28px" height="28px"></span>
                                    <input id="new-customer-email" type="email"
                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                        value="{{ old('email') }}" placeholder="EMAIL" required autocomplete="email">
                                </label>
                                <small class="invalid-feedback text-danger" role="alert" id="new-customer-email-error">
                                </small>
                            </div>
                            <div class="login-password">
                                <h4>{{ __('messages.register_password') }}</h4>
                                <label>
                                    <span><img src="assets/img/password.png" width="28px" height="28px"></span>
                                    <input id="new-customer-password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        placeholder="**********" required autocomplete="new-password">
                                </label>
                                <small class="invalid-feedback text-danger" role="alert" id="new-customer-password-error">
                                </small>
                            </div>
                            <div class="login-password">
                                <h4>{{ __('messages.register_confirm_password') }}</h4>
                                <label>
                                    <span><img src="assets/img/password.png" width="28px" height="28px"></span>
                                    <input id="new-customer-password-confirm" type="password" class="form-control"
                                        name="password_confirmation" placeholder="**********" required
                                        autocomplete="new-password">
                                </label>
                            </div>
                        </div>
                        <div class="create-account-submit-wrapper">
                            <div class="terms-wrapper">
                                <img src="assets/img/exclamation.png"
                                    width="14px"><a href="">{{ __('messages.register_agree') }}</a><br>
                            </div>
                            <button id="new-customer-register-btn" type="submit" class="btn-create-account form-control">ORDER</button>
                        </div>
                    </form>

                    <form id="old-customer" hidden="true">
                        <div class="form-group login-inner-wrapper">
                            <div class="login-email">
                                <h4>{{ __('messages.login_email') }}</h4>
                                <label>
                                    <span><img src="{{ asset('assets/img/login.png') }}" width="28px"
                                            height="28px"></span>
                                    <input id="old-customer-email" type="email"
                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                        placeholder="EMAIL" value="{{ old('email') }}" required autocomplete="email"
                                        autofocus>
                                </label>
                                <small class="invalid-feedback text-danger" role="alert" id="old-customer-email-error">

                                </small>
                            </div>
                            <div class="login-password">
                                <h4>{{ __('messages.login_password') }}</h4>
                                <label>
                                    <span><img src="{{ asset('assets/img/password.png') }}" width="28px"
                                            height="28px"></span>
                                    <input id="old-customer-password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        placeholder="**********" required autocomplete="current-password">
                                </label>
                            </div>
                        </div>
                        <button id="old-customer-login-btn" type="button" class="btn-login form-control"><i
                                class="fa fa-circle-o-notch fa-spin loading"
                                style="display:none;"></i>&nbsp;order</button>
                    </form>
                </div>
                @endif
                <div class="login-wrapper" id="checkout" style="{{ (Auth::check()) ? '' : 'display:none' }}">
                    <div class="payment-method-wrapper">
                            <h4>{{ __('messages.payment_method') }}</h4>
                            <div id="paypal-button-container"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
