<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
        name='viewport' />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <!-- CSS Files -->
    <link href="{{ asset('assets/backoffice/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backoffice/css/paper-dashboard.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{ asset('assets/backoffice/css/custom_style.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/backoffice/sweetalert/sweetalert2.min.css') }}" rel="stylesheet" />
  
</head>

<body class="">
    <div class="wrapper ">
        @include('backoffice.includes.sidebar')
        <div class="main-panel">
            <!-- Navbar -->
            @include('backoffice.includes.navbar')
            <!-- End Navbar -->
            <!-- <div class="panel-header panel-header-lg">
      
        <canvas id="bigDashboardChart"></canvas>
      
      </div> -->
        @yield('content')

            @include('backoffice.includes.footer')
        </div>
    </div>
    <!--   Core JS Files   -->
    <script src="{{ asset('assets/backoffice/js/core/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/backoffice/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('assets/backoffice/js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/backoffice/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
    <!-- Chart JS -->
    <script src="{{ asset('assets/backoffice/js/plugins/chartjs.min.js') }}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('assets/backoffice/js/plugins/bootstrap-notify.js') }}"></script>
    <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('assets/backoffice/js/paper-dashboard.min.js') }}" type="text/javascript"></script>
    <!-- MOMENT JS -->
    <script type="text/javascript" src="{{ asset('assets/js/moment.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ asset('assets/backoffice/sweetalert/sweetalert2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/backend.js') }}"></script>
</body>

</html>
