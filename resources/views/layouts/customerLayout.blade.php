@include('layouts.includes.header')
<div class="account-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="logo-wrapper">
                    <div class="logo">
                        <img src="{{ asset('assets/img/logo.png') }}" alt="logo" class="img-responsive" width="280px">
                    </div>
                    <div class="slogan">
                        <h3>{{ __('messages.title_description') }}</h3>
                    </div>
                </div>
                <div class="user-wrapper">
                    <div class="user-inner-wrapper">
                        @php
                            $user_photo = Auth::user()->profile;
                            $path = 'assets/profile/'.$user_photo;
                        @endphp
                        <div class="user-img">
                            <img src="{{ asset($path) }}" style="border-radius: 50%; height: 200px; width: 200px;">
                        </div>
                        <div class="user-name">
                            <h4 style="text-transform:capitalize;">{{ Auth::user()->name }}</h4>
                        </div>
                        <div class="process-tabs-wrapper">
                            <ul>
                                <li><a href="{{ route('account.index') }}" class="{{ Request::segment(1) === 'account' ? 'activeLink' : null }}">{{ __('messages.account') }}</a></li>
                                <li><a href="{{ route('order.index') }}" class="{{ Request::segment(1) === 'order' ? 'activeLink' : null }}">{{ __('messages.order') }}</a></li>
                                <li><a href="{{ route('credit.index') }}" class="{{ Request::segment(1) === 'credit' ? 'activeLink' : null }}">{{ __('messages.credit') }}</a></li>
                            </ul>
                        </div>
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.includes.footer')