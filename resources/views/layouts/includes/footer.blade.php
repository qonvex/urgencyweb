<footer>
    <div class="slant-after">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="logo">
                    <img src="{{ asset('assets/img/logo-w.png') }}" alt="logo" class="img-responsive" width="250px">
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <h4>Address</h4>
                        <p>848 mutton town road</p>
                        <p>battle ground, wa</p>
                    </div>
                    <div class="col-md-6">
                        <h4>Contact</h4>
                        <p class="email">webemergency.info@gmail.com</p>
                        <p>360-687-0425</p>
                    </div>
                </div>
                <div class="slogan">
                    <h3>You break it. We fix it!</h3>
                </div>
                <div class="order">
                    <ul>
                        <li><a href="#"><img src="{{ asset('assets/img/PayPal.png') }}" alt="logo" height="35px"></a></li>
                        <li><a href="#"><img src="{{ asset('assets/img/visa.png') }}" alt="logo" height="35px"></a></li>
                        <li><a href="#"><img src="{{ asset('assets/img/mc.png') }}" alt="logo" height="35px"></a></li>
                        <li><a href="#"><img src="{{ asset('assets/img/discover.png') }}" alt="logo" height="35px"></a></li>
                        <li><a href="#"><img src="{{ asset('assets/img/ae.png') }}" alt="logo" height="35px"></a></li>
                    </ul>
                </div>
                <div class="copyright">
                    &copy; <?php echo date("Y"); ?> Urgence web | ohm
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- CREATE NEW PROJECT MODAL -->
@include('customer.account.partials.addModal')

<!-- CREATE ADD HOUR MODAL -->
@include('customer.credit.partials.addHoursModal')

@include('customer.profile.profileModal')
</body>
<script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/mousewheel.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom-script.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/backend.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/payment.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/toastr.min.js') }}"></script>
<script>
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        @if(Session::has('success'))
        toastr.success('{{Session::get('success')}}')
        @endif
        @if(Session::has('danger'))
        toastr.error('{{Session::get('danger')}}')
        @endif
    </script>
    <script>
        $( function() {
          $( "#datepicker" ).datepicker(
            { 
                minDate: 0
            }
          );
        });
    </script>
</html>
