<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	 <!-- CSRF Token -->
	 <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  	<link rel="stylesheet" href="/resources/demos/style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.carousel.min.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom-style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom-style2.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/toastr.min.css') }}">
	
	<script
		src="https://www.paypal.com/sdk/js?client-id=AUjxirkMnYpO98Ev5WZs8aGS8WHdTBbtuNn6VldLV6pFIXfckfFGU_CGzCfQ0SSGEdj0aebOJ593sn-w&currency=EUR">
	</script>
</head>
	<body>
		<div id="loader" style="display:none;">
			<div class="lds-ring"><div></div><div></div><div></div><div></div></div>
		</div>
	<header>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="language-wrapper">
						<ul>
							<li class="login-tab"><img src="{{ asset('assets/img/login.png') }}" width="32px"><a href="#" style="text-transform:capitalize; color:black;">{{ Auth::user()->name }}</a>
								<ul>
									<li>
										<a href="/index">
											{{ __('messages.home') }}
										</a>
									</li>
									<li>
										<a href="#" data-toggle="modal" data-target="#profileForm">
											{{ __('messages.profile') }}
										</a>
									</li>
									<li>
										<a href="{{ route('logout') }}"
											onclick="event.preventDefault();
											document.getElementById('logout-form').submit();">
											{{ __('messages.logout') }}
										</a>
									</li>
								</ul>
							</li>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
							<li><a href="{{ url('locale/fr') }}" class="{{ app()->getLocale() === 'fr' ? 'active' : null }}">FR</a></li>
							<li><a href="{{ url('locale/en') }}" class="{{ app()->getLocale() === 'en' ? 'active' : null }}">EN</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</header>