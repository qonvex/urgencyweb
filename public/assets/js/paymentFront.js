$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //======================================================
    //CUSTOMER FRONT PAYMENT
    //======================================================
    var price;
    var hours;
    var hour_id;
    var success;
    var csrf;
    paypal.Buttons({
        createOrder: function (data, actions) {
            hours = parseInt($('#hourInputs .activehour').val());
            price = parseInt($('.activehour').data('id'));
            hour_id = $('.activehour').data('hour_id');
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        currency_code: 'EUR',
                        value: price
                    }
                }]
            });
        },
        onApprove: function (data, actions) {
            startLoading();
            data.hours = parseInt(hours);
            data.hour_id = parseInt(hour_id);
            $.ajax({
                type: 'POST',
                url: '/verifyTransaction',
                headers: {
                    'X-CSRF-TOKEN': csrf
                },
                data,
                success: function (data) {
                    startLoading()
                    window.location.reload()
                },
                error: function (data) {
                    stopLoading();
                    $('#CreditAddhour').modal('hide')
                    window.location.reload()
                }
            })
        }
    }).render('#paypal-button-container');

    //trigger the login button
    $('#old-customer-login-btn').click(function (e) {
        e.preventDefault();
        login(function (data) {
            csrf = data
            console.log(csrf)
        })
    })

    //trigger the register button
    $('#new-customer-register-btn').click(function (e) {
        e.preventDefault();
        register()
    })

    $('#orderNowModal').click(function (e) {
        e.preventDefault()
        $('#OrderNow').modal('show');
        let hours = parseInt($('#hourInputs .activehour').val());
        console.log(hours)
        let price = parseInt($('#hour-price').html());
        $('#hour-modal').html(`${hours} ${hours > 1 ? 'hrs' : 'hr'}`)
        $('#hour-price-modal').html(price)
    })

    $('#check-account').change(function (e) {
        if (this.checked) {
            $('#new-customer').attr('hidden', true)
            $('#old-customer').attr('hidden', false)
        }
        if (!this.checked) {
            $('#new-customer').attr('hidden', false)
            $('#old-customer').attr('hidden', true)
        }
    })
})

//ajax to login
function login(callback) {
    $.ajax({
        type: 'POST',
        url: '/login',
        data: {
            'email': $('#old-customer #old-customer-email').val(),
            'password': $('#old-customer #old-customer-password').val(),
        },
        success: function (data) {
            callback(data)
            $('#checkout').removeAttr("style");
            $('#payment-form-wrapper').attr('hidden', true)
        },
        error: function (data) {
            stopLoading();
            data = JSON.parse(data.responseText)
            $('#old-customer-email-error').html('')
            $(`
                    <strong>${data.email}</strong>
                `).appendTo('#old-customer-email-error')
        }
    })
}

//ajax to register
function register() {
    $.ajax({
        type: 'POST',
        url: '/register/customer',
        data: {
            'name': $('#new-customer #new-customer-name').val(),
            'email': $('#new-customer #new-customer-email').val(),
            'password': $('#new-customer #new-customer-password').val(),
            'password_confirmation': $('#new-customer #new-customer-password-confirm').val(),
        },
        success: function (data) {
            $('#checkout').removeAttr("style");
            $('#payment-form-wrapper').attr('hidden', true)
        },
        error: function (data) {
            stopLoading();
            data = JSON.parse(data.responseText)
            $('#new-customer-name-error').html('')
            $('#new-customer-email-error').html('')
            $('#new-customer-password-error').html('')
            if (data.errors.name) {
                $(`
                    <strong>${data.errors.name}</strong>
                `).appendTo('#new-customer-name-error')
            }
            if (data.errors.email) {
                $(`
                    <strong>${data.errors.email}</strong>
                `).appendTo('#new-customer-email-error')
            }
            if (data.errors.password) {
                $(`
                    <strong>${data.errors.password}</strong>
                `).appendTo('#new-customer-password-error')
            }
        }
    })
}



function startLoading() {
    $('#loader').show();
}

function stopLoading(params) {
    $('#loader').hide();
}
