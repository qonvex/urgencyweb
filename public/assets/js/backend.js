$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //======================================================
    //FRONT PAGE
    //======================================================

    // registration
    $('#register-btn').click(function (e) {
        startLoading();
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/register',
            data: {
                'name': $('#register-form #name').val(),
                'email': $('#register-form #email').val(),
                'password': $('#register-form #password').val(),
                'password_confirmation': $('#register-form #password-confirm').val(),
            },
            success: function (data) {
                stopLoading();
                window.location.assign(`/verification/code/${data.link}`);
            },
            error: function (data) {
                stopLoading();
                data = JSON.parse(data.responseText)
                $('#register-name-error').html('')
                $('#register-email-error').html('')
                $('#register-password-error').html('')
                if (data.errors.name) {
                    $(`
                        <strong>${data.errors.name}</strong>
                    `).appendTo('#register-name-error')
                }
                if (data.errors.email) {
                    $(`
                        <strong>${data.errors.email}</strong>
                    `).appendTo('#register-email-error')
                }
                if (data.errors.password) {
                    $(`
                        <strong>${data.errors.password}</strong>
                    `).appendTo('#register-password-error')
                }
            }
        });
    })

    //login
    $('#login-btn').click(function (e) {
        e.preventDefault();
        startLoading();
        $.ajax({
            type: 'POST',
            url: '/login',
            data: {
                'email': $('#login-form #email').val(),
                'password': $('#login-form #password').val(),
            },
            success: function (data) {
                stopLoading();
                window.location.assign('/account');
            },
            error: function (data) {
                stopLoading();
                data = JSON.parse(data.responseText)
                $('#email-error').html('')
                $(`
                    <strong>${data.email}</strong>
                `).appendTo('#email-error')
            }
        });
    })
    //quotation
    let files_array = new Array;
    $('#btn-quotation').click(function (e) {
        e.preventDefault()
        startLoading();
        let data = new FormData()
        let formData = $('#qoutationForm').serializeArray()

        formData.forEach(function (field) {
            data.append(field.name, field.value)
        })

        files_array.forEach(function (file) {
            data.append('file[]', file)
        })
        $.ajax({
            type: 'POST',
            url: '/quotation',
            processData: false,
            contentType: false,
            data,
            success: function (data) {
                files_array = [];
                stopLoading();
                $('#qoutationForm input').val('')
                $('#qoutationForm textarea').val('')
                $('.attachments').html('')
                $('#quotation-name-error').html('')
                $('#quotation-email-error').html('')
                $('#quotation-subject-error').html('')
                $('#quotation-message-error').html('')
                $('#QuatationSent').modal('show')
            },
            error: function (data) {
                stopLoading();
                data = JSON.parse(data.responseText)
                let error = data.errors;
                $('#quotation-name-error').html('')
                $('#quotation-email-error').html('')
                $('#quotation-subject-error').html('')
                $('#quotation-message-error').html('')
                if (error) {
                    if (error.name) {
                        $(`
                            <strong>${error.name}</strong>
                        `).appendTo('#quotation-name-error')
                    }
                    if (error.email) {
                        $(`
                            <strong>${error.email}</strong>
                        `).appendTo('#quotation-email-error')
                    }
                    if (error.subject) {
                        $(`
                            <strong>${error.subject}</strong>
                        `).appendTo('#quotation-subject-error')
                    }
                    if (error.message) {
                        $(`
                            <strong>${error.message}</strong>
                        `).appendTo('#quotation-message-error')
                    }
                } else {
                    toastr.warning('Went something wrong Please try again')
                }

            }
        });
    })

    //listen to change event in file input
    $('#files').change(function (e) {
        let files = e.target.files;
        let attachment = $('.attachments');
        for (let i = 0; i < files.length; i++) {
            files_array.push(files[i])
            $(`<div class="n_img"><span class="myclose" data-name="${files[i].name}">x</span><span width="100px" style="margin-right:20px;font-size:10px;">${files[i].name}</span></div>`).appendTo(attachment)
        }
    })

    //listen to event click / removing file in array
    $(document).on('click', '.myclose', function (e) {
        let name = $(this).data('name')
        files_array.forEach(function (file, index) {
            if (name == file.name) {
                files_array.splice(index, 1)
            }
        })
        $(this).parents('.n_img').remove()
    })

    //======================================================
    //CUSTOMER
    //====================================================== 

    //update profile 
    //preview profile image
    $("#photo-profile").change(function () {
        $('#profile-pic').attr('hidden', true);
        $('#profile-view').attr('hidden', false);
        readURL(this);
    });
    //preview profile image
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#profile-view').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    //show change password form
    $('#showPassword').change(function (e) {
        let value = e.target.value;
        if (this.checked) {
            $('#password-inputs').attr('hidden', false);
        }
        if (!this.checked) {
            $('#password-inputs').attr('hidden', true);
        }
    })


    //Account

    //add project
    let project_files_array = new Array;
    $('#btn-project').click(function (e) {
        e.preventDefault()
        startLoading();
        let data = new FormData()
        let formData = $('#projectForm').serializeArray()

        formData.forEach(function (field) {
            data.append(field.name, field.value)
        })

        project_files_array.forEach(function (file) {
            data.append('file[]', file)
        })
        $.ajax({
            type: 'POST',
            url: '/project',
            processData: false,
            contentType: false,
            data,
            success: function (data) {
                $('#NewProject').modal('hide')
                project_files_array = [];
                toastr.success('PROJECT ADDED');
                window.location.reload()
            },
            error: function (data) {
                stopLoading();
                data = JSON.parse(data.responseText)
                let error = data.errors;
                console.log(error)
                $('#project-title-error').html('')
                $('#project-start-date-error').html('')
                $('#project-description-error').html('')
                $('#project-message-error').html('')
                if (error) {
                    if (error.title) {
                        $(`
                            <strong>${error.title}</strong>
                        `).appendTo('#project-title-error')
                    }
                    if (error.start_date) {
                        $(`
                            <br><strong>${error.start_date}</strong>
                        `).appendTo('#project-start-date-error')
                    }
                    if (error.description) {
                        $(`
                            <strong>${error.description}</strong>
                        `).appendTo('#project-description-error')
                    }
                    if (error.message) {
                        $(`
                            <strong>${error.message}</strong>
                        `).appendTo('#project-message-error')
                    }
                } else {
                    toastr.warning('Went something wrong Please try again')
                }

            }
        });
    })

    $('#project_files').change(function (e) {
        let files = e.target.files;
        let attachment = $('.attachments');
        for (let i = 0; i < files.length; i++) {
            project_files_array.push(files[i])
            $(`<div class="n_img"><span class="myclose" data-name="${files[i].name}">x</span><span width="100px" style="margin-right:20px;font-size:10px;">${files[i].name}</span></div>`).appendTo(attachment)
        }
    })

    $(document).on('click', '.project_file_close', function (e) {
        let name = $(this).data('name')
        project_files_array.forEach(function (file, index) {
            if (name == file.name) {
                project_files_array.splice(index, 1)
            }
        })
        $(this).parents('.n_img').remove()
    })

    //project sorting
    $('#project-sort').change(function () {
        let value = $(this).val();
        $.ajax({
            type: 'GET',
            url: '/customer/project',
            data: {
                value: value,
            },
            dataType: 'json',
            success: function (data) {
                $('#project-table').html('')
                console.log(data.projects)

                if (data.projects.length > 0) {
                    data.projects.forEach(function (project) {
                        $(`
                        <tr>
                            <td width="10%" valign="center">
                            <label>
                                    <span class="${project.status == 0 ? 'status-white' : (project.status == 1 ? 'status-orange' : 'status-green')}"></span>
                                    </label>
                            </td>
                            <td width="25%" valign="center" align="left">
                                <small class="progress-status ${project.status == 0 ? 'text-waiting' : (project.status == 1 ? 'text-inprogress' : 'text-done')}">${project.status == 0 ? 'waiting...' : (project.status == 1 ? 'In progress' : 'Done')}</small>
                                <h4>${project.title}</h4>
                                <p>${project.description.slice(0, 15)}</p>
                            </td>
                            <td width="25%" valign="center" align="center">
                                <strong><span>Started :</span></strong> <span>${moment(project.start_date).format('LL')}</span>
                                </td>
                                <td width="25%" valign="center" align="center">
                                <strong><span>Hours used :</span></strong>
                                <span>
                                ${project.hours_used != null ? project.hours_used : '0 hrs'}hrs
                                </span>
                                </td>
                                <td width="15%" valign="center" align="center">
                                <a href="/account/project/${project.id}">VIEW</a>
                                </td>
                                </tr>
                                `).appendTo('#project-table')
                    })
                } else {
                    $(`<p class="bg-danger">No project</p>`).appendTo('#project-table')
                }

            },
            error: function (data) {
                console.log(data);
            }
        });
    })
    //CREDIT PAGE
    $('#orderNow').click(function (e) {
        e.preventDefault();
        let hour = $('#hourInputs .activehour').val();
        $.ajax({
            type: 'POST',
            url: '/order',
            data: {
                hour: parseInt(hour),
            },
            dataType: 'json',
            success: function (data) {
                window.location.reload();
            },
            error: function (data) {
                window.location.reload();
            }
        });
    })

    //ORDER HISTORY PAGE SORTING
    $('#select-month').change(function (e) {
        let value = $('#select-month').val();
        console.log(value)
        $.ajax({
            type: 'GET',
            url: '/customer/order',
            data: {
                value: parseInt(value),
            },
            dataType: 'json',
            success: function (data) {
                console.log(data)
                $('#order-table').html('')

                if (data.orders.length > 0) {
                    data.orders.forEach(function (order) {
                        $(`
                        <tr>
                            <td valign="center" align="left">
                                <h5>${moment(order.created_at).format('LL')}</h5>
                            </td>
                            <td valign="center" align="left">
                                <h5>${order.hours} hrs</h5>
                            </td>
                            <td valign="center" align="left">
                                <h5>${order.price} €</h5>
                            </td>
                            <td valign="center" align="center">
                                <ul>
                                    <li>
                                        <a href="#"><img src="assets/img/download.png"></a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="assets/img/delete.png"></a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        `).appendTo('#order-table')
                    })
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    })

    //ORDER HISTORY PAGE DELETE ORDER
    $('.delete-order').click(function (e) {
        e.preventDefault();
        let order = $(this).data('id');
        let row = this;
        $.ajax({
            type: 'PATCH',
            url: `/customer/order/delete/${order}`,
            data: {
                order: parseInt(order),
            },
            dataType: 'json',
            success: function (data) {
                row.closest('tr').remove()
            },
            error: function (data) {
                console.log(data);
            }
        });
    })


    // //order page invoice
    // $('.invoice-order').click(function (e) {
    //     e.preventDefault();
    //     let order_id = $(this).data('id');
    //     $.ajax({
    //         type: 'GET',
    //         url: `/customer/order/invoice/${order_id}`,
    //         data: {
    //             order: parseInt(order),
    //         },
    //         dataType: 'json',
    //         success: function (data) {
    //             row.closest('tr').remove()
    //         },
    //         error: function (data) {
    //             console.log(data);
    //         }
    //     });
    // })
















    //======================================================
    //BACK OFFICE
    //======================================================

    //CUSTOMER INDEX PAGE
    //search customer

    //timer delay
    let delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();
    $(document).on('keyup', '#search-project', function (e) {
        let search = e.target.value;
        delay(function () {
            $.ajax({
                type: "GET",
                url: `/customer/search/data`,
                data: {
                    search: search
                },
                dataType: 'json',
                success: function (data) {
                    $('#customer-table').html('')
                    data.customers.forEach(function (customer) {
                        $(`
                        <tr>
                            <td>
                                <div class="logo ml-5">
                                    <img src="/assets/profile/${customer.profile}" class="customer-img">
                                </div>   
                            </td>
                            <td>
                                <a href="/customer/${customer.id}" style="text-decoration:none; color:#000000;">
                                    ${customer.name}
                                </a>
                            </td>
                            <td>
                                ${customer.email}
                            </td>
                            <td>
                                ${customer.contact == null ? 'no contact' : customer.contact}
                            </td> 
                            <td>
                                ${customer.address == null ? 'no address' : customer.address}
                            </td>
                            <td>
                                ${customer.projects.length < 0 ? '0' : customer.projects.length}
                            </td>
                            <td>
                                ${customer.total_credits} hrs
                            </td>
                            <td class="text-right">
                                <span>
                                    <a href="/customer/${customer.id}"> <i class="fa fa-eye"></i></a>
                                </span>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                &nbsp;
                            </td>
                        </tr>
                        `).appendTo('#customer-table')
                    })
                },
                error: function (data) {

                }
            });
        }, 1500);
    });

    //sort customer project
    $('#project-view-sorting').change(function (e) {
        let value = e.target.value;
        let customer_id = $('#customer_id').val();
        $.ajax({
            type: "GET",
            url: `/customer/search/project/${customer_id}/${value}`,
            dataType: 'json',
            success: function (data) {
                console.log(data)
                $('#customer-project-table').html('');
                $('#error-bag').html('');
                if (data.projects.length > 0) {
                    data.projects.forEach(function (project) {
                        $(`
                        <tr>
                            <td>
                                ${project.title} <br>
                                <i><small>${project.description.split(/\s+/).slice(0,5).join(" ")}</small></i>
                            </td>
                            <td>
                                ${moment(project.start_date).format('LL')}
                            </td>
                            <td>
                                ${project.hours_used == null ? '0hrs' : project.hours_used+'hrs'}
                            </td>
                            <td>
                                <span class="credit-status ${project.status == 0 ? '' : (project.status == 1 ? 'in-progress' : 'done')}">${project.status == 0 ? 'waiting...' : (project.status == 1 ? 'IN PROGRESS' : 'DONE')}
                                </span>
                            </td>
                            <td>
                                ${project.is_approved == 1 ? `<i class="fa fa-check"></i>` : `<p class="mb-0">waiting...</p>` }
                            </td>
                            <td class="text-right">
                                <span>
                                    <a href="#" class="view-project" data-id="${project.id}"><i class="fa fa-eye"></i></a>
                                </span>
                                &nbsp; &nbsp;
                                <span>
                                    <a href="#" class="delete-project" data-id="${project.id}"><i class="fa fa-trash text-danger"></i></a>
                                </span>
                                &nbsp; &nbsp;
                            </td>
                        </tr>
                        `).appendTo('#customer-project-table')
                    })
                } else {
                    $(`<small class="bg-danger text-light">No project</small>`).appendTo('#error-bag')
                }
            },
            error: function (data) {
                console.log(data)
            }
        });
    });

    //PROJECT INDEX PAGE
    //delete project
    $(document).on('click', '.delete-project', function (e) {
        let id = $(this).data('id');
        let project = this;
        swal.fire({
            title: "Are you sure!",
            text: 'All data of this project will be deleted',
            type: "warning",
            confirmButtonColor: "#c73b39",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: `/project/delete/${id}`,
                    data: {
                        id: id
                    },
                    success: function (data) {
                        Swal.fire(
                            'Deleted!',
                            'Data has been deleted.',
                            'success'
                        )
                        project.closest('tr').remove();
                    },
                    error: function (data) {
                        console.log(data)
                    }
                });
            }
        });
    });
    //sort project
    $('#project-sorting').change(function (e) {
        let value = e.target.value;
        $.ajax({
            type: "GET",
            url: `/project/sort/${value}`,
            dataType: 'json',
            success: function (data) {
                console.log(data)
                $('#project-table').html('');
                $('#error-bag').html('');
                if (data.projects.length > 0) {
                    data.projects.forEach(function (project) {
                        $(`
                        <tr>
                            <td>
                                ${project.title} <br>
                                <i><small>${project.description.split(/\s+/).slice(0,5).join(" ")}</small></i>
                            </td>
                            <td>
                                ${moment(project.start_date).format('LL')}
                            </td>
                            <td>
                                ${project.hours_used == null ? '0hrs' : project.hours_used+'hrs'}
                            </td>
                            <td>
                                <span class="credit-status ${project.status == 0 ? '' : (project.status == 1 ? 'in-progress' : 'done')}">${project.status == 0 ? 'waiting...' : (project.status == 1 ? 'IN PROGRESS' : 'DONE')}
                                </span>
                            </td>
                            <td>
                                ${project.is_approved == 1 ? `<i class="fa fa-check"></i>` : `<p class="mb-0">waiting...</p>` }
                            </td>
                            <td class="text-right">
                                <span>
                                    <a href="#" class="view-project" data-id="${project.id}"><i class="fa fa-eye"></i></a>
                                </span>
                                &nbsp; &nbsp;
                                <span>
                                    <a href="#" class="delete-project" data-id="${project.id}"><i class="fa fa-trash text-danger"></i></a>
                                </span>
                                &nbsp; &nbsp;
                            </td>
                        </tr>
                        `).appendTo('#project-table')
                    })
                } else {
                    $(`<small class="bg-danger text-light">No project</small>`).appendTo('#error-bag')
                }
            },
            error: function (data) {
                console.log(data)
            }
        });
    });
    //view project
    $(document).on('click', '.view-project', function (e) {
        let id = $(this).data('id');
        $.ajax({
            type: 'GET',
            url: `/project/view/${id}`,
            data: {
                id: id,
            },
            dataType: 'json',
            success: function (data) {
                console.log(data)
                $('#project-details').html('');
                $('#viewProject').modal('show');
                $(`
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <p class="h6">Customer</h5>
                            </div>
                            <div class="card-body">
                                <p><strong>Name :</strong> ${data.customer.name}</p>
                                <p><strong>Credit :</strong> ${data.credit_hours < 0 ? '0' : data.credit_hours} hrs</p>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <p class="h6">Project</p>
                            </div>
                            <div class="card-body">
                                <p><strong>Title : </strong> ${data.project.title}</p>
                                <p class="text-justify"><strong>Description : </strong>  ${data.project.description}</p>
                                <p><strong>Start Date : </strong>  ${moment(data.project.start_date).format('LL')}</p>
                                <p><strong>Status : </strong> ${data.project.status == 0 ? 'waiting...' : (data.project.status == 1 ? 'In progress' : 'Done')}</p>
                                <p><strong>Files : </strong><button class="btn btn-sm" id="download-btn" data-id="${data.project.id}" style="${data.project.files == 0 ? 'display:none' : ''}"><i class="nc-icon nc-cloud-download-93 mr-2"></i>Download</button></p>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <p class="h6">Actions</p>
                            </div>
                            <div class="card-body">
                            <form id="project-form">
                                <input type="text" value="${data.project.id}" name="id" hidden="hidden">
                                <div class="form-group mx-sm-3 mb-2 form-inline">
                                    <strong>Approved : </strong>
                                    <input class="approve form-control p-2 ml-4" name="approve" ${data.project.is_approved == 1 ? 'checked' : ''} type="checkbox" class="ml-3" data-id="${data.project.id}">
                                </div>
                                <div class="form-group mx-sm-3 mb-2 form-inline">
                                    <strong>Status : </strong>
                                    <select class="form-control ml-5 p-1 status" style="width:184px;" name="status" ${data.project.is_approved == 1 ? '' : 'disabled'}>
                                        <option value="0" ${data.project.status == 0 ? 'selected' : ''}>Waiting</option>
                                        <option value="1" ${data.project.status == 1 ? 'selected' : ''}>In progress</option>
                                        <option value="2" ${data.project.status == 2 ? 'selected' : ''}>Done</option>
                                    </select>
                                </div>
                                <div class="form-group mx-sm-3 mb-2 form-inline">
                                    <strong>Hours used : </strong>
                                    <input name="hour_used" type="number" class="hours_used form-control p-2 ml-2" value="${data.project.hours_used}" min="0" ${data.project.is_approved == 1 ? '' : 'disabled'}>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                `).appendTo('#project-details')
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    //enabled status field
    $(document).on('change', '.approve', function (e) {
        console.log(e.target.value)
        if (e.target.value == "on") {
            $('.status').prop("disabled", false);
            $('.hours_used').prop("disabled", false);
        }
    })
    //update status
    $('.save-project').click(function (e) {
        let formdata = $('#project-form').serializeArray();
        console.log(formdata)
        $.ajax({
            type: 'PATCH',
            url: `/project/update/status`,
            data: formdata,
            dataType: 'json',
            success: function (data) {
                $('#viewProject').modal('hide');
                Swal.fire({
                    allowOutsideClick: false,
                    title: 'Updated!',
                    type: "success"
                }).then((result) => {
                    if (result.value) {
                        window.location.reload()
                    }
                })
            },
            error: function (data) {
                console.log(data);
            }
        });
    })
    //download project files
    $(document).on('click', '#download-btn', function (e) {
        e.preventDefault()
        let id = $(this).data('id');
        window.location.assign('/project/download?id=' + id + '&uid=' + Math.random().toString(36));
    })

    //ORDER INDEX PAGE
    //view order
    $(document).on('click', '.view-order', function (e) {
        let id = $(this).data('id');
        $.ajax({
            type: 'GET',
            url: `/order/view/${id}`,
            data: {
                id: id
            },
            dataType: 'json',
            success: function (data) {
                console.log(data.order)
                $('#order-details').html('');
                $('#viewOrder').modal('show');
                $(`
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <p class="h6">Order Details</h5>
                                <hr>
                            </div>
                            <div class="card-body">
                                <p><strong>Customer :</strong> ${data.order.user.name}</p>
                                <p><strong>Hours :</strong> ${data.order.hours} hrs</p>
                                <p><strong>Price :</strong> ${data.order.price} €</p>
                                <p><strong>Transaction ID :</strong> #${data.order.id} </p>
                                <hr>
                                <p><strong>Created At :</strong> ${moment(data.order.created_at)})}</p>
                                <p><strong>Updated At :</strong> ${data.order.updated_at}</p>
                            </div>
                        </div>
                    </div>
                </div>
                `).appendTo('#order-details')
            },
            error: function (data) {
                console.log(data)
            }
        });
    });

    //QUOTATION INDEX PAGE
    $(document).on('click', '#quotation', function (e) {
        let id = $(this).data('id');
        $.ajax({
            type: "GET",
            url: `/quotation/${id}`,
            success: function (data) {
                console.log(data)
                $('#viewQuotation').modal('show')
                $('#quotation-details').html('')
                let quotation = data.quotation;
                $(`
                    <div class="row">
                        <div class="col-12">
                            <table>
                                <caption><h5>Quotation</h5></caption>
                                <thead>
                            
                                </thead>
                                <tr>
                                    <td>From</td>
                                    <td>${quotation.name}</td>
                                </tr>
                            </table>
                            <p><strong>From :</strong> ${quotation.name}</p>
                            <p><strong>Email :</strong> ${quotation.email}</p>
                            <p><strong>Subject :</strong> ${quotation.subject}</p>
                            <p><strong>Message :</strong><br> </p><p class="text-justify">&emsp;&emsp;&emsp;${quotation.message}</p>
                            <p><strong>Files : </strong><button class="btn btn-sm" id="download-quotation-files" data-id="${quotation.id}"><i class="nc-icon nc-cloud-download-93 mr-2"></i>Download</button></p>
                        </div>
                    </div>
                `).appendTo('#quotation-details')
            },
            error: function (data) {
                Swal.fire(
                    'Opps!',
                    'Wen something wrong',
                    'error'
                )
            }
        });
    });
    // $(document).on('click', '.view-quotation', function (e) {
    //     let id = $(this).data('id');
    //     $.ajax({
    //         type: "GET",
    //         url: `/quotation/${id}`,
    //         success: function (data) {
    //             $('#viewQuotation').modal('show')
    //             $('#quotation-details').html('')
    //             let quotation = data.quotation;
    //             $(`
    //             <div class="container-fluid">
    //                 <div class="row">
    //                     <div class="col-12">
    //                     <div class="card">
    //                         <div class="card-header">
    //                             <h5>Quotation</h5>
    //                         </div>
    //                         <div class="card-body">
    //                             <p><strong>From :</strong> ${quotation.name}</p>
    //                             <p><strong>Email :</strong> ${quotation.email}</p>
    //                             <p><strong>Subject :</strong> ${quotation.subject}</p>
    //                             <p><strong>Message :</strong><br> </p><p class="text-justify">&emsp;&emsp;&emsp;${quotation.message}</p>
    //                             <p><strong>Files : </strong><button class="btn btn-sm" id="download-quotation-files" data-id="${quotation.id}"><i class="nc-icon nc-cloud-download-93 mr-2"></i>Download</button></p>
    //                         </div>
    //                     </div>
    //                 </div>
    //             </div>
    //             `).appendTo('#quotation-details')
    //         },
    //         error: function (data) {
    //             Swal.fire(
    //                 'Opps!',
    //                 'Wen something wrong',
    //                 'error'
    //             )
    //         }
    //     });
    // });

    //download quotation files
    $(document).on('click', '#download-quotation-files', function (e) {
        e.preventDefault()
        let id = $(this).data('id');
        window.location.assign('/quotation/download?id=' + id + '&uid=' + Math.random().toString(36));
    })
    $(document).on('click', '.edit-quotation', function (e) {
        e.preventDefault
        e.stopImmediatePropagation()
    })
    $(document).on('click', '.delete-quotation', function (e) {
        e.preventDefault
        e.stopImmediatePropagation()
        let id = $(this).data('id');
        let quotation = this;
        swal.fire({
            title: "Are you sure!",
            text: 'All data of this quotation will be deleted',
            type: "warning",
            confirmButtonColor: "#c73b39",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "DELETE",
                    url: `/quotation/${id}`,
                    data: {
                        id: id
                    },
                    success: function (data) {
                        Swal.fire(
                            'Deleted!',
                            'Data has been deleted.',
                            'success'
                        )
                        quotation.closest('tr').remove();
                    },
                    error: function (data) {
                        console.log(data)
                    }
                });
            }
        });
    });

    //CONFIG INDEX PAGE
    //show add modal form
    $('.btn-add-admin').click(function (e) {
        $('#addAdmin').modal('show')
        $('#adminForm').trigger('reset')
        $('#admin_mode').html('Add Administrator')
        $('.btn-admin').attr('id','save-admin')
        $('.btn-admin').html('Save Changes')
        $('.pass-div').removeClass('d-none')
        $('#change-pass').addClass('d-none')
        $('.new-pass').addClass('d-none')
        $('.err').html('')
    });
    // edit admin users
    $(document).on('click','.btn-edit-admin',function (e) {
        e.preventDefault();
        $('#adminForm').trigger('reset')
        $('.err').html('')
        $('#admin_mode').html('Edit Administrator')
        $('.btn-admin').attr('id','update-admin')
        $('.btn-admin').html('Update')
        $('.new-pass').html('')
        let admin = $(this).data('id');
        $.ajax({
            type:'get',
            url: '/config/'+admin,
            success: function(data){
                $('input[name=name]').val(data.name)
                $('input[name=email]').val(data.email)
                $('input[name=id]').val(data.id)
                $('.pass-div').addClass('d-none')
                $('#change-pass').removeClass('d-none')
                $('#change-pass').html(`
                        <div class="text-right">
                            <input type="checkbox" name="changepass" id="changepass" />
                            <label for="changepass">Change Password</label>
                        </div>
                `)
                $('#addAdmin').modal('show')
            }
        });
    });
    $(document).on('click','#changepass',function(e){
        e.preventDefault
        if($('.pass-div').hasClass('d-none'))
        {
            $('.pass-div').removeClass('d-none')
            $('.new-pass').removeClass('d-none')
            $('.new-pass').html(`
                <label for="name">Old Password :</label>
                <input name="old_password" type="password" class="form-control">
                <small class="text-danger err" id="old-password-error">  
                </small>
            `)
        } else {
            $('.pass-div').addClass('d-none')
            $('.new-pass').html('')
        }
    })
    //save to database
    $(document).on('click','#save-admin',function (e) {
        e.preventDefault()
        let formdata = $('#adminForm').serializeArray();
        $.ajax({
            type: "POST",
            url: `/admin`,
            data: formdata,
            success: function (data) {
                $('#addAdmin').modal('hide')
                Swal.fire(
                    'Added!',
                    'Admin has been added.',
                    'success'
                ).then((result) => {
                    if (result.value) {
                        window.location.reload()
                    }
                })
            },
            error: function (data) {
                data = JSON.parse(data.responseText)
                let error = data.errors
                $('#admin-name-error').html('')
                $('#admin-email-error').html('')
                $('#admin-password-error').html('')
                if (error.name) {
                    $(`
                        <strong>${error.name}</strong>
                    `).appendTo('#admin-name-error')
                }
                if (error.email) {
                    $(`
                        <strong>${error.email}</strong>
                    `).appendTo('#admin-email-error')
                }
                if (error.password || data.error) {
                    $(`
                        <strong>${error.password}</strong>
                    `).appendTo('#admin-password-error')
                }
            }
        });
    });
    //update admin details 
    $(document).on('click','#update-admin',function (e) {
        e.preventDefault()
        let formdata = $('#adminForm').serializeArray();
        let changepass=$('#changepass').prop('checked')
        formdata.push({name:'changepass',value:changepass})
        $.ajax({
            type: "PATCH",
            url: `/admin`,
            data: formdata,
            success: function (data) {
                console.log(data)
                $('#addAdmin').modal('hide')
                Swal.fire(
                    'Updated!',
                    'Admin has been updated.',
                    'success'
                ).then((result) => {
                    if (result.value) {
                        window.location.reload()
                    }
                })
            },
            error: function (data) {
                data = JSON.parse(data.responseText)
                console.log(data)
                let error = data.errors
                $('#admin-name-error').html('')
                $('#admin-email-error').html('')
                $('#admin-password-error').html('')
                Object.keys(error).forEach(function(err){
                    $('#admin-'+err+'-error').html(`<strong>${error[err]}</strong>`)
                })
            }
        });
    });
    //delete admin
    $('.delete-admin').click(function (e) {
        e.preventDefault();
        let admin = $(this).data('id');
        let row = this;
        swal.fire({
            title: "Are you sure!",
            text: 'All data of this admin will be deleted',
            type: "warning",
            confirmButtonColor: "#c73b39",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'DELETE',
                    url: `/config/admin/delete/${admin}`,
                    success: function (data) {
                        Swal.fire(
                            'Deleted!',
                            'Admin has been deleted.',
                            'success'
                        ).then((result) => {
                            if (result.value) {
                                window.location.reload()
                            }
                        })
                        row.closest('tr').remove()
                    },
                    error: function (data) {
                        Swal.fire(
                            'Opps!',
                            'Wen something wrong',
                            'error'
                        )
                    }
                });
            }
        });
    })
    //show pricing modal
    $(document).on('click','.btn-add-price',function(e){
        e.preventDefault
        $('#addHour').modal('show')
        $('#hourForm').trigger('reset')
        $('.btn-price').attr('id','add-price')
    })
    //add hour pricing
    $(document).on('click','#add-price',function(e){
        e.preventDefault
        let formData=$('#hourForm').serializeArray()
        $.ajax({
            type: "POST",
            data:formData,
            url: '/config/price',
            success: function (data) {
                $('.pricing').load(location.href+' .pricing > *')
                $('#addHour').modal('hide')
            },
            error: function (data) {
                data=JSON.parse(data.responseText)
                let error=data.errors
                Object.keys(error).forEach(function(err){
                    $('#'+err+'-error').html(`<strong>${error[err]}</strong>`)
                })
            }
        });
    })
    //show edit modal with price data
    $(document).on('click','.edit-price',function (e) {
        let hour_id = $(this).data('id')
        $('.btn-price').attr('id','update-price')
        $.ajax({
            type: "GET",
            url: `/config/price/${hour_id}`,
            success: function (data) {
                $('input[name=hour]').val(data.hour.hours)
                $('input[name=price]').val(data.hour.hour_price)
                $('input[name=price_id]').val(data.hour.id)
                $('#addHour').modal('show')
                // $('#hours').html(`${data.hour.hours > 1 ? data.hour.hours+'hrs' : data.hour.hours+'hr'}`)
            },
            error: function (data) {
                Swal.fire(
                    'Opps!',
                    'Went something wrong',
                    'error'
                )
            }
        });
    })
    // //edit price
    // $('.edit-price').click(function (e) {
    //     let hour_id = $(this).data('id')
    //     $.ajax({
    //         type: "GET",
    //         url: `/config/price/${hour_id}`,
    //         success: function (data) {
    //             $('#hourForm').html('')
    //             $('#editHour').modal('show')
    //             $('#hours').html(`${data.hour.hours > 1 ? data.hour.hours+'hrs' : data.hour.hours+'hr'}`)
    //             $(`
    //                 <div class="col-md-4 offset-4">
    //                     <div class="text-center">
    //                         <h1 id="price-preview">${data.hour.hour_price}€</h1>
    //                     </div>
    //                     <div class="form-group">
    //                         <input id="hour_price" min="0" class="form-control" type="number" value="${data.hour.hour_price}" data-id="${data.hour.id}">
    //                     </div>
    //                 </div>
    //             `).appendTo('#hourForm')
    //         },
    //         error: function (data) {
    //             Swal.fire(
    //                 'Opps!',
    //                 'Went something wrong',
    //                 'error'
    //             )
    //         }
    //     });
    // })
    //change click
    $(document).on('change', '#hour_price', function (e) {
        let temp_price = e.target.value;
        $('#price-preview').html(`${temp_price}€`)
    })
    //update price
    $(document).on('click','#update-price',function (e) {
        let hour = $('input[name=hour]').val()
        let hour_price = $('input[name=price]').val()
        let formData=$('#hourForm').serializeArray()
        if (hour_price < 0) {
            $('#editHour').modal('hide')
            Swal.fire(
                'Opps!',
                'Price must not be less than 0',
                'error'
            )
        }
        if (hour_price >= 0) {
            $.ajax({
                type: "PATCH",
                url: `/config/price`,
                data: formData,
                success: function (data) {
                    $('#addHour').modal('hide')
                    $('.pricing').load(location.href+' .pricing > *')
                    // Swal.fire(
                    //     'Updated!',
                    //     `Price has been updated.`,
                    //     'success'
                    // ).then((result) => {
                    //     $('.pricing').load(location.href+' .pricing > *')
                    //     // if (result.value) {
                    //     //     window.location.reload()
                    //     // }
                    // })
                },
                error: function (data) {
                    Swal.fire(
                        'Opps!',
                        'Wen something wrong',
                        'error'
                    )
                }
            });
        }
    })
    $(document).on('click','.delete-price',function(e){
        e.preventDefault
        let id=$(this).data('id')
        swal.fire({
            title: "Are you sure!",
            // text: 'All data of this quotation will be deleted',
            type: "warning",
            confirmButtonColor: "#c73b39",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "DELETE",
                    url: `/config/price/${id}`,
                    success: function (data) {
                        Swal.fire({
                            title: "Deleted!",
                            text: "Your data has been deleted.",
                            type: "success",
                            buttons: false,
                            timer: 2000
                        }).then(function(){
                            $('.pricing').load(location.href+' .pricing > *')
                        });
                    },
                    error: function (data) {
                        console.log(data)
                    }
                });
            }
        });

    })

    // adonis scripts

    $("#search_me").autocomplete({
        source: [],
        search: function( event, ui ) {
            let value = $(this).val()
            let params = 'keyword=' + value
            sendAjax(params, '/generalSearch/?'+params, 'GET', function(data) {
                console.log(data, 'data')
                $("#search_me").autocomplete("option", "source", data)
                $("#search_me").autocomplete("search", value)
            })
        },
        select: function( event, ui ) {
            console.log({event, ui})
        }
    });
})

function setSearchedItems(value) {
}
// general functions

function sendAjax(data, url, type, callback) {
    if (type == 'GET') {
        $.ajax({
            type: type,
            url: url,
            data:{},
            success: function (data) {
                callback(data)
            },
            error: function (data) {
                callback(data)
            },
        });
    } else {
        $.ajax({
            type: type,
            url: url,
            data: data,
            success: function (data) {
                callback()
            },
            error: function (data) {
                callback()
            },
        });
    }

}

function startLoading() {
    $('.loading').show()
}

function stopLoading() {
    $('.loading').hide()
}