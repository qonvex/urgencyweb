jQuery(document).ready(function ($) {

    skew_size_before();
    skew_size_after();
    credit_up_and_down_hour();
    up_and_down_hour();

    $('.credit-addhour-wrapper .payment-method-wrapper .payment-method-form .credit-card-wrapper .credit-card-number input').keypress(function (event) {
        var val = $(this).val();
        var newval = '';
        val = val.replace(/\s/g, '');
        for (var i = 0; i < val.length; i++) {
            if (i % 4 == 0 && i > 0) newval = newval.concat(' ');
            newval = newval.concat(val[i]);
        }
        $(this).val(newval);
    });

    var totalhourslength = $('.hm-section2 .number-hours-wrapper input').length;
    var totalhourslengthhalf = totalhourslength / 2;
    var wholetotalhourslength = parseInt(totalhourslengthhalf);
    var defaulthm = 8;
    $('.hm-section2 .number-hours-wrapper').each(function (index, el) {
        $('.hm-section2 .number-hours-wrapper input').each(function (index, el) {
            $(this).addClass('displaynone');
        });
        var activehour = $(this).find(':nth-child(' + (defaulthm) + ')').addClass('activehour').siblings('input').removeClass('activehour');
        var activehour = $(this).find(':nth-child(' + (defaulthm) + ')').removeClass('displaynone');

        var activehourprev = $(this).find(':nth-child(' + ((defaulthm) - 1) + ')').addClass('activehourprev').siblings('input').removeClass('activehourprev');
        var activehourprev = $(this).find(':nth-child(' + ((defaulthm) - 1) + ')').removeClass('displaynone');

        var activehourprevprev = $(this).find(':nth-child(' + ((defaulthm) - 2) + ')').addClass('activehourprevprev').siblings('input').removeClass('activehourprevprev');
        var activehourprevprev = $(this).find(':nth-child(' + ((defaulthm) - 2) + ')').removeClass('displaynone');

        var activehournext = $(this).find(':nth-child(' + ((defaulthm) + 1) + ')').addClass('activehournext').siblings('input').removeClass('activehournext');
        var activehournext = $(this).find(':nth-child(' + ((defaulthm) + 1) + ')').removeClass('displaynone');

        var activehournextnext = $(this).find(':nth-child(' + ((defaulthm) + 2) + ')').addClass('activehournextnext').siblings('input').removeClass('activehournextnext');
        var activehournextnext = $(this).find(':nth-child(' + ((defaulthm) + 2) + ')').removeClass('displaynone');
    });

    var totalhourslengthpop = $('.credit-addhour-wrapper .number-hours-wrapper input').length;
    var totalhourslengthpophalf = totalhourslengthpop / 2;
    var wholetotalhourslengthpop = parseInt(totalhourslengthpophalf);
    var defaultcredit = 8;
    $('.credit-addhour-wrapper .number-hours-wrapper').each(function (index, el) {
        $('.credit-addhour-wrapper .number-hours-wrapper input').each(function (index, el) {
            $(this).addClass('displaynone');
        });
        var activehour = $(this).find(':nth-child(' + (defaultcredit) + ')').addClass('activehour').siblings('input').removeClass('activehour');
        var activehour = $(this).find(':nth-child(' + (defaultcredit) + ')').removeClass('displaynone');

        var activehourprev = $(this).find(':nth-child(' + ((defaultcredit) - 1) + ')').addClass('activehourprev').siblings('input').removeClass('activehourprev');
        var activehourprev = $(this).find(':nth-child(' + ((defaultcredit) - 1) + ')').removeClass('displaynone');

        var activehourprevprev = $(this).find(':nth-child(' + ((defaultcredit) - 2) + ')').addClass('activehourprevprev').siblings('input').removeClass('activehourprevprev');
        var activehourprevprev = $(this).find(':nth-child(' + ((defaultcredit) - 2) + ')').removeClass('displaynone');

        var activehournext = $(this).find(':nth-child(' + ((defaultcredit) + 1) + ')').addClass('activehournext').siblings('input').removeClass('activehournext');
        var activehournext = $(this).find(':nth-child(' + ((defaultcredit) + 1) + ')').removeClass('displaynone');

        var activehournextnext = $(this).find(':nth-child(' + ((defaultcredit) + 2) + ')').addClass('activehournextnext').siblings('input').removeClass('activehournextnext');
        var activehournextnext = $(this).find(':nth-child(' + ((defaultcredit) + 2) + ')').removeClass('displaynone');
    });

    var onloadActive = $('#hourInputs .activehour').data('id');
    $('#hour-price').html(`${onloadActive}`)

    $('.hm-section2 #hourInputs').mousewheel(function (turn, delta) {

        if (delta == 1) {
            $('.hm-section2 .btn-up').trigger('click');
        } else {
            $('.hm-section2 .btn-down').trigger('click');
        }
        return false;
    });

    $('.credit-addhour-wrapper #hourInputs').mousewheel(function (turn, delta) {

        if (delta == 1) {
            $('.credit-addhour-wrapper .btn-up').trigger('click');
        } else {
            $('.credit-addhour-wrapper .btn-down').trigger('click');
        }
        return false;
    });

    // $('.hm-section2 .number-hours-wrapper input:nth-child(6)').addClass('activehour').siblings('input').removeClass('activehour');

    $(window).resize(function () {
        skew_size_before();
        skew_size_after();
    });

    $('.hours-slide.carousel').carousel({
        interval: false,
        autoplay: false,
    });

    $('.mouse').click(function () {
        var $scrolldown = $('.hm-section2').offset();
        $('html, body').animate({
            scrollTop: $scrolldown.top
        }, 'slow');
        return false;
    });

    // $(window).scroll(function(){
    //     var scroll = (parseInt($(window).scrollTop() / 430 - 6));
    //     $('.slant-before, .slant-after').css({ transform: 'rotate(' + scroll + 'deg)' });
    //     console.log(scroll);
    // });


    var testimonials = $('.owl-testimonials');
    testimonials.owlCarousel({
        margin: 100,
        nav: true,
        loop: true,
        autoplay: false,
        autoHeight: true,
        responsive: {
            0: {
                items: 1
            },
        }
    });

    var projectPreview = $('.owl-project-preview');
    projectPreview.owlCarousel({
        margin: 100,
        nav: true,
        loop: true,
        autoplay: false,
        autoHeight: true,
        responsive: {
            0: {
                items: 1
            },
        }
    });

    var OwlExpertiseMobile = $('.owl-expertise-mobile');
    OwlExpertiseMobile.owlCarousel({
        margin: 10,
        nav: true,
        loop: true,
        autoplay: false,
        autoHeight: true,
        responsive: {
            0: {
                items: 3
            },
            767: {
                items: 6
            },
            991: {
                items: 9
            },
        }
    });

    // var OwlExpertiseMobile = $('.owl-expertise-mobile');
    // OwlExpertiseMobile.owlCarousel({
    //     margin: 100,
    //     nav: true,
    //     loop: true,
    //     autoplay: false,
    //     autoHeight:true,
    //     responsive: {
    //         0: {
    //             items: 1
    //         },
    //         767: {
    //             items: 6
    //         },
    //         991: {
    //             items: 9
    //         },
    //     }
    // });

    $(document).on('click', '.hm-section2 .quotation-wrapper .attach-wrapper .attachments > div .myclose', function () {
        var close_attach = $(this).data('id');
        $('.' + close_attach).remove();
    });

    $(document).on('click', '.new-project-wrapper .attach-wrapper .attachments > div .myclose', function () {
        var close_attach = $(this).data('id');
        $('.' + close_attach).remove();
    });

    $(document).on('mouseenter', '.hm-section3 .expertise-wrapper .experise-inner-wrapper ul li .expertise-img', function () {
        $(this).siblings('.expertise-img-hover').css('opacity', '1');
    });

    $(document).on('mouseleave', '.hm-section3 .expertise-wrapper .experise-inner-wrapper ul li .expertise-img', function () {
        $(this).siblings('.expertise-img-hover').css('opacity', '0');
    });

    $(document).on('mouseenter', '.hm-section3 .expertise-wrapper .experise-inner-wrapper .expertise-img-wrapper .expertise-img', function () {
        $(this).siblings('.expertise-img-hover').css('opacity', '1');
    });

    $(document).on('mouseleave', '.hm-section3 .expertise-wrapper .experise-inner-wrapper .expertise-img-wrapper .expertise-img', function () {
        $(this).siblings('.expertise-img-hover').css('opacity', '0');
    });

    $('.testimonials-inner-wrapper .prev_next_nav .prev').click(function (event) {
        $('.owl-prev').trigger('click');
    });

    $('.testimonials-inner-wrapper .prev_next_nav .next').click(function (event) {
        $('.owl-next').trigger('click');
    });

    $('.project-view-inner-preview .prev_next_nav .prev').click(function (event) {
        $('.owl-prev').trigger('click');
    });

    $('.project-view-inner-preview .prev_next_nav .next').click(function (event) {
        $('.owl-next').trigger('click');
    });

    // $(window).load(function () {
    //     $('.account-wrapper .user-inner-wrapper .myaccount-projects-wrapper table tr label input').each(function (index, el) {
    //         if ($(this).prop("checked")) {
    //             $(this).closest('td').siblings('td').children('.progress-status').addClass('done');
    //             $(this).closest('td').siblings('td').children('.progress-status').removeClass('in-progress');
    //             $(this).closest('td').siblings('td').children('.progress-status').text('done');
    //         } else {
    //             $(this).closest('td').siblings('td').children('.progress-status').addClass('in-progress');
    //             $(this).closest('td').siblings('td').children('.progress-status').removeClass('done');
    //             $(this).closest('td').siblings('td').children('.progress-status').text('In Progress');
    //         }
    //     });
    // });

    // $('.account-wrapper .user-inner-wrapper .myaccount-projects-wrapper table tr label input').click(function (event) {
    //     if ($(this).prop("checked") == true) {
    //         $(this).closest('td').siblings('td').children('.progress-status').addClass('done');
    //         $(this).closest('td').siblings('td').children('.progress-status').removeClass('in-progress');
    //         $(this).closest('td').siblings('td').children('.progress-status').text('done');
    //     } else if ($(this).prop("checked") == false) {
    //         $(this).closest('td').siblings('td').children('.progress-status').addClass('in-progress');
    //         $(this).closest('td').siblings('td').children('.progress-status').removeClass('done');
    //         $(this).closest('td').siblings('td').children('.progress-status').text('In Progress');
    //     }
    // });


    $('#CreateAccount').on('hidden.bs.modal', function () {
        $('body').css('padding', '0px');
        $('body').removeAttr('style');
    });

    $(document).on('click', '.btn-create-account', function () {
        $('body').css('overflow', 'hidden');
        $('.urgence-modal').css('overflow-x', 'hidden');
        $('.urgence-modal').css('overflow-y', 'auto');
        $('#Login').modal('hide');
    });

    $('#ForgotPassword').on('hidden.bs.modal', function () {
        $('body').css('padding', '0px');
        $('body').removeAttr('style');
    });

    $(document).on('click', '.btn-forgot-password', function () {
        $('body').css('overflow', 'hidden');
        $('.urgence-modal').css('overflow-x', 'hidden');
        $('.urgence-modal').css('overflow-y', 'auto');
        $('#Login').modal('hide');
        $('#ForgotPassword').modal('show');
    });

    $('#ForgotPasswordVerification').on('hidden.bs.modal', function () {
        $('body').css('padding', '0px');
        $('body').removeAttr('style');
    });

    $(document).on('click', '.btn-forgot-password-submit', function () {
        $('body').css('overflow', 'hidden');
        $('.urgence-modal').css('overflow-x', 'hidden');
        $('.urgence-modal').css('overflow-y', 'auto');
        $('#ForgotPassword').modal('hide');
        $('#ForgotPasswordVerification').modal('show');
    });
});


function skew_size_before() {
    var tg_skew = 0.05729;
    var top_pad = 35;
    var skew_w = $(window).width() + 200;
    var skew_h = Math.floor(skew_w * tg_skew + 210);

    var main_cont_pad = Math.floor(skew_h / 2);

    $(".slant-before").css({
        'width': skew_w + 'px',
        'height': skew_h + 'px'
    });
    $(".slant-before").css({
        'bottom': -main_cont_pad + 25
    });
}

function skew_size_after() {
    var tg_skew = 0.05729;
    var top_pad = 35;
    var skew_w = $(window).width() + 200;
    var skew_h = Math.floor(skew_w * tg_skew + 210);

    var main_cont_pad = Math.floor(skew_h / 2);

    $(".slant-after").css({
        'width': skew_w + 'px',
        'height': skew_h + 'px'
    });
    $(".slant-after").css({
        'top': -main_cont_pad + 25
    });
}

function up_and_down_hour() {

    $('.hm-section2 .btn-down').click(function () {
        var totallength = $('.hm-section2 .number-hours-wrapper input').length;
        var activehourcenter = (totallength / 2) + 1;
        // var activehour = parseInt(activehourcenter);
        var activehour = parseInt(8);
        var activehourprev = activehour - 1;
        var activehourprevprev = activehour - 2;
        var activehournext = activehour + 1;
        var activehournextnext = activehour + 2;
        var activeinput = $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehour + ')');
        var activeinputprev = $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehourprev + ')');
        var activeinputprevprev = $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehourprevprev + ')');
        var activeinputnext = $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehournext + ')');
        var activeinputnextnext = $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehournextnext + ')');
        var hourinputval = $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehour + ')').val();
        var firstchild = $('.hm-section2 .number-hours-wrapper input:nth-child(1)');
        activeinput.addClass('activehour').siblings('input').removeClass('activehour').addClass('displaynone');
        activeinput.removeClass('displaynone');

        activeinputprev.addClass('activehourprev').siblings('input').removeClass('activehourprev');
        activeinputprev.removeClass('displaynone');

        activeinputprevprev.addClass('activehourprevprev').siblings('input').removeClass('activehourprevprev');
        activeinputprevprev.removeClass('displaynone');

        activeinputnext.addClass('activehournext').siblings('input').removeClass('activehournext');
        activeinputnext.removeClass('displaynone');

        activeinputnextnext.addClass('activehournextnext').siblings('input').removeClass('activehournextnext');
        activeinputnextnext.removeClass('displaynone');
        // firstchild.remove();
        if (hourinputval == '1 hour') {
            var hourinputnum = hourinputval.replace(' hour', '');
        } else {
            var hourinputnum = hourinputval.replace(' hours', '');
        }
        $('.price-wrapper .price').text($('.activehour').data('id'));
        // firstchild.hide(2000, function(){ firstchild.remove(); });
        $('.hm-section2 .number-hours-wrapper input:last-child').fadeOut("slow", function () {
            // After animation completed:
            $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehourprevprev + ')').animate({
                top: '-20px'
            }).animate({
                top: '0px'
            });
            $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehourprev + ')').animate({
                top: '-20px'
            }).animate({
                top: '0px'
            });
            $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehournext + ')').animate({
                top: '-20px'
            }).animate({
                top: '0px'
            });
            $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehournextnext + ')').animate({
                top: '-20px'
            }).animate({
                top: '0px'
            });
            firstchild.remove();
            $('.hm-section2 .number-hours-wrapper').append(firstchild);
            // console.log($('.hm-section2 .number-hours-wrapper input:nth-child(13)').val());
        });
    });

    $('.hm-section2 .btn-up').click(function () {
        var totallength = $('.hm-section2 .number-hours-wrapper input').length;
        var activehourcenter = (totallength / 2) - 1;
        // var activehour = parseInt(activehourcenter);
        var activehour = parseInt(8);
        var activehourprev = activehour - 1;
        var activehourprevprev = activehour - 2;
        var activehournext = activehour + 1;
        var activehournextnext = activehour + 2;
        var activeinput = $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehour + ')');
        var activeinputprev = $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehourprev + ')');
        var activeinputprevprev = $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehourprevprev + ')');
        var activeinputnext = $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehournext + ')');
        var activeinputnextnext = $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehournextnext + ')');
        var hourinputval = $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehour + ')').val();
        var lastchild = $('.hm-section2 .number-hours-wrapper input:last-child');
        activeinput.addClass('activehour').siblings('input').removeClass('activehour').addClass('displaynone');
        activeinput.removeClass('displaynone');

        activeinputprev.addClass('activehourprev').siblings('input').removeClass('activehourprev');
        activeinputprev.removeClass('displaynone');

        activeinputprevprev.addClass('activehourprevprev').siblings('input').removeClass('activehourprevprev');
        activeinputprevprev.removeClass('displaynone');

        activeinputnext.addClass('activehournext').siblings('input').removeClass('activehournext');
        activeinputnext.removeClass('displaynone');

        activeinputnextnext.addClass('activehournextnext').siblings('input').removeClass('activehournextnext');
        activeinputnextnext.removeClass('displaynone');
        // lastchild.remove();
        if (hourinputval == '1 hour') {
            var hourinputnum = hourinputval.replace(' hour', '');
        } else {
            var hourinputnum = hourinputval.replace(' hours', '');
        }

        $('.price-wrapper .price').text($('.activehour').data('id'));
        // lastchild.hide(2000, function(){ lastchild.remove(); });
        $('.hm-section2 .number-hours-wrapper input:last-child').fadeOut("slow", function () {
            // After animation completed:
            $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehourprevprev + ')').animate({
                top: '20px'
            }).animate({
                top: '0px'
            });
            $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehourprev + ')').animate({
                top: '20px'
            }).animate({
                top: '0px'
            });
            $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehournext + ')').animate({
                top: '20px'
            }).animate({
                top: '0px'
            });
            $('.hm-section2 .number-hours-wrapper input:nth-child(' + activehournextnext + ')').animate({
                top: '20px'
            }).animate({
                top: '0px'
            });
            lastchild.remove();
            $('.hm-section2 .number-hours-wrapper').prepend(lastchild);
        });
    });

    var lastScrollTop = 0;
    $(".hm-section2 #hourInputs").scroll(function () {

        var st = $(this).scrollTop();
        if (st > lastScrollTop) {
            $('.hm-section2 .btn-down').trigger('click');
        } else {
            $('.hm-section2 .btn-up').trigger('click');
        }
        lastScrollTop = st;
    });

}

function credit_up_and_down_hour() {

    $('.credit-addhour-wrapper .btn-down').click(function () {
        var totallength = $('.credit-addhour-wrapper .number-hours-wrapper input').length;
        var activehourcenter = (totallength / 2) + 1;
        // var activehour = parseInt(activehourcenter);
        var activehour = parseInt(8);
        var activehourprev = activehour - 1;
        var activehourprevprev = activehour - 2;
        var activehournext = activehour + 1;
        var activehournextnext = activehour + 2;
        var activeinput = $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehour + ')');
        var activeinputprev = $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehourprev + ')');
        var activeinputprevprev = $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehourprevprev + ')');
        var activeinputnext = $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehournext + ')');
        var activeinputnextnext = $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehournextnext + ')');
        var hourinputval = $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehour + ')').val();
        var firstchild = $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(1)');
        activeinput.addClass('activehour').siblings('input').removeClass('activehour').addClass('displaynone');
        activeinput.removeClass('displaynone');

        activeinputprev.addClass('activehourprev').siblings('input').removeClass('activehourprev');
        activeinputprev.removeClass('displaynone');

        activeinputprevprev.addClass('activehourprevprev').siblings('input').removeClass('activehourprevprev');
        activeinputprevprev.removeClass('displaynone');

        activeinputnext.addClass('activehournext').siblings('input').removeClass('activehournext');
        activeinputnext.removeClass('displaynone');

        activeinputnextnext.addClass('activehournextnext').siblings('input').removeClass('activehournextnext');
        activeinputnextnext.removeClass('displaynone');
        // firstchild.remove();
        // if (hourinputval == '1 hour') {
        //     var hourinputnum = hourinputval.replace(' hour', '');
        // } else {
        //     var hourinputnum = hourinputval.replace(' hours', '');
        // }
        $('.price-wrapper .price').text($('.activehour').data('id'));

        $('.credit-addhour-wrapper .number-hours-wrapper input:last-child').fadeOut("slow", function () {
            // After animation completed:
            $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehourprevprev + ')').animate({
                top: '-20px'
            }).animate({
                top: '0px'
            });
            $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehourprev + ')').animate({
                top: '-20px'
            }).animate({
                top: '0px'
            });
            $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehournext + ')').animate({
                top: '-20px'
            }).animate({
                top: '0px'
            });
            $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehournextnext + ')').animate({
                top: '-20px'
            }).animate({
                top: '0px'
            });
            firstchild.remove();
            $('.credit-addhour-wrapper .number-hours-wrapper').append(firstchild);
        });
    });

    $('.credit-addhour-wrapper .btn-up').click(function () {
        var totallength = $('.credit-addhour-wrapper .number-hours-wrapper input').length;
        var activehourcenter = (totallength / 2) - 1;
        // var activehour = parseInt(activehourcenter);
        var activehour = parseInt(8);
        var activehourprev = activehour - 1;
        var activehourprevprev = activehour - 2;
        var activehournext = activehour + 1;
        var activehournextnext = activehour + 2;
        var activeinput = $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehour + ')');
        var activeinputprev = $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehourprev + ')');
        var activeinputprevprev = $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehourprevprev + ')');
        var activeinputnext = $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehournext + ')');
        var activeinputnextnext = $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehournextnext + ')');
        var hourinputval = $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehour + ')').val();
        var lastchild = $('.credit-addhour-wrapper .number-hours-wrapper input:last-child');
        activeinput.addClass('activehour').siblings('input').removeClass('activehour').addClass('displaynone');
        activeinput.removeClass('displaynone');

        activeinputprev.addClass('activehourprev').siblings('input').removeClass('activehourprev');
        activeinputprev.removeClass('displaynone');

        activeinputprevprev.addClass('activehourprevprev').siblings('input').removeClass('activehourprevprev');
        activeinputprevprev.removeClass('displaynone');

        activeinputnext.addClass('activehournext').siblings('input').removeClass('activehournext');
        activeinputnext.removeClass('displaynone');

        activeinputnextnext.addClass('activehournextnext').siblings('input').removeClass('activehournextnext');
        activeinputnextnext.removeClass('displaynone');
        // lastchild.remove();
        if (hourinputval == '1 hour') {
            var hourinputnum = hourinputval.replace(' hour', '');
        } else {
            var hourinputnum = hourinputval.replace(' hours', '');
        }
        $('.price-wrapper .price').text($('.activehour').data('id'));

        $('.credit-addhour-wrapper .number-hours-wrapper input:last-child').fadeOut("slow", function () {
            // After animation completed:
            $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehourprevprev + ')').animate({
                top: '20px'
            }).animate({
                top: '0px'
            });
            $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehourprev + ')').animate({
                top: '20px'
            }).animate({
                top: '0px'
            });
            $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehournext + ')').animate({
                top: '20px'
            }).animate({
                top: '0px'
            });
            $('.credit-addhour-wrapper .number-hours-wrapper input:nth-child(' + activehournextnext + ')').animate({
                top: '20px'
            }).animate({
                top: '0px'
            });
            lastchild.remove();
            $('.credit-addhour-wrapper .number-hours-wrapper').prepend(lastchild);
        });
        // lastchild.hide('2000', function(){ firstchild.remove(); });
        // lastchild.slideUp("slow", function() { $(this).remove(); } );
        // lastchild.fadeOut(2000, function() { lastchild.remove(); });
    });

    var lastScrollTop = 0;
    $(".credit-addhour-wrapper #hourInputs").scroll(function () {

        var st = $(this).scrollTop();
        if (st > lastScrollTop) {
            $('.credit-addhour-wrapper .btn-down').trigger('click');
        } else {
            $('.credit-addhour-wrapper .btn-up').trigger('click');
        }
        lastScrollTop = st;
    });

}

//test commit front
