$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    //======================================================
    //CUSTOMER PAYMENT
    //======================================================
    var price;
    var hours;
    var hour_id;
    paypal.Buttons({
        createOrder: function (data, actions) {
            hours = parseInt($('#hourInputs .activehour').val());
            price = parseInt($('.activehour').data('id'));
            hour_id = $('.activehour').data('hour_id');
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        currency_code: 'EUR',
                        value: price
                    }
                }]
            });
        },
        onApprove: function (data, actions) {
            startLoading();
            data.hours = parseInt(hours);
            data.hour_id = parseInt(hour_id);
            $.ajax({
                type: 'POST',
                url: '/verifyTransaction',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data,
                success: function (data) {
                    window.location.reload()
                },
                error: function (data) {
                    stopLoading();
                    $('#CreditAddhour').modal('hide')
                    window.location.reload()
                }
            })
        }
    }).render('#paypal-button-container');
})

$('#orderNowModal').click(function (e) {
    e.preventDefault()
    $('#OrderNow').modal('show');
    let hours = parseInt($('#hourInputs .activehour').val());
    console.log(hours)
    let price = parseInt($('#hour-price').html());
    $('#hour-modal').html(`${hours} ${hours > 1 ? 'hrs' : 'hr'}`)
    $('#hour-price-modal').html(price)
})

$('#check-account').change(function (e) {
    if (this.checked) {
        $('#new-customer').attr('hidden', true)
        $('#old-customer').attr('hidden', false)
    }
    if (!this.checked) {
        $('#new-customer').attr('hidden', false)
        $('#old-customer').attr('hidden', true)
    }
})


function startLoading() {
    $('#loader').show();
}

function stopLoading(params) {
    $('#loader').hide();
}
