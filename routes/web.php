<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});
Route::get('/', function () {
    if (auth()->check()) {
        return redirect('/account');
    }
    elseif(auth('admin')->check()) {
        return redirect('/admin');
    }
    return view('welcome');
});



/*
|--------------------------------------------------------------------------
| CUSTOMER
|--------------------------------------------------------------------------
*/
//CUSTOMER AUTHENTICATION ROUTE
Auth::routes();
Route::get('/password/reset/{token}', '\App\Http\Controllers\Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::get('/verification/code/{token}', 'Auth\VerifyCodeController@showCodeForm');
Route::post('/resend/code/{token}', 'Auth\VerifyCodeController@resendCode')->name('resend.code');
Route::post('/logout/user', 'Auth\LoginController@logout')->name('logout');
Route::post('/verification/{token}', 'Auth\VerifyCodeController@verifyCode')->name('verify.code');
Route::get('/getprice', 'Customer\PaymentsController@getPrice');
Route::post('/register/customer', 'Auth\RegisterController@newCustomerOrder');


//QOUTATION ROUTE
Route::post('/quotation', 'Customer\QuotationController@send')->name('quotation.send');

//ROUTE FOR CUSTOMER ACCOUNT
Route::group(['middleware' => 'auth'], function()
{
    Route::get('/index', function () {
        return view('welcome');
    });

    //route to home
    Route::get('/home', 'HomeController@index')->name('home');

    //PROFILE
    Route::put('/profile/{user_id}', 'Customer\AccountController@updateProfile');

    //ACCOUNT 
    Route::post('/project', 'Customer\AccountController@store')->name('project.store');
    Route::get('/account/project/{id}', 'Customer\AccountController@view')->name('project.view');
    Route::get('/account', 'Customer\AccountController@index')->name('account.index');
    Route::get('/customer/project', 'Customer\AccountController@sortProject');

    //TRANSACTIoN/PAYMENT
    Route::post('/verifyTransaction','Customer\PaymentsController@verifyTransaction');

    //ORDER 
    Route::get('/order', 'Customer\OrderController@index')->name('order.index');
    Route::get('/customer/order', 'Customer\OrderController@sortOrder');
    Route::get('/customer/order/invoice/{id}', 'Customer\OrderController@invoicePdf')->name('order.invoice');
    Route::patch('/customer/order/delete/{id}', 'Customer\OrderController@deleteOrder');

    //CREDIT 
    Route::get('/credit', 'Customer\CreditController@index')->name('credit.index');
    Route::post('/order', 'Customer\CreditController@store')->name('order.store');
});



/*
|--------------------------------------------------------------------------
| ADMIN
|--------------------------------------------------------------------------s
*/
//ADMIN AUTHENTICATION ROUTE
Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm')->name('login.admin');
Route::post('/login/admin', 'Auth\LoginController@adminLogin')->name('login.admin');


//ROUTE FOR ADMIN ACCOUNT
Route::group(['middleware' => 'auth:admin'], function() {

    Route::get('generalSearch', 'Backoffice\SearchController@search');
    //Dashboard
    Route::get('/admin', 'Backoffice\DashboardController@index')->name('dashboard');
    Route::get('/project/view', 'Backoffice\DashboardController@viewProject')->name('new.project');
    Route::get('/quotation/view', 'Backoffice\DashboardController@viewQuotation')->name('new.quotation');

    //customers
    Route::get('/customers', 'Backoffice\CustomersController@index')->name('customers.index');
    Route::get('/customer/{id}', 'Backoffice\CustomersController@view')->name('customer.view');
    Route::get('/customer/search/data', 'Backoffice\CustomersController@search');
    Route::get('/customer/search/project/{customer_id}/{value}', 'Backoffice\CustomersController@sortProject');

    //projects
    Route::get('/projects', 'Backoffice\ProjectsController@index')->name('projects.index');
    Route::get('/project/view/{id}', 'Backoffice\ProjectsController@view')->name('project.view');
    Route::get('/project/sort/{value}', 'Backoffice\ProjectsController@sort');
    Route::post('/project/delete/{id}', 'Backoffice\ProjectsController@delete');
    Route::patch('/project/update/status', 'Backoffice\ProjectsController@update');
    Route::get('/project/download', 'Backoffice\ProjectsController@downloadProjectFiles');

    //orders
    Route::get('/orders', 'Backoffice\OrdersController@index')->name('orders.index');
    Route::get('/order/view/{id}', 'Backoffice\OrdersController@view');

    //quotations 
    Route::get('/quotations', 'Backoffice\QuotationController@index')->name('quotations.index');
    Route::get('/quotation/download', 'Backoffice\QuotationController@downloadQuotationFiles');
    Route::get('/quotation/{id}', 'Backoffice\QuotationController@view');
    Route::delete('/quotation/{id}', 'Backoffice\QuotationController@delete');

    //config 
    Route::get('/config', 'Backoffice\ConfigController@index')->name('config.index');
    //add admin
    Route::get('/config/{id}', 'Backoffice\ConfigController@edit')->name('config.edit');
    Route::post('/admin', 'Backoffice\ConfigController@addAdmin');
    Route::patch('/admin', 'Backoffice\ConfigController@updateAdmin');
    Route::delete('/config/admin/delete/{id}', 'Backoffice\ConfigController@deleteAdmin');
    //update price
    Route::get('/config/price/{hour_id}', 'Backoffice\ConfigController@getPrice');
    Route::patch('/config/price', 'Backoffice\ConfigController@updatePrice');
    Route::post('/config/price', 'Backoffice\ConfigController@addprice');
    Route::delete('/config/price/{id}', 'Backoffice\ConfigController@deleteprice');
});