<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationFiles extends Model
{
    protected $table = 'quotation_files';

    protected $guarded = [];

    public function quotaion() {
        return $this->belongsTo('App\Quotation');
    }
}
