<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\SearchRecords;

class User extends Authenticatable
{
    use SearchRecords, Notifiable;

    protected $appends = ['total_credits', 'total_price_orders', 'total_hour_orders',  'total_consumed_hours'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'contact', 'address', 'password', 'is_activated',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function projects() {
        return $this->hasMany('App\Project', 'user_id');
    }
    public function orders() {
        return $this->hasMany('App\Order', 'user_id');
    }
    public function payments() {
        return $this->hasMany('App\Payment', 'user_id');
    }
    public function credit() {
        return $this->hasOne('App\Credit', 'user_id');
    }

    //CUSTOM ATTRIBUTES

    //GET TOTAL CREDITS
    public function getTotalCreditsAttribute(){
        $credits = 0;
        foreach ($this->orders as $order) {
            $credits += $order->hours;
        }
        foreach ($this->projects as $project) {
            $credits -= $project->hours_used;
        }
        return $credits;
    }

    //GET TOTAL PRICE AMOUNT
    public function getTotalPriceOrdersAttribute()
    {
        $total_price_orders = 0;
        foreach ($this->orders as $order) {
            $total_price_orders += $order->price;
        }
        return $total_price_orders;
    }

    //GET TOTAL HOURS ORDERED
    public function getTotalHourOrdersAttribute()
    {
        $total_hour_orders = 0;
        foreach ($this->orders as $order) {
            $total_hour_orders += $order->hours;
        }
        return $total_hour_orders;
    }

    //GET TOTAL CONSUMED HOURS
    public function getTotalConsumedHoursAttribute()
    {
        $total_consumed = 0;
        foreach ($this->projects as $project) {
            $total_consumed += $project->hours_used;
        }
        return $total_consumed;
    }
}
