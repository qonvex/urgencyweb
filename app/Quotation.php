<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $table = 'quotations';

    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User');
    }
    public function quotation_files() {
        return $this->hasMany('App\QuotationFiles', 'quotation_id');
    }
}
