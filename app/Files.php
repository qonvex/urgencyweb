<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
    protected $table = 'files';

    protected $guarded = [];

    public function project() {
        return $this->belongsTo('App\Project');
    }
}
