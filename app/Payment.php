<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User');
    }
    public function order() {
        return $this->belongsTo('App\Order');
    }
   
}
