<?php
namespace App\Libraries;
#qx_uploader ver.1

class Qx_uploader {
	public $dir = "storage/app/";
	public $message = "Success!";
	public $file = "";


	public function upload_now($file, $folder="", $type="", $max_size="") {
		// $folder 		= (!empty($folder) ? $folder . "/" : "");

		$folder_arr = explode('/', $folder);
		$target_dir = base_path($this->dir);
		if (!empty($folder)) {
			foreach ($folder_arr as $key => $value) {
				if (!file_exists($target_dir . $value)) {
					// abort(500, base_path($target_dir . $value));
					mkdir($target_dir . $value);
				}
				$target_dir .= $value . '/';
			}
		}
		
		$file_size		= $file["size"];
		$file_name		= date('mdYGisu_') . basename($file["name"]);
		$target_file 	= $target_dir . $file_name;
		$uploadOk 		= 1;
		$imageFileType 	= pathinfo($target_file,PATHINFO_EXTENSION);

		if (!file_exists($target_dir)) {
			$result = mkdir($target_dir);
		}

		if(!empty($max_size)) {
			$uploadOk = $this->filter_size($file_size, $max_size);
		}

		if(!empty($type) && $uploadOk != 0) {
			$uploadOk = $this->filter_type($imageFileType, $type);
		}

		if ($uploadOk != 0) {
		    if (move_uploaded_file($file["tmp_name"], $target_file)) {
		        $this->message  = 'Success';
		        $this->file  	= $file_name;

		        $this->resize_image_upload($target_file, $this->file);
		    } else {
		       $this->message  	= 'Upload Error: Please choose another file.';
		    }
		}

		return array('res'		=>	$uploadOk,
					 'message'	=>	$this->message,
					 'file'		=>	$this->file
				);
	}

	function filter_size($file_size, $max_size) {
		$res = 1;
		if($file_size > $max_size) {
			$res = 0;
			$this->message  = 'File too large!';
		}

		return $res;
	}


	function filter_type($imageFileType, $allowed_types) {
		$res = 1;
		switch ($allowed_types) {
			case 'image':
				if((strtolower($imageFileType) != "jpg") && 
				   (strtolower($imageFileType) != "png") && 
				   (strtolower($imageFileType) != "jpeg") && 
				   (strtolower($imageFileType) != "gif" )) {
				    $res = 0;
					$this->message  = 'This file is not an Image!';
				}
				break;

			case 'document':
				if((strtolower($imageFileType) != "docx") && 
				   (strtolower($imageFileType) != "pdf" )) {
				    $res = 0;
					$this->message  = 'This file is not a Document!';
				}
				break;

			case 'csv':
				if((strtolower($imageFileType) != "csv") && 
				   (strtolower($imageFileType) != "xlsx" )) {
				    $res = 0;
					$this->message  = 'This file is Invalid!';
				}
				break;

			case 'video':
				if((strtolower($imageFileType) != "mp4") && 
				   (strtolower($imageFileType) != "flv" )) {
				    $res = 0;
					$this->message  = 'This file is not a Video!';
				}
				break;

			case 'music':
				if((strtolower($imageFileType) != "mp3") && 
				   (strtolower($imageFileType) != "wav" )) {
				    $res = 0;
					$this->message  = 'This file is not a Music!';
				}
				break;

			default:
				if(strtolower($imageFileType) != $allowed_types){
				    $res = 0;
					$this->message  = 'This file is Invalid!';
				}
				break;
		}

		return $res;
	}

	function delete_file($filename) {
		$file_path		=	$this->dir . $filename;
		unlink($file_path);
	}

	function resize_image_upload($src, $filename) {


		$size = getimagesize($src);

		if(in_array($size[2] , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
	    {
			$jpeg_quality = 50;

			$perimeter = array('small'	=>	280, 'medium'	=>	480, 'large'	=>	760);

			$w = $size[0]; //original width
			$h = $size[1]; //original height

			foreach ($perimeter as $key => $value) {
				try {
					if (!file_exists($this->dir . 'thumbs')) {
						mkdir($this->dir . 'thumbs/');
					}

					if (!file_exists($this->dir . 'thumbs/' . $key . '/')) {
						mkdir($this->dir . 'thumbs/' . $key . '/');
					}

					$dest = $this->dir . 'thumbs/' . $key . '/' . $filename;
					$targ_w = $value;
					$targ_h = ($targ_w / $w) * $h;

					$exploded = explode('.',$src);
					$ext = $exploded[count($exploded) - 1]; 

					if (preg_match('/jpg|jpeg/i',$ext)) {
						$img_r = imagecreatefromjpeg($src);
					}else if (preg_match('/png/i',$ext)) {
						$img_r = imagecreatefrompng($src);
					}else if (preg_match('/gif/i',$ext)) {
						$img_r = imagecreatefromgif($src);
					}else if (preg_match('/bmp/i',$ext)) {
						$img_r = imagecreatefrombmp($src);
					}

					$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

					//set transparency to png
					if (preg_match('/png/i',$ext)) {
						imagealphablending($dst_r, false);
	 					imagesavealpha($dst_r,true);
	 					$transparent = imagecolorallocatealpha($dst_r, 255, 255, 255, 127);
	 					imagefilledrectangle($dst_r, 0, 0, $targ_w, $targ_h, $transparent);
	 				}

					$sample = imagecopyresampled($dst_r, $img_r, 0, 0, 0, 0,
					$targ_w,$targ_h,$w,$h);

					if (preg_match('/png/i',$ext)) {
						imagepng($dst_r, $dest);
					} else {
						imagejpeg($dst_r, $dest, $jpeg_quality);
					}
				} catch (Exception $e) {
					
				}
			}
	    }


	}

	function regenerate_thumbnails($images, $location) {
		if (count($images)) {
			foreach ($images as $imk => $imv) {
				$dir = $this->dir . $location . '/' . $imv; 
				if (file_exists($dir) && !empty(trim($imv))) {
					$this->resize_image_upload($dir, $imv);
				}
			}
		}
	}
}
