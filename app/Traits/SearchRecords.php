<?php 
namespace App\Traits;

trait SearchRecords {

  public function scopeSearch($query, $whereClauses, $fields)
  {
  	foreach ($whereClauses as $key => $value) {
  		$query = $query->where($key, 'LIKE', '%'.$value.'%');
  	}

  	if(!empty($fields)) {
  		$query->select(explode(',', trim($fields)));
  	}

  	return $query;
  }
 
}
