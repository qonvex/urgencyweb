<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Project;
use App\Files;
use Session;
class AccountController extends Controller
{
    public function index()
    {
        $projects = Project::where('user_id', Auth::user()->id)
                            ->where('is_deleted', 0)
                            ->orderBy('created_at', 'desc')->get();
        return view('customer.account.index', compact('projects'));
    }

    public function store(Request $request)
    {
        $this->validate_project($request);
        $project = new Project;
        $project->title = $request->title;
        $project->start_date = $request->start_date;
        $project->description = $request->description;
        $project->message = $request->message;
        $project->user_id = Auth::user()->id;
        $project->save();

        //UPLOADING FILES
        if ($request->file) {
            $allowedfileExtension=['pdf','jpg','png','docx','xlsx','ppt','pptx'];
            foreach ($request->file as $file) {
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();

                $check=in_array($extension,$allowedfileExtension);
                if ($check) {
                    $date = date('YmdHis');
                    $file_name = $date.$filename;
                    $file->storeAs('storage/projectFiles', $file_name);
                    $fileModel = new Files;
                    $fileModel->name = $file_name;
                    $fileModel->file_name = $filename;
                    $fileModel->project_id = $project->id;
                    $fileModel->save();
                }
           }
        }
    }
    
    public function view($id)
    {
        $project = Project::with('files')->find($id);
        return view('customer.account.project.view', compact('project'));
    }

    public function sortProject(Request $request)
    {
        $value = $request->value;
        if ($value == 3) {
            $projects = Project::where('user_id', Auth::user()->id)->where('is_deleted', 0)->get();            
        }
        else {
            $projects = Project::where('status', $value)->where('user_id', Auth::user()->id)->where('is_deleted', 0)->get();
        }
        return response()->json([
            'projects' => $projects,
            'success' => 'Record get successfully!'
        ]);
    }

    public function validate_project($request)
    {
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'start_date' => 'required|max:255',
            'description' => 'max:1000',
            'message' => 'max:1000',
        ]);
    }

    //update profile
    public function updateProfile(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:2|max:255',
            'email' => 'required|email|min:2|max:100',
        ]);
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->contact = $request->contact;
        $user->address = $request->address;

        // $file = $request->file('profile');
        // dd($file);

        if ($file = $request->file('profile')) {
            
            $pic_name = $user->profile;
            $image_path = public_path('assets/profile/'.$pic_name);
            if($pic_name != 'prof.jpg') {
                if(file_exists($image_path)) {
                    unlink($image_path);
                }
            }

            $name = time() . $file->getClientOriginalName();
            $user->profile = $name;
            $file->move('assets/profile', $name);
        }

        if ($request->current_password) {
            if (!(Hash::check($request->current_password, $user->password))) {
                // The passwords matches
                Session::flash('danger', 'Current password did not match');
                return redirect()->back();
            }
            if(strcmp($request->current_password, $request->new_password) == 0){
                //Current password and new password are same
                Session::flash('danger', 'Current password and New password cannot be the same');
                return redirect()->back();
            }

            if(!($request->new_password == $request->confirm_password)){
                //Current password and new password are not same
                Session::flash('danger', 'New password and Confirm password did not match');
                return redirect()->back();
                
            }
            $user->password = bcrypt($request->new_password);
        }

        $user->save();
        Session::flash('success', 'Profile information updated');
        return redirect()->back();
    }
}
