<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Quotation;
use App\QuotationFiles;
use App\Mail\SendQuotation;
use Mail;
class QuotationController extends Controller
{
    public function send(Request $request)
    {
        $this->validate_quotation($request);
        $quotation = new Quotation;
        $quotation->name = $request->name;
        $quotation->email = $request->email;
        $quotation->subject = $request->subject;
        $quotation->message = $request->message;
        $quotation->save();

         //UPLOADING FILES
        if ($request->file) {
            $allowedfileExtension=['pdf','jpg','png','docx','xlsx','ppt','pptx'];
           foreach ($request->file as $file) {
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();

                $check=in_array($extension,$allowedfileExtension);
                if ($check) {
                    $date = date('YmdHis');
                    $file_name = $date.$filename;
                    $file->storeAs('storage/quotationFiles', $file_name);
                    $quotaionFileModel = new QuotationFiles;
                    $quotaionFileModel->name = $file_name;
                    $quotaionFileModel->quotation_id = $quotation->id;
                    $quotaionFileModel->save();
                }
           }

        }
        //SEND EMAIL TO CUSTOMER
        //email content
         $content = [
             'title'=> 'UrgenceWeb Quotation', 
             'name'=> $quotation->name, 
             'email'=> $quotation->email, 
             'subject'=> $quotation->subject,
             'message'=> $quotation->message, 
             'body'=> 'Your quotation was sent to urgenceweb',
         ];
        Mail::to($quotation->email)->send(new SendQuotation($content));
    }
    public function validate_quotation($request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email',
            'subject' => 'required|max:1000',
            'message' => 'max:1000',
        ]);
    }
}
