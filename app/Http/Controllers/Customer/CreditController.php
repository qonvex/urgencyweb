<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Project;
class CreditController extends Controller
{
    public function index()
    {
        //projects
        $projects = Project::where('status', 1)
                            ->orWhere('status', 2)
                            ->where('user_id', Auth::user()->id)
                            ->orderBy('status', 'desc')->get();
        $customer = Auth::user();
        return view('customer.credit.index', compact('projects', 'customer'));
    }

    // public function store(Request $request)
    // {
    //     $this->validate_project($request);
    //     $order = new Order;
    //     $order->hours = $request->hour;
    //     $order->price = $request->hour * 10;
    //     $order->user_id = Auth::user()->id;
    //     $order->save();

    //    $user = Auth::user();
    //    if ($user->credit == null) {
    //        $credit = new Credit;
    //        $credit->hours = $request->hour;
    //        $credit->user_id = $user->id;
    //        $credit->save();
    //    }
    //    else {
    //        $credit = $user->credit;
    //        $credit->hours = $credit->hours + $request->hour;
    //        $credit->save();
    //    }
    //     return response()->json([
    //         'order' => $order,
    //         'success' => 'Record post successfully!'
    //     ]);
    // }

    // public function validate_project($request)
    // {
    //     $validatedData = $request->validate([
    //         'hour' => 'required|numeric|min:1',
    //     ]);
    // }
}
