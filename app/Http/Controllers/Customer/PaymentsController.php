<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use Illuminate\Support\Facades\Auth;
use App\Payment;
use App\Order;
use App\Config;
class PaymentsController extends Controller
{

    public function verifyTransaction(){

        $apiContext = new PayPalHttpClient(
            new SandboxEnvironment(
              config('paypal.id'),
              config('paypal.secret')
            )
        );

        $order= $apiContext->execute(new OrdersGetRequest(request()->orderID))->result;
        //process validation here
        $hour_id=request()->hour_id;

        $hours=request()->hours;
        $order_price = $order->purchase_units[0]->amount->value;
        if ($order_price!=$this->getPriceInt($hour_id)) {
            //hackerman
            abort(419);
        }
        $capture = $apiContext->execute(new OrdersCaptureRequest($order->id))->result;

        //add row to payments/transactions table and orders table
        $price = Config::findOrFail($hour_id);
        $orderModel = new Order;
        $orderModel->hours = request()->hours;
        $orderModel->price = $price->hour_price / $hours;
        $orderModel->user_id = Auth::user()->id;
        $orderModel->save();

        $payment = new Payment;
        $payment->amount = $price->hour_price;
        $payment->payer_id = request()->payerID;
        $payment->order_transaction_id = request()->orderID;
        $payment->user_id = Auth::user()->id;
        $payment->order_id = $orderModel->id;
        $payment->save();

        return redirect()->route('credit.index');
    }

    public function getPriceInt($id){
        return Config::findOrFail($id)->hour_price;
    }

    public function getPrice()
    {
        return response()->json([
            'price' => $this->getPriceInt()
        ]);
    }
}
