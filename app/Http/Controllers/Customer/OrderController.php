<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Order;
use PDF;
class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::with('payment')->where('user_id', Auth::user()->id)
                            ->where('is_deleted', 0)
                            ->orderBy('created_at', 'desc')->paginate(10);
        return view('customer.order.index', compact('orders'));
    }

    public function sortOrder(Request $request)
    {
        $value = $request->value;
            if ($value == 0) {
                $orders = Order::where('user_id', Auth::user()->id)
                                ->where('is_deleted', 0)
                                ->orderBy('created_at', 'desc')->get();            
            }
            else {
                $orders = Order::whereMonth('created_at', $value)
                                ->where('is_deleted', 0)
                                ->where('user_id', Auth::user()->id)
                                ->orderBy('created_at', 'desc')->get();
            }
            return response()->json([
                'orders' => $orders,
                'success' => 'Record get successfully!'
            ]);
    }

    public function deleteOrder(Request $request, $id)
    {

        $order = Order::findOrFail($id);
        $order->is_deleted = 1;
        $order->save();
        return response()->json([
            'order' => $order,
            'success' => 'Record get successfully!'
        ]);
    }

    //invoice
    public function invoicePdf($id)
    {
        $order = Order::with('payment', 'user')->findOrFail($id);

        $data = [
            'customer_name' => $order->user->name,
            'customer_email' => $order->user->email,
            'customer_address' => $order->user->address,
            'invoice_date' => $order->payment->created_at,
            'payment_id' => $order->payment->id,
            'payer_id' => $order->payment->payer_id,
            'order_id' => $order->payment->order_transaction_id,
            'order_id' => $order->payment->order_transaction_id,
            'hours' => $order->hours,
            'amount' => $order->payment->amount,
        ];
        $pdf = PDF::loadView('customer.order.invoice', $data);
        return $pdf->download('order'.$order->id.'.pdf');
    }
}
