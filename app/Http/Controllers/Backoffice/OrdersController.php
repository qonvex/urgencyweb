<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
class OrdersController extends Controller
{
    public function index()
    {
        $orders = Order::with('user', 'payment')->orderBy('created_at', 'desc')->paginate(10);

        $total_billed = Order::sum('price');
        $total_hours = Order::sum('hours');
        $total_orders = Order::count();

        return view('backoffice.orders.index', compact('orders', 'total_hours', 'total_billed', 'total_orders'));
    }

    public function view($id)
    {
        $order = Order::with('user')->findOrFail($id);
        return response()->json([
            'order' => $order
        ]);
    }
}
