<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\User;
use Zipper;
class ProjectsController extends Controller
{
    public function index()
    {
        $projects = Project::with('user')->where('is_deleted', 0)->orderBy('created_at', 'desc')->paginate(10);

        $not_started = Project::where('is_approved', 0)->where('is_deleted', 0)->get();
        $in_progress = Project::where('status', 1)->where('is_deleted', 0)->get();
        $done = Project::where('status', 2)->where('is_deleted', 0)->get();
        return view('backoffice.projects.index', compact('projects', 'not_started', 'in_progress', 'done'));
    }

    //sorting project
    public function sort($value)
    {
        if ($value == 3) {
            $projects = Project::where('is_deleted', 0)->orderBy('created_at', 'desc')->limit(10)->get();
        }else {
            $projects = Project::where('status', $value)
                                ->where('is_deleted', 0)->orderBy('created_at', 'desc')->get();
        }
        return response()->json([
            'projects' => $projects,
            'success' => 'Record get successfuly'
        ]);
    }

    //view project
    public function view($id)
    {
        $project = Project::with('user.credit', 'files')->findOrFail($id);
        //gert the customer credit
        $customer = User::with('orders', 'projects')->findOrFail($project->user_id);


         //get total hours
        $consumed = array();
        $total_hours = array();
        foreach ($customer->orders as $order) {
             array_push($total_hours, $order->hours);
        }
         //total consumed hours
        foreach ($customer->projects as $project_consumed) {
             array_push($consumed, $project_consumed->hours_used);
        }
        $total_consumed = array_sum($consumed);
        $total_hours = array_sum($total_hours);
 
        $credit_hours = $total_hours - $total_consumed;

        return response()->json([
            'project' => $project,
            'customer' => $customer,
            'credit_hours' => $credit_hours,
            'success' => 'Record get successfully!'
        ]);
    }

    //update project
    public function update(Request $request)
    {
        $project = Project::findOrFail($request->id);
        if ($request->approve == "on") {
            $project->is_approved = 1;
        } else {
            $project->is_approved = 0;
        }
        $project->status = $request->status;
        if($request->hour_used == null ) {
            $project->hours_used = 0;
        } else {
            $project->hours_used = $request->hour_used;
        }
        $project->save();
        return response()->json([
            'project' => $project,
            'success' => 'Record updated successfully!'
        ]);
    }

     //delete project
    public function delete($id)
    {
        $project = Project::findOrFail($id);
        $project->is_deleted = 1;
        $project->save();
        return response()->json([
            'project' => $project,
            'success' => 'Record deleted successfully!'
        ]);
    }

    public function downloadProjectFiles(Request $request)
    {
        $project = Project::with('files')->findOrFail($request->id);
        $files_array = [];
        foreach ($project->files as $file) {
            array_push($files_array, storage_path('app/storage/projectFiles/'.$file->name));
        }
        if (file_exists(storage_path('app/storage/zip_projectFiles/'.$project->title.'.zip'))) {
            unlink(storage_path('app/storage/zip_projectFiles/'.$project->title.'.zip'));
        }
        Zipper::make(storage_path('app/storage/zip_projectFiles/'.$project->title.'.zip'))->add($files_array)->close();
        return response()->download(storage_path('app/storage/zip_projectFiles/'.$project->title.'.zip'));
    }
}
