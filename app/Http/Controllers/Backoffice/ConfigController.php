<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Admin;
use App\Config;
class ConfigController extends Controller
{
    public function index()
    {
    	$admins = Admin::where('id', '!=', Auth::user()->id)->get();
    	$config_hours = Config::all()->sortBy('hours');
    	return view('backoffice.config.index', compact('admins', 'config_hours'));
    }

	//add admin
    public function addAdmin(Request $request)
    {	
    	$this->validate_admin($request);
    	$admin = new Admin;
    	$admin->name = $request->name;
    	$admin->email = $request->email;
		$admin->password = bcrypt($request->password);
		$admin->save();
	}

    public function updateAdmin(Request $request)
    {
        $this->validate_admin($request);
        $id=$request->id;
        $admin=Admin::findOrFail($id);
        $admin->update([
            'name'=>$request->name,
            'email'=>$request->email
        ]);
        if ($request->changepass=='true') {
            if (Hash::check($request->old_password,$admin->password)) {
                $admin->fill(['password'=>Hash::make($request->password)])->save();
                $request->session()->flash('success', 'Password changed');
                return 'success';
            } else {
                 return response()->json(['errors'=>['nomatch'=>'Old password does not match']],419);
            }
           
        }

    }
    // show edit modal with admin data
    public function edit($id)
    {
        return Admin::find($id);
    }
	//delete admin with id
	public function deleteAdmin($id)
	{
		$admin = Admin::findOrFail($id);
		$admin->delete();
	}

    //delete price
    public function deleteprice($id)
    {
        $config = Config::findOrFail($id);
        $config->delete();
    }
    //add new price
    public function addprice(Request $request)
    {
        $validatedData = $request->validate([
            'hour' => 'required|gte:1|unique:configs,hours',
            'price' => 'required|numeric|gte:0',
        ]);
        $hour=$request->hour;
        $price=$request->price;
        $pricing=new Config;
        $pricing->hours=$hour;
        $pricing->hour_price=$price;
        $pricing->save();
        return json_encode('success',200);
    }

	//get the price
	public function getPrice($hour_id)
	{
		$hour = Config::findOrFail($hour_id);
		return response()->json([
			'hour' => $hour
		]);
	}

	//update the price
	public function updatePrice(Request $request)
	{
		$validatedData = $request->validate([
            'hour' => 'required|gte:1|unique:configs,hours,'.$request->price_id,
            'price' => 'required|numeric|gte:0',
        ]);

        $id=$request->price_id;
        $config=Config::findOrFail($id);
        $config->update(['hours'=>$request->hour,'hour_price'=>$request->price]);
        return json_encode('success',200);
	}

	//validation
    public function validate_admin($request)
    {
        if ($request->has('changepass')) {
            if ($request->changepass=='true') {
                $validatedData = $request->validate([
                    'name' => 'required|max:255',
                    'email' => 'required|email|unique:admins,email,'.$request->id,
                    'old_password' => 'required',
                    'password' => 'required|min:8|same:confirm_password|different:old_password'
                ]);
            } else {
                $validatedData = $request->validate([
                    'name' => 'required|max:255',
                    'email' => 'required|email|unique:admins,email,'.$request->id,
                ]);
            }
        } else {
            $validatedData = $request->validate([
                'name' => 'required|max:255',
                'email' => 'required|email|unique:admins',
                'password' => 'required|min:8|same:confirm_password'
            ]);
        }
    }
}
