<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Project;
use App\Order;
use App\Quotation;
use App\Payment;
use Carbon\Carbon;
use DB;
class DashboardController extends Controller
{
    public function index()
    {
        $customers = User::where('is_activated', 1)->get();
        $projects = Project::where('is_approved', 0)->get();
        $orders = Order::all();

        //GET TOTAL HOURS REMAINING
        $approved_projects = Project::where('is_approved', 1)->get();

        $orders = Order::all();

        //get total hours
        $hours = array();
        //total billed
        $payments = Payment::all();
        $billed = array();
        foreach ($orders as $order) {
        array_push($hours, $order->hours);
        }
        foreach ($payments as $payment) {
        array_push($billed, $payment->amount);
        }
        $total_hours = array_sum($hours);
        $total_billed = array_sum($billed);

        //all orders
        $all_orders = Order::with('user', 'payment')->orderBy('created_at', 'desc')->take(10)->get();
        //new quotations
        $new_quotations = Quotation::with('qoutation_files')->where('is_viewed', 0)->get();

        return view('backoffice.admin', compact('customers', 'projects', 'orders', 'total_billed', 'total_hours', 'all_orders', 'new_quotations'));
    }

    //view new project
    public function viewProject()
    {
        $projects = Project::with('user')->where('is_deleted', 0)->where('status', 0)->orderBy('created_at', 'desc')->paginate(10);

        $not_started = Project::where('is_approved', 0)->where('is_deleted', 0)->get();
        $in_progress = Project::where('status', 1)->where('is_deleted', 0)->get();
        $done = Project::where('status', 2)->where('is_deleted', 0)->get();
        return view('backoffice.projects.index', compact('projects', 'not_started', 'in_progress', 'done'));
    }

    //view new quotation
    public function viewQuotation()
    {
        $quotations = Quotation::where('is_viewed', 0)->orderBy('created_at', 'desc')->paginate(10);;
        return view('backoffice.quotations.index', compact('quotations'));
    }
}
