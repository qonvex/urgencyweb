<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class SearchController extends Controller
{
	private $records = [];

    public function search()
    {
    	$customers =  User::search(
    		['name' => request()->keyword], 
    		'name,id')->get();
    	$this->sanitizeRecords($customers, 'customer');

    	return $this->records;

    }

    public function sanitizeRecords($data, $type)
    {
    	if(count($data) < 1) return true;

    	foreach ($data as $key => $value) {
	    	switch ($type) {
	    		case 'customer':
	    			$this->records[] = $value->name;
	    			// $this->records[] = ['name' => $value->name, 'url' => 'customer/'.$value->id];
	    			break;
	    		
	    		default:
	    			# code...
	    			break;
	    	}
    	}
    }

}
