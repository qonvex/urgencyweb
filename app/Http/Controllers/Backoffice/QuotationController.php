<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Quotation;
use App\QuotationFiles;
use Zipper;
class QuotationController extends Controller
{
    public function index()
    {
        $quotations = Quotation::orderBy('created_at', 'desc')->paginate(10);;
        return view('backoffice.quotations.index', compact('quotations'));
    }

    //view quotation
    public function view($id)
    {
        $quotation = Quotation::with('quotation_files')->findOrFail($id);
        return response()->json([
            'quotation' => $quotation
        ]);
    }

    //delete quotation
    public function delete($id)
    {
        $quotation = Quotation::findOrFail($id);
        $quotation->delete();
        return response()->json([
            'quotation' => $quotation,
            'success' => 'Record deleted successfully!'
        ]);
    }

    public function downloadQuotationFiles(Request $request)
    {
        $quotation = Quotation::with('quotation_files')->findOrFail($request->id);
        
        $files_array = [];
        foreach ($quotation->quotation_files as $file) {
            array_push($files_array, storage_path('app/storage/quotationFiles/'.$file->name));
        }
        if (file_exists(storage_path('app/storage/zip_quotationFiles/'.$quotation->subject.'.zip'))) {
            unlink(storage_path('app/storage/zip_quotationFiles/'.$quotation->subject.'.zip'));
        }
        Zipper::make(storage_path('app/storage/zip_quotationFiles/'.$quotation->subject.'.zip'))->add($files_array)->close();
        return response()->download(storage_path('app/storage/zip_quotationFiles/'.$quotation->subject.'.zip'));
    }
}
