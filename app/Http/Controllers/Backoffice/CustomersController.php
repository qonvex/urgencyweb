<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Order;
use App\Project;
class CustomersController extends Controller
{
    public function index()
    {
        $customers = User::with('projects', 'orders', 'credit')->where('is_activated', 1)
                        ->orderBy('created_at', 'desc')->paginate(10);
        return view('backoffice.customers.index', compact('customers'));
    }

    public function view($id)
    {
        $customer = User::with('projects', 'projects.files', 'orders', 'credit')->findOrFail($id);
        $totalOrderHours = $customer->orders->sum('hours');
        $totalConsumedHours = $customer->projects->sum('hours_used');

        $orders = Order::with('payment')->where('user_id', $customer->id)->orderBy('created_at', 'desc')->paginate(6);
        return view('backoffice.customers.view', compact('customer', 'orders', 'totalOrderHours', 'totalConsumedHours'));
    }

    //search customer
    public function search(Request $request)
    {

        if ($request->search == null) {
            $customers = User::with('projects', 'orders')->where('is_activated', 1)->limit(10)->get();
        }else {
             $customers = User::with('projects', 'orders')->where('is_activated', 1)
                                ->where('name','LIKE','%'.$request->search."%")
                                ->orWhere('email','LIKE','%'.$request->search."%")
                                ->limit(10)
                                ->get();
        }

        return response()->json([
            'customers' => $customers,
            'success' => 'Record get successfuly'
        ]);
    }

    //customer sorting project
    public function sortProject($customer_id, $value)
    {
        if ($value == 3) {
            $projects = Project::where('is_deleted', 0)->where('user_id', $customer_id)->orderBy('created_at', 'desc')->limit(10)->get();
        }else {
            $projects = Project::where('status', $value)
                                ->where('is_deleted', 0)
                                ->where('user_id', $customer_id)
                                ->orderBy('created_at', 'desc')->limit(10)->get();
        }
        return response()->json([
            'projects' => $projects,
            'success' => 'Record get successfuly'
        ]);
    }
}
