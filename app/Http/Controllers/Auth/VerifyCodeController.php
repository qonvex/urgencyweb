<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use App\Mail\SendActivation;
use Mail;
use URL;
use Auth;
class VerifyCodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function showCodeForm()
    {
        return view('auth.verifycode');
    }


    public function credentials(Request $request)
    {
        $request['is_activated'] = 1;
        return $request->only('email', 'password', 'is_activated');
    }

    public function verifyCode(Request $request, $token)
    {
        $check = DB::table('users_activation')->where('token', $token)->first();

        $pin = $check->pin;
           
        $codes = str_split($pin);

        $one = $request->input('first');
        $two = $request->input('two');
        $three = $request->input('three');
        $four = $request->input('four');
        $five = $request->input('five');

        $activation_code = array($one, $two, $three, $four, $five);
        if(!is_null($check)){
          $user = User::find($check->id_user);

          if ($codes === $activation_code) {
              $user->update(['is_activated' => 1]);
              DB::table('users_activation')->where('token',$token)->delete();

              Mail::send('emails.success', $user->toArray(), function($message) use ($user){
                $message->to($user->email);
                $message->subject('Activated');
              });
              Auth::login($user);
              return redirect('/account')->with('success',"activated successfully.");
          }
          if ($user->is_activated == 1){
            return redirect()->to('login')->with('success',"user are already actived.");
          }
        }
        return back()->with('errors', "code ddnt match");
    }

    public function resendCode(Request $request, $token)
    {
        $check = DB::table('users_activation')->where('token', $token)->first();
        $user = User::find($check->id_user);

        $content = [
          'title'=> 'UrgenceWeb Activation Code', 
          'user'=> $user->name, 
          'link'=> URL::to('/').'/verification/code/'.$check->token, 
          'pin'=> $check->pin, 
          'body'=> 'Hi this is your activation code',
          'button' => 'Click here'
        ];

        //sending email
        Mail::to($user->email)->send(new SendActivation($content));
        return back()->with('success', "email sent");
    }
}
