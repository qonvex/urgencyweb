<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use DB;
use App\Mail\SendActivation;
use Mail;
use URL;
use Auth;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/account';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function register(Request $request) {
        $input = $request->all();
        $validator = $this->validator($input);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        if ($validator->passes()){
            $user = $this->create($input)->toArray();
            $user['link'] = str_random(30);
            $user['pin'] = $this->generatePIN();

            //email content
            $content = [
                'title'=> 'UrgenceWeb Activation Code', 
                'user'=> $user, 
                'link'=> URL::to('/').'/verification/code/'.$user['link'], 
                'pin'=> $user['pin'], 
                'body'=> 'Here is your activation code',
                'button' => 'Click here'
            ];
            //saving to verification table
            DB::table('users_activation')->insert(['id_user'=>$user['id'],'token'=>$user['link'], 'pin'=>$user['pin']]);
    
            //sending email
            Mail::to($user['email'])->send(new SendActivation($content));
            return response()->json([
                'link' => $user['link'],
            ], 200);
        }
      }

      public function generatePIN($digits = 5)
      {
          $i = 0; //counter
          $pin = ""; //our default pin is blank.
          while($i < $digits){
              //generate a random number between 0 and 9.
              $pin .= mt_rand(0, 9);
              $i++;
          }
          return $pin;
      }





    //create new customer
    protected function createNewCustomer(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'is_activated' => 1,
        ]);
    }
    public function newCustomerOrder(Request $request) {
        $input = $request->all();
        $validator = $this->validator($input);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        if ($validator->passes()){
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'is_activated' => 1,
            ]);

            Auth::login($user);
            return redirect('/account');
        }
    }
}
