<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User');
    }
    public function payment() {
        return $this->hasOne('App\Payment', 'order_id');
    }
}
