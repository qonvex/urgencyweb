<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use App\Project;
use App\Order;
use App\Payment;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'contact' => $faker->phoneNumber,
        'address' => $faker->streetAddress,
        'is_activated' => 1,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->define(Project::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'start_date' => now(),
        'description' => $faker->paragraph,
        'message' => $faker->paragraph,
    ];
});

$factory->define(Order::class, function (Faker $faker) {

    $prices = \App\Config::pluck('hour_price')->toArray();
    $hours = \App\Config::pluck('hours')->toArray();
    $index=$faker->numberBetween(0,count($prices) - 1);
    return [
        'hours' => $hours[$index],
        'price' => $prices[$index],
    ];
});

$factory->define(Payment::class, function (Faker $faker) {
    return [
        'amount' => $faker->numberBetween(1,30),
        'payer_id' => $faker->regexify('[A-Z0-9]+[A-Z0-9]+[A-Z]{2,4}'),
        'order_transaction_id' => $faker->regexify('[A-Z0-9]+[A-Z0-9]+[A-Z]{2,4}'),
    ];
});
