<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $users = factory(App\User::class, 100)
           ->create()
           ->each(function ($user) {
                $projects = factory(App\Project::class, 5)->make()
                ->each(function($project) use ($user) {
                    $project->user_id = $user->id;
                    $project->save();
                });
                $orders = factory(App\Order::class, 5)->make()
                ->each(function($order) use ($user) {
                    $order->user_id = $user->id;
                    $order->save();
                    $payment = factory(App\Payment::class)->make();
                    $payment->amount=$order->price;
                    $payment->order_id=$order->id;
                    $payment->user_id=$user->id;
                    $payment->save();
                });
        });
    }
}
